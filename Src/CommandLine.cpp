#include "CommandLine.h"

using namespace std::literals;

ce::CommandLine::CommandLine(const std::vector<std::string>& RawCommandLine) 
    : RawCommandLine(RawCommandLine),
    PositionalArguments(),
    KeyValueArguments()
{
    bool skippedFirst = false;
    for(const std::string& Arg : RawCommandLine)
    {
        if(!skippedFirst)
        {
            skippedFirst = true;
            continue;
        }

        if(Arg.starts_with("-"s))
        {
            auto assignIdx = Arg.find("=");
            if(assignIdx == std::string::npos)
            {
                this->KeyValueArguments.push_back(std::make_pair(Arg, "True"));
            }
            else
            {
                this->KeyValueArguments.push_back(std::make_pair(Arg.substr(0, assignIdx), Arg.substr(assignIdx + 1)));
            }
        }
        else
        {
            PositionalArguments.push_back(Arg);
        }
    }
}

const std::string& ce::CommandLine::GetExecutablePath() const
{
    return this->RawCommandLine[0];
}


const std::vector<std::string>& ce::CommandLine::GetPositionalArguments() const
{
    return this->PositionalArguments;
}

const std::string* ce::CommandLine::GetKeyValueArgument(const std::string& Key) const
{
    for(const auto& keyValue : this->KeyValueArguments)
    {
        if(keyValue.first == Key)
        {
            return &keyValue.second;
        }
    }

    return nullptr;
}