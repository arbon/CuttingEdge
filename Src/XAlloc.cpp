#include "XAlloc.h"

void * ce::XAlloc::aligned_malloc(size_t size, size_t alignment) {
#ifdef _WIN32
    return _aligned_malloc(size, alignment);
#endif
#ifdef __gnu_linux__
    return aligned_alloc(alignment, size);
#endif
}

void ce::XAlloc::aligned_free(void * p) {
#ifdef _WIN32
    _aligned_free(p);
#endif
#ifdef __gnu_linux__
    free(p);
#endif
}
