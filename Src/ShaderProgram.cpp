#include "ShaderProgram.h"

const char* ce::ShaderProgram::VERT_EXTENSION = ".vert";
const char* ce::ShaderProgram::FRAG_EXTENSION = ".frag";
const char* ce::ShaderProgram::GEOM_EXTENSION = ".geom";

ce::ShaderProgram::ShaderProgram(std::string name) : name(name), shaders(), programId(), properties()
{
}

bool ce::ShaderProgram::CheckShaderStatus(const ce::ShaderProgram::Shader& shader, std::string &info) {
    // GLint id = shader.id;
	// GLint success = 0;
    // glGetShaderiv(id, GL_COMPILE_STATUS, &success);

    // if (success == GL_FALSE) {
    //     //Setup Error Log
    //     GLint maxLength = 0;
    //     glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

    //     std::vector<char> errorLog(maxLength);
    //     glGetShaderInfoLog(id, maxLength, &maxLength, &errorLog[0]);

    //     for (int i = 0; i < errorLog.size(); i++) {
    //         info += errorLog[i];
    //     }
    //     return false;
    // }
    return true;
}
