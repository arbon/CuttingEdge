#include "Texture.h"

ce::Texture::Texture(std::shared_ptr<ce::Texture::TextureData> data)
{
	this->data = data;
}

glm::vec2 ce::Texture::GetDimensions()
{
	if (this->data) {
		return glm::vec2(data->width, data->height);
	}

    return glm::vec2();
}