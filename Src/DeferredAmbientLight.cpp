#include "DeferredAmbientLight.h"

DeferredAmbientLight::DeferredAmbientLight() : ce::PostProcessEffect()
{
    std::shared_ptr<ce::ShaderProgram> program;

    if (program = ce::ResourceManager::GetShaderProgram("Ambient Light")) {
        this->material = std::make_shared<ce::Material>(program);
    }
    else {
        program = std::make_shared<ce::ShaderProgram>(ce::ShaderProgram("Ambient Light"));

        program->shaders.push_back(ce::ResourceManager::LoadShader(ambientVertPath));
        program->shaders.push_back(ce::ResourceManager::LoadShader(ambientFragPath));

        ce::ResourceManager::CompileShaderProgram(program);
        this->material = std::make_shared<ce::Material>(program);
    }
}
