#include "Deferred2DRenderLayer.h"

ce::Deferred2DRenderLayer::Deferred2DRenderLayer()
{
    auto diffuseBuffer = std::make_shared<ce::RenderLayer::TexBuffer>();
    diffuseBuffer->name = "diffuse";

    auto surfaceNormalBuffer = std::make_shared<ce::RenderLayer::TexBuffer>();
    surfaceNormalBuffer->name = "surfacenormal";

    this->inputBufferReqs.push_back(diffuseBuffer);
    this->inputBufferReqs.push_back(surfaceNormalBuffer);
}

void ce::Deferred2DRenderLayer::Render(ce::Camera & camera)
{
    if (!this->inputBuffers.size()) {
        return;
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    auto i = this->ppDrawables.begin();
    while (i != this->ppDrawables.end()) {
        if (auto drawable = i->drawable.lock()) {
            auto materialLocked = drawable->GetMaterial().lock();

            if (!materialLocked) {
                continue;
            }

            for (auto buffer : this->inputBuffers) {
                materialLocked->SetTexture(buffer->name, buffer->Buffer);
            }

            materialLocked->Bind();
            materialLocked->UpdateAllUniforms();

            drawable->Draw();

            materialLocked->Unbind();

            ++i;
        }
        else {
            i = this->ppDrawables.erase(i);
        }
    }
}

void ce::Deferred2DRenderLayer::AddDrawable(std::weak_ptr<ce::Drawable> drawable)
{
    if (auto drawableLocked = drawable.lock()) {
        ce::PostProcessTrait* trait = drawableLocked->GetTrait<PostProcessTrait>();
        if (trait != nullptr) {
            this->ppDrawables.insert(*trait);
        }
    }
}
