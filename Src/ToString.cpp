#include "ToString.h"
#include <sstream>

std::string ce::ToString(glm::vec2 vec, const int precision)
{
    std::ostringstream out;
    out.precision(precision);
    out << std::fixed << "(" << vec.x << ", " << vec.y << ")";
    return out.str();
}

std::string ce::ToString(glm::vec3 vec, const int precision)
{
    std::ostringstream out;
    out.precision(precision);
    out << std::fixed << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
    return out.str();
}

std::string ce::ToString(glm::vec4 vec, const int precision)
{
    std::ostringstream out;
    out.precision(precision);
    out << std::fixed << "(" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << ")";
    return out.str();
}

std::string ce::ToString(glm::mat4 mat, const int precision)
{
    std::ostringstream out;
    out.precision(precision);
    
    out << std::fixed << "(" << mat[0].x << ", " << mat[0].y << ", " << mat[0].z << ", " << mat[0].w << ")";
    out << std::fixed << "(" << mat[1].x << ", " << mat[1].y << ", " << mat[1].z << ", " << mat[1].w << ")";
    out << std::fixed << "(" << mat[2].x << ", " << mat[2].y << ", " << mat[2].z << ", " << mat[2].w << ")";
    out << std::fixed << "(" << mat[3].x << ", " << mat[3].y << ", " << mat[3].z << ", " << mat[3].w << ")";
    
    return out.str();
}
