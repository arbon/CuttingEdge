#include "STRigidbody.h"
#include "SmoothTilePhysicsSystem.h"
#include "World.h"
#include "ToString.h"

ce::STRigidbody::STRigidbody(std::weak_ptr<ce::Transform> transform, std::string name) : ce::Component(transform, name), isJumping(true), SmoothTileEnabled(true), isCollidingWithWorld(false)
{
	b2BodyDef* bodyDef = new b2BodyDef();
	bodyDef->type = b2_dynamicBody;
	bodyDef->userData = this;

    auto physics = ce::World::GetWorld()->GetPhysicsSystem<ce::SmoothTilePhysicsSystem>();
    if (physics != nullptr) {
        if (physics->world)
            this->body = physics->world->CreateBody(bodyDef);
    }

    glm::vec3 worldPos = transform.lock()->GetWorldPosition();
    this->body->SetTransform({worldPos.x, worldPos.y}, 0.0f);
}

void ce::STRigidbody::SetVelocity(glm::vec2 velocity)
{
    this->body->SetLinearVelocity({ velocity.x, velocity.y });
}

glm::vec2 ce::STRigidbody::GetVelocity()
{
    return glm::vec2(this->body->GetLinearVelocity().x, this->body->GetLinearVelocity().y);
}

std::weak_ptr<ce::Drawable> ce::STRigidbody::GetDrawable()
{
	return std::weak_ptr<ce::Drawable>();
}

std::weak_ptr<ce::Processable> ce::STRigidbody::GetProcessable()
{
	return std::weak_ptr<ce::Processable>();
}

void ce::STRigidbody::AddStartCollisionCallback(std::function<void(STCollision)> callback)
{
    this->startCollisionCallbacks.push_back(callback);
}

void ce::STRigidbody::AddEndCollisionCallback(std::function<void(STCollision)> callback)
{
    this->endCollisionCallbacks.push_back(callback);
}

void ce::STRigidbody::StartCollide(STCollision collision)
{
    for (auto callback : this->startCollisionCallbacks) {
        callback(collision);
    }
}

void ce::STRigidbody::EndCollide(STCollision collision)
{
    for (auto callback : this->endCollisionCallbacks) {
        callback(collision);
    }
}

void ce::STRigidbody::ApplyAcceleration(glm::vec2 acceleration)
{
    double timeStep = ce::World::GetWorld()->GetPhysicsTickTime();
    b2Vec2 vel = this->body->GetLinearVelocity();
    vel += timeStep * b2Vec2(acceleration.x, acceleration.y);

    this->body->SetLinearVelocity(vel);

    //glm::vec2 velg(vel.x, vel.y);
    //if(velg.x != 0.0f || velg.y != 0.0f) std::cout << ce::ToString(velg) << "\n";
}