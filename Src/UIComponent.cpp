#include "UIComponent.h"
#include "PlatformUtils.h"

ce::UIComponent::UIComponent(std::weak_ptr<Transform> transform, std::string name) : ce::Component(transform, name), loadedDocuments()
{
    // Rocket initialisation.
    this->renderInterface = std::make_shared<ce::LibRocketRenderInterface>();
    RML_CORE::SetRenderInterface(renderInterface.get());

    this->systemInterface = std::make_shared<ce::LibRocketSystemInterface>();
    RML_CORE::SetSystemInterface(systemInterface.get());

    RML_CORE::Initialise();
#if defined(CE_UI_ROCKET)
    RML_CONTROLS::Initialise();
#endif

    glm::vec2 windowSize = ce::World::GetWorld()->GetWindowSize();
    this->context = std::unique_ptr<RML_CORE::Context, RML_CORE::ContextDeleter>(RML_CORE::CreateContext("main", RML_CORE::Vector2i(windowSize.x, windowSize.y)));
    RML_DEBUGGER::Initialise(context.get());

    const std::vector<std::string> fontNames {
        "Delicious-Roman.otf",
        "Delicious-Italic.otf",
        "Delicious-Bold.otf",
        "Delicious-BoldItalic.otf"
    };

    for (std::string fontName : fontNames)
    {
        std::string fontPath = ce::ResolvePath("cutting_edge/Resources/Fonts/" + fontName).string();
        RML_FONT_DATABASE::LoadFontFace(RML_CORE::String(fontPath.c_str()));
    }

    ce::World::GetWorld()->Input.AddRawInputHandler([this](SDL_Event& event) {
        switch (event.type)
        {
            case SDL_MOUSEMOTION:
                this->context->ProcessMouseMove(event.motion.x, event.motion.y, this->systemInterface->GetKeyModifiers());
                break;

            case SDL_MOUSEBUTTONDOWN:
                this->context->ProcessMouseButtonDown(this->systemInterface->TranslateMouseButton(event.button.button), this->systemInterface->GetKeyModifiers());
                if (!this->DoPassMouseInput()) {
                    event = SDL_Event();
                }
                break;

            case SDL_MOUSEBUTTONUP:
                this->context->ProcessMouseButtonUp(this->systemInterface->TranslateMouseButton(event.button.button), this->systemInterface->GetKeyModifiers());
                if (!this->DoPassMouseInput()) {
                    event = SDL_Event();
                }
                break;

            case SDL_MOUSEWHEEL:
                this->context->ProcessMouseWheel(event.wheel.y, this->systemInterface->GetKeyModifiers());
                break;

            case SDL_KEYDOWN:
            {
                // Intercept F2 key stroke to toggle libRocket's 
                // visual debugger tool
                if (event.key.keysym.sym == SDLK_F2)
                {
                    RML_DEBUGGER::SetVisible(!RML_DEBUGGER::IsVisible()); 
                    break;
                }

                this->context->ProcessKeyDown(this->systemInterface->TranslateKey(event.key.keysym.sym), this->systemInterface->GetKeyModifiers());
                break;
            }

            default:
                break;
        }
    });
}

std::weak_ptr<ce::Drawable> ce::UIComponent::GetDrawable()
{
    return std::dynamic_pointer_cast<ce::Drawable>(this->getPointer());
}

std::weak_ptr<ce::Processable> ce::UIComponent::GetProcessable()
{
    return std::dynamic_pointer_cast<ce::Processable>(this->getPointer());
}

void ce::UIComponent::Process()
{
    this->context->Update();
    //Was called twice previously for some reason
    //this->context->Update();
}

void ce::UIComponent::Draw()
{
    this->context->Render();
}

std::weak_ptr<ce::Material> ce::UIComponent::GetMaterial()
{
    return std::weak_ptr<Material>();
}

std::weak_ptr<ce::Component> ce::UIComponent::GetComponent()
{
    return this->getPointer();
}

RML_CORE::ElementDocument* ce::UIComponent::LoadDocument(std::string path)
{
    std::string resolvedPath = ce::ResolvePath(path).string();
    
    RML_CORE::ElementDocument* document = this->context->LoadDocument(RML_CORE::String(resolvedPath.c_str()));
    if (document)
    {
        document->Show();
        this->loadedDocuments[path] = document;
        // Not sure why this was necessary, uncomment if problems with libRocket
        //document->RemoveReference();
    }

    return document;
}

bool ce::UIComponent::DoPassMouseInput()
{
    auto hoverElement = this->context->GetHoverElement();
    if (hoverElement == nullptr) {
        return true;
    }

    if (hoverElement->GetTagName() == "#root") return true;
    if (hoverElement->HasAttribute("pass-input")) return true;

    return false;
}
