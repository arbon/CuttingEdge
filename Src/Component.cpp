#include "Component.h"
#include "Scene.h"

using ce::Component;

ce::Component::Component(std::weak_ptr<ce::Transform> transform, std::string name)
{
    this->transform = transform;
}

std::weak_ptr<ce::Transform> ce::Component::GetTransform() const
{
	return this->transform;
}

void ce::Component::Destroy(std::weak_ptr<ce::Component> component)
{
	this->GetTransform().lock()->GetScene().lock()->DeleteComponent(component);
}

void ce::Component::Destroy(std::weak_ptr<ce::Transform> transform)
{
	this->GetTransform().lock()->GetScene().lock()->DeleteTransform(transform);
}