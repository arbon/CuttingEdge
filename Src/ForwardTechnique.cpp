#include "ForwardTechnique.h"

ce::ForwardTechnique::ForwardTechnique() : ce::RenderTechnique()
{
	this->layers.push_back(std::unique_ptr<ce::ForwardTransparentLayer>(new ce::ForwardTransparentLayer()));
}
