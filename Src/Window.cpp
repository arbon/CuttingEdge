#include "Window.h"
#include "RHI.h"

using namespace ce;

Window::Window(const CreateWindowParameters& params)
{
    auto rhi = params.rhi.lock();
    this->sdlWindow = std::unique_ptr<SDL_Window, sdl_deleter>(
        SDL_CreateWindow(params.name.c_str(), params.x, params.y, params.width, params.height, rhi->GetWindowFlags() | params.sdlFlags));
}

glm::vec2 Window::GetWindowSize()
{
    int width;
    int height;

    SDL_GetWindowSize(this->sdlWindow.get(), &width, &height);

    return glm::vec2(width, height);
}

SDL_Window* Window::GetRawWindow()
{
    return this->sdlWindow.get();
}
