#include "Scene.h"
#include <iostream>
#include "TimeUtil.h"

ce::Scene::Scene() : rootTransforms(), drawables(), processables()
{
    this->activeCamera = std::shared_ptr<ce::Camera>();
    this->defaultTechnique = std::make_unique<ce::ForwardTechnique>();
    this->defaultTechnique->InitializeLayers();
}

ce::Scene::Scene(std::unique_ptr<RenderTechnique> renderTechnique) : rootTransforms(), drawables(), processables(), activeCamera(), defaultTechnique(std::move(renderTechnique))
{
    this->defaultTechnique->InitializeLayers();
}

void ce::Scene::AddDrawable(std::weak_ptr<ce::Drawable> drawable)
{
    auto drawableLocked = drawable.lock();

    if (drawableLocked) {
        this->drawables.push_back(drawableLocked);
        this->defaultTechnique->AddDrawable(drawable);
    }
}

void ce::Scene::AddProcessable(std::weak_ptr<ce::Processable> processable)
{
    auto processableLocked = processable.lock();

    if (processableLocked) {
        this->processables.push_back(processableLocked);
    }
}

void ce::Scene::_DeleteTransform(std::weak_ptr<ce::Transform> transform)
{
    auto transformLocked = transform.lock();

    auto parent = transformLocked->GetParent().lock();

    if (parent) {
        auto& children = parent->GetChildren();
        auto transformIt = std::find(children.begin(), children.end(), transformLocked);
        if (transformIt != children.end()) {
            children.erase(transformIt);
        }
    }
    else {
        auto transformIt = std::find(this->rootTransforms.begin(), this->rootTransforms.end(), transformLocked);
        if (transformIt != this->rootTransforms.end()) {
            this->rootTransforms.erase(transformIt);
        }
    }
}

void ce::Scene::_DeleteComponent(std::weak_ptr<ce::Component> component)
{
    auto componentLocked = component.lock();

    if (!componentLocked) {
        return;
    }

    if (auto transformLocked = componentLocked->GetTransform().lock()) {
        transformLocked->_RemoveComponent(componentLocked);
    }
}

void ce::Scene::Process()
{
    auto i = this->processables.begin();
    while (i != this->processables.end()) {
        if (auto processableLocked = i->lock()) {
            if(processableLocked->shouldProcess) processableLocked->Process();
            ++i;
        }
        else { 
            i = this->processables.erase(i); 
        }
    }

    for (auto transform : this->toDeleteTransforms) {
        this->_DeleteTransform(transform);
    }

    this->toDeleteTransforms.clear();

    for (auto component : this->toDeleteComponents) {
        this->_DeleteComponent(component);
    }
}

void ce::Scene::Draw()
{
    if(auto cameraLocked = this->GetActiveCamera().lock()) {
        this->defaultTechnique->Render(*cameraLocked);
    }
}

void ce::Scene::Draw(ce::RenderTechnique& technique)
{
    if (auto cameraLocked = this->GetActiveCamera().lock()) {
        technique.Render(*cameraLocked);
    }
}

void ce::Scene::Draw(ce::Material& material)
{
	glm::mat4 viewProj = this->activeCamera->GetViewProjectionMatrix();
	std::string mvp_string = "MVP";
	material.Bind();

	for (auto drawableWeak : this->drawables) {
        auto drawable = drawableWeak.lock();
        if (auto componentLocked = drawable->GetComponent().lock()) {
            if (auto transformLocked = componentLocked->GetTransform().lock()) {

                glm::mat4 mvp = transformLocked->CalcMVPMatrix(viewProj);
                material.SetMatrix4(mvp_string, mvp);
                material.UpdateAllUniforms();

                drawable->Draw();
            }
        }
	}

	material.Unbind();
}

std::weak_ptr<ce::Camera> ce::Scene::GetActiveCamera() const
{
	return this->activeCamera;
}

void ce::Scene::SetActiveCamera(std::shared_ptr<ce::Camera> camera)
{
	this->activeCamera = camera;
}

std::weak_ptr<ce::Transform> ce::Scene::CreateTransform(std::weak_ptr<ce::Transform> parent, std::string name)
{
    std::shared_ptr<ce::Transform> newTransform = std::make_shared<Transform>(this->getPointer(), parent, name);

	if (auto parentLocked = parent.lock()) {
        parentLocked->_AddChildRef(newTransform);
    }
    else {
        rootTransforms.push_back(newTransform);
    }

	return newTransform;
}

void ce::Scene::InitComponent(std::weak_ptr<ce::Component> component)
{
    auto componentLocked = component.lock();

    if (!componentLocked) {
        return;
    }

    std::weak_ptr<ce::Drawable> drawable = componentLocked->GetDrawable();

	if (auto drawableLocked = drawable.lock()) {
		this->drawables.push_back(drawableLocked);
	}

    std::weak_ptr<ce::Processable> processable = componentLocked->GetProcessable();

	if (auto processableLocked = processable.lock()) {
		this->processables.push_back(processableLocked);
	}
}

void ce::Scene::DeleteTransform(std::weak_ptr<ce::Transform> transform)
{
    this->toDeleteTransforms.push_back(transform);
}

void ce::Scene::DeleteComponent(std::weak_ptr<ce::Component> component)
{
    this->toDeleteComponents.push_back(component);
}

std::vector<std::weak_ptr<ce::Drawable>> ce::Scene::GetDrawables() const
{
	return this->drawables;
}

const std::unique_ptr<ce::RenderTechnique>& ce::Scene::GetDefaultRenderTechnique() const
{
	return this->defaultTechnique;
}
