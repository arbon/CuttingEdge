#include "LibRocketRenderInterface.h"

ce::LibRocketRenderInterface::LibRocketRenderInterface()
{
    this->shaderProgram = ce::ResourceManager::GetShaderProgram(this->shaderProgName);

    if (!this->shaderProgram) {
        this->shaderProgram = std::make_shared<ce::ShaderProgram>(ce::ShaderProgram(this->shaderProgName));

        this->shaderProgram->shaders.push_back(ce::ResourceManager::LoadShader(this->vertShaderPath));
        this->shaderProgram->shaders.push_back(ce::ResourceManager::LoadShader(this->fragShaderPath));

        ce::ResourceManager::CompileShaderProgram(this->shaderProgram);

        std::string shaderError;

        for (auto test : this->shaderProgram->shaders) {
            auto shaderLocked = test.lock();
            if(shaderLocked) {
                this->shaderProgram->CheckShaderStatus(*shaderLocked, shaderError);
            }
        }

        std::cout << shaderError;
    }

    this->material = std::make_shared<ce::Material>(this->shaderProgram);
}

ce::LibRocketRenderInterface::~LibRocketRenderInterface()
{
}

// Called by Rocket when it wants to compile geometry it believes will be static for the forseeable future.
#if defined(CE_UI_RMLUI)
RML_CORE::CompiledGeometryHandle ce::LibRocketRenderInterface::CompileGeometry(RML_CORE::Span<const RML_CORE::Vertex> vertices_span, RML_CORE::Span<const int> indices_span)
#elif defined(CE_UI_ROCKET)
RML_CORE::CompiledGeometryHandle ce::LibRocketRenderInterface::CompileGeometry(RML_CORE::Vertex* vertices, int num_vertices, int* indices, int num_indices, RML_CORE::TextureHandle texture)
#endif
{
    static int mesh_id = 1;

    std::shared_ptr<ce::Mesh::MeshData> meshData = std::make_shared<ce::Mesh::MeshData>();
    meshData->material = std::make_shared<ce::Material>(this->shaderProgram);

#if defined(CE_UI_ROCKET)
    auto texData = reinterpret_cast<ce::Texture::TextureData*>(texture);
    if (texData != nullptr) {
        meshData->material->SetTexture(this->mainTexUniform, texData->index);
        GLfloat noTexVal = 0;
        meshData->material->SetFloat(this->noTexUniform, noTexVal);
    }
    else {
#endif
        GLuint index = 0;
        GLfloat noTexVal = 1;
        meshData->material->SetFloat(this->noTexUniform, noTexVal);
        meshData->material->SetTexture(this->mainTexUniform, index);
#if defined(CE_UI_ROCKET)
    }
#endif

#if defined(CE_UI_RMLUI)
    const RML_CORE::Vertex* vertices = vertices_span.data();
    int num_vertices = vertices_span.size();
    const int* indices = indices_span.data();
    int num_indices = indices_span.size();
#endif

    meshData->elementsCount = num_indices;
    meshData->elements.assign(indices, indices + num_indices);

    meshData->verticesCount = num_vertices;
    for (int i = 0; i < num_vertices; ++i)
    {
        ce::Vertex vertex;

        vertex.position = glm::vec3(vertices[i].position.x, vertices[i].position.y, 0.0f);
        vertex.color = glm::vec4(vertices[i].colour.red / 255.0f, vertices[i].colour.green / 255.0f, vertices[i].colour.blue / 255.0f, vertices[i].colour.alpha / 255.0f);
        vertex.uv = glm::vec2(vertices[i].tex_coord[0], vertices[i].tex_coord[1]);

        meshData->vertices.push_back(vertex);
    }

    meshData->name = "ui-mgen-" + std::to_string(mesh_id);
    mesh_id++;

    ce::ResourceManager::AddMeshData(meshData);
    ce::ResourceManager::BufferMesh(meshData);

    return reinterpret_cast<RML_CORE::CompiledGeometryHandle>(meshData.get());
}

// Called by Rocket when it wants to render application-compiled geometry.
#if defined(CE_UI_RMLUI)
void ce::LibRocketRenderInterface::RenderGeometry(RML_CORE::CompiledGeometryHandle geometry, RML_CORE::Vector2f translation, RML_CORE::TextureHandle texture)
#elif defined(CE_UI_ROCKET)
void ce::LibRocketRenderInterface::RenderCompiledGeometry(RML_CORE::CompiledGeometryHandle geometry, const RML_CORE::Vector2f& translation)
#endif
{
    auto meshData = reinterpret_cast<ce::Mesh::MeshData*>(geometry);

    glm::vec2 window = ce::World::GetWorld()->GetWindowSize();
    glm::mat4 proj = glm::ortho(0.0f, window.x, window.y, 0.0f, -1.0f, 10000.0f);
    glm::mat4 translationMat = glm::translate(glm::mat4(1.0f), glm::vec3(translation.x, translation.y, 0.0f));
    translationMat = proj * translationMat;

#if defined(CE_UI_RMLUI)
    auto texData = reinterpret_cast<ce::Texture::TextureData*>(texture);
    if (texData != nullptr) {
        meshData->material->SetTexture(this->mainTexUniform, texData->index);
        GLfloat noTexVal = 0;
        meshData->material->SetFloat(this->noTexUniform, noTexVal);
    }
#endif
    meshData->material->SetMatrix4(this->translationUniform, translationMat);
    meshData->material->Bind();
    meshData->material->UpdateAllUniforms();

    glBindVertexArray(meshData->vao);

    glDrawElements(GL_TRIANGLES, meshData->elementsCount, GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);

    meshData->material->Unbind();
}

// Called by Rocket when it wants to release application-compiled geometry.
#if defined(CE_UI_RMLUI)
void ce::LibRocketRenderInterface::ReleaseGeometry(RML_CORE::CompiledGeometryHandle geometry)
#elif defined(CE_UI_ROCKET)
void ce::LibRocketRenderInterface::ReleaseCompiledGeometry(RML_CORE::CompiledGeometryHandle geometry)
#endif
{
    auto meshData = reinterpret_cast<ce::Mesh::MeshData*>(geometry);

    ce::ResourceManager::DeleteMeshData(meshData->name);
}

// Called by Rocket when it wants to enable or disable scissoring to clip content.
void ce::LibRocketRenderInterface::EnableScissorRegion(bool enable)
{
    if (!enable)
        glDisable(GL_SCISSOR_TEST);
    else
        glEnable(GL_SCISSOR_TEST);
}

// Called by Rocket when it wants to change the scissor region.
#if defined(CE_UI_RMLUI)
void ce::LibRocketRenderInterface::SetScissorRegion(RML_CORE::Rectanglei region)
#elif defined(CE_UI_ROCKET)
void ce::LibRocketRenderInterface::SetScissorRegion(int x, int y, int width, int height)
#endif
{
#if defined(CE_UI_RMLUI)
    int x = region.Left();
    int y = region.Top();
    int width = region.Width();
    int height = region.Height();
#endif

    glm::vec2 window = ce::World::GetWorld()->GetWindowSize();

    glScissor(x, window.y - (y + height), width, height);
}

// Called by Rocket when a texture is required by the library.
#if defined(CE_UI_RMLUI)
RML_CORE::TextureHandle ce::LibRocketRenderInterface::LoadTexture(RML_CORE::Vector2i& texture_dimensions, const RML_CORE::String& source)
#elif defined(CE_UI_ROCKET)
bool ce::LibRocketRenderInterface::LoadTexture(RML_CORE::TextureHandle& texture_handle, RML_CORE::Vector2i& texture_dimensions, const RML_CORE::String& source)
#endif
{
    auto texture = ce::ResourceManager::LoadTexture(RML_CORE::GetRmlCString(source));
    ce::ResourceManager::BufferTexture(texture->data);

    if (!texture)
    {
        return 0;
    }

    texture_dimensions.x = texture->data->width;
    texture_dimensions.y = texture->data->height;

#if defined(CE_UI_RMLUI)
    return reinterpret_cast<RML_CORE::TextureHandle>(texture->data.get());
#elif defined(CE_UI_ROCKET)
    texture_handle = reinterpret_cast<RML_CORE::TextureHandle>(texture->data.get());
    return true;
#endif
}

// Called by Rocket when a texture is required to be built from an internally-generated sequence of pixels.
#if defined(CE_UI_RMLUI)
RML_CORE::TextureHandle ce::LibRocketRenderInterface::GenerateTexture(RML_CORE::Span<const RML_CORE::byte> source_span, RML_CORE::Vector2i source_dimensions)
#elif defined(CE_UI_ROCKET)
bool ce::LibRocketRenderInterface::GenerateTexture(RML_CORE::TextureHandle& texture_handle, const RML_CORE::byte* source, const RML_CORE::Vector2i& source_dimensions)
#endif
{
    static int texture_id = 1;

#if defined(CE_UI_RMLUI)
    const RML_CORE::byte* source = source_span.data();
#endif

    std::shared_ptr<ce::Texture::TextureData> texData = std::make_shared<ce::Texture::TextureData>();
    texData->width = source_dimensions.x;
    texData->height = source_dimensions.y;
    texData->name = "ui-gen-" + std::to_string(texture_id);
    texData->data = source; 
    texData->components = 4;

    std::shared_ptr<ce::Texture> texture = ce::ResourceManager::LoadTexture(std::move(texData));
    ce::ResourceManager::BufferTexture(texture->data);

    texture_id++;

#if defined(CE_UI_RMLUI)
    return reinterpret_cast<RML_CORE::TextureHandle>(texture->data.get());
#elif defined(CE_UI_ROCKET)
    texture_handle = reinterpret_cast<RML_CORE::TextureHandle>(texture->data.get());
    return true;
#endif
}

// Called by Rocket when a loaded texture is no longer required.
void ce::LibRocketRenderInterface::ReleaseTexture(RML_CORE::TextureHandle texture)
{
    auto texData = reinterpret_cast<ce::Texture::TextureData*>(texture);

    ce::ResourceManager::DeleteTexture(texData->name);
}

#if defined(CE_UI_ROCKET)
    // Called by Rocket when it wants to render geometry that it does not wish to optimise.
    void ce::LibRocketRenderInterface::RenderGeometry(RML_CORE::Vertex* vertices, int num_vertices, int* indices, int num_indices, RML_CORE::TextureHandle texture, const RML_CORE::Vector2f& translation)
    {
        auto texData = reinterpret_cast<ce::Texture::TextureData*>(texture);
        GLuint texIndex = texData == nullptr ? 0 : texData->index;
        this->material->SetTexture(this->mainTexUniform, texIndex);

        glm::vec2 window = ce::World::GetWorld()->GetWindowSize();
        glm::mat4 proj = glm::ortho(0.0f, window.x, window.y, 0.0f, -1.0f, 10000.0f);
        glm::mat4 translationMat = glm::translate(glm::mat4(1.0f), glm::vec3(translation.x, translation.y, 0.0f));
        translationMat = proj * translationMat;
        this->material->SetMatrix4(this->translationUniform, translationMat);

        GLfloat noTexVal = texIndex == 0;
        this->material->SetFloat(this->noTexUniform, noTexVal);

        this->material->Bind();
        this->material->UpdateAllUniforms();

        std::vector<RML_CORE::Vector2f> Positions(num_vertices);
        std::vector<RML_VERTEX_COLOUR_TYPE> Colors(num_vertices);
        std::vector<RML_CORE::Vector2f> TexCoords(num_vertices);

        for (int i = 0; i < num_vertices; i++) {
            Positions[i] = vertices[i].position;
            Colors[i] = vertices[i].colour;
            TexCoords[i] = vertices[i].tex_coord;
        };

        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
        glVertexPointer(2, GL_FLOAT, 0, &Positions[0]);
        glColorPointer(4, GL_UNSIGNED_BYTE, 0, &Colors[0]);
        glTexCoordPointer(2, GL_FLOAT, 0, &TexCoords[0]);

        glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_INT, indices);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);

        this->material->Unbind();
    }

    // Returns the native horizontal texel offset for the renderer.
    float ce::LibRocketRenderInterface::GetHorizontalTexelOffset()
    {
        //return -render_system->getHorizontalTexelOffset();
        return 0.0f;
    }

    // Returns the native vertical texel offset for the renderer.
    float ce::LibRocketRenderInterface::GetVerticalTexelOffset()
    {
        //return -render_system->getVerticalTexelOffset();
        return 0.0f;
    }
#endif

