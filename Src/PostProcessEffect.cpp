#include "PostProcessEffect.h"

ce::PostProcessEffect::PostProcessEffect()
{
    this->meshData = ce::ResourceManager::LoadPrimitiveGeometry(ce::ResourceManager::GEOM_SCREEN_QUAD);
    ce::ResourceManager::BufferMesh(this->meshData);

    this->traits.push_back(new ce::PostProcessTrait());
}

void ce::PostProcessEffect::Draw()
{
    glBindVertexArray(this->meshData->vao);

    glDrawElements(GL_TRIANGLES, this->meshData->elementsCount, GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);
}

std::weak_ptr<ce::Material> ce::PostProcessEffect::GetMaterial()
{
    return this->material;
}

std::weak_ptr<ce::Component> ce::PostProcessEffect::GetComponent()
{
    return this->component;
}
