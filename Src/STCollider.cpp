#include "STCollider.h"
#include "SmoothTilePhysicsSystem.h"

ce::STCollider::STCollider(std::weak_ptr<ce::Transform> transform, std::weak_ptr<ce::SmoothTilePhysicsSystem> physics, std::shared_ptr<b2Shape> shape, std::shared_ptr<b2Body> body, std::string name) : ce::Component(transform, name)
{
    auto physicsLocked = physics.lock();

    if (!physicsLocked || !shape || !body)
        return;

	b2FixtureDef *fixtureDef = new b2FixtureDef();
	fixtureDef->shape = shape.get();
	fixtureDef->density = 1.0f;

	if (body == nullptr) {
		b2BodyDef* bodyDef = new b2BodyDef();
		bodyDef->type = b2_staticBody;
		bodyDef->userData = this;
		b2Body* newBody = physicsLocked->world->CreateBody(bodyDef);
		this->fixture = std::shared_ptr<b2Fixture>(newBody->CreateFixture(fixtureDef));
	}
	else {
        this->fixture = std::shared_ptr<b2Fixture>(body->CreateFixture(fixtureDef));
	}
}

std::weak_ptr<ce::Drawable> ce::STCollider::GetDrawable()
{
	return std::weak_ptr<ce::Drawable>();
}

std::weak_ptr<ce::Processable> ce::STCollider::GetProcessable()
{
	return std::weak_ptr<ce::Processable>();
}
