#include "PostProcessTrait.h"

ce::PostProcessTrait::PostProcessTrait() : priority(20)
{
}

int ce::PostProcessTrait::GetPriority() const
{
    return this->priority;
}

void ce::PostProcessTrait::SetPriority(int priority)
{
    this->priority = priority;
}

bool ce::PostProcessTrait::operator<(const ce::DrawableTrait & rhs) const
{
    
    return this->GetPriority() < reinterpret_cast<PostProcessTrait const&>(rhs).GetPriority();
}
