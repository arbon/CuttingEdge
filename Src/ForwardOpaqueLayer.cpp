#include "ForwardOpaqueLayer.h"

void ce::ForwardOpaqueLayer::Render(ce::Camera& camera)
{
	glm::mat4 viewProj = camera.GetViewProjectionMatrix();
	std::string mvp_string = "MVP";
	glm::mat4 mvp;

	for (auto drawableWeak : this->drawables) {
        auto drawable = drawableWeak.lock();
        auto materialLocked = drawable->GetMaterial().lock();
        auto componentLocked = drawable->GetComponent().lock();

        if (!materialLocked || !componentLocked) {
            continue;
        }

        auto transformLocked = componentLocked->GetTransform().lock();

        if (!transformLocked) {
            continue;
        }

		mvp = transformLocked->CalcMVPMatrix(viewProj);

        materialLocked->Bind();
        materialLocked->SetMatrix4(mvp_string, mvp);
        materialLocked->UpdateAllUniforms();
		//		this->activeCamera->GetProjectionMatrix() * this->activeCamera->GetViewMatrix() * drawable->GetComponent()->GetTransform()->GetWorldMatrix();
		//		glUniformMatrix4fv(0, 1, GL_FALSE, &(this->activeCamera->GetProjectionMatrix() * this->activeCamera->GetViewMatrix() * drawable->GetComponent()->GetTransform()->GetWorldMatrix())[0][0]);
		//		glUniformMatrix4fv(0, 1, GL_FALSE, &drawable->GetComponent()->GetTransform()->CalcMVPMatrix(this->activeCamera)[0][0]);
		//		glUniformMatrix4fv(material->shader->propertyMap["MVP"].first, 1, GL_FALSE, &mvp[0][0]);
		//		glUniformMatrix4fv(0, 1, GL_FALSE, &mvp[0][0]);

		drawable->Draw();
        materialLocked->Unbind();
	}
}
