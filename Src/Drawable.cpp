#include "Drawable.h"
#include "Component.h"

void ce::Drawable::_SetComponent(std::weak_ptr<Component> component)
{
    this->component = component;
    auto drawablePointer = this->component.lock()->GetDrawable();
    for (auto trait : this->traits) {
        trait->drawable = drawablePointer;
    }
}
