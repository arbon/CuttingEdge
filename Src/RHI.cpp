#include "RHI.h"

#include <algorithm>
#include <set>
#include <iostream>
#include <optional>
#include <limits>
#include <cstdint>
#include <filesystem>
#include <iostream>
#include <fstream>

#include <shaderc/shaderc.hpp>
#include "spirv_reflect.h"
#include "common/output_stream.h"
#include "SDL_vulkan.h"
#include "gl_includes.h"
#include "Window.h"
#include "ShaderProgram.h"

using namespace ce;

// From Libs/SPIRV-Reflect/examples/main_io_variables.cpp
static uint32_t FormatSize(VkFormat format)
{
  uint32_t result = 0;
  switch (format) {
  case VK_FORMAT_UNDEFINED: result = 0; break;
  case VK_FORMAT_R4G4_UNORM_PACK8: result = 1; break;
  case VK_FORMAT_R4G4B4A4_UNORM_PACK16: result = 2; break;
  case VK_FORMAT_B4G4R4A4_UNORM_PACK16: result = 2; break;
  case VK_FORMAT_R5G6B5_UNORM_PACK16: result = 2; break;
  case VK_FORMAT_B5G6R5_UNORM_PACK16: result = 2; break;
  case VK_FORMAT_R5G5B5A1_UNORM_PACK16: result = 2; break;
  case VK_FORMAT_B5G5R5A1_UNORM_PACK16: result = 2; break;
  case VK_FORMAT_A1R5G5B5_UNORM_PACK16: result = 2; break;
  case VK_FORMAT_R8_UNORM: result = 1; break;
  case VK_FORMAT_R8_SNORM: result = 1; break;
  case VK_FORMAT_R8_USCALED: result = 1; break;
  case VK_FORMAT_R8_SSCALED: result = 1; break;
  case VK_FORMAT_R8_UINT: result = 1; break;
  case VK_FORMAT_R8_SINT: result = 1; break;
  case VK_FORMAT_R8_SRGB: result = 1; break;
  case VK_FORMAT_R8G8_UNORM: result = 2; break;
  case VK_FORMAT_R8G8_SNORM: result = 2; break;
  case VK_FORMAT_R8G8_USCALED: result = 2; break;
  case VK_FORMAT_R8G8_SSCALED: result = 2; break;
  case VK_FORMAT_R8G8_UINT: result = 2; break;
  case VK_FORMAT_R8G8_SINT: result = 2; break;
  case VK_FORMAT_R8G8_SRGB: result = 2; break;
  case VK_FORMAT_R8G8B8_UNORM: result = 3; break;
  case VK_FORMAT_R8G8B8_SNORM: result = 3; break;
  case VK_FORMAT_R8G8B8_USCALED: result = 3; break;
  case VK_FORMAT_R8G8B8_SSCALED: result = 3; break;
  case VK_FORMAT_R8G8B8_UINT: result = 3; break;
  case VK_FORMAT_R8G8B8_SINT: result = 3; break;
  case VK_FORMAT_R8G8B8_SRGB: result = 3; break;
  case VK_FORMAT_B8G8R8_UNORM: result = 3; break;
  case VK_FORMAT_B8G8R8_SNORM: result = 3; break;
  case VK_FORMAT_B8G8R8_USCALED: result = 3; break;
  case VK_FORMAT_B8G8R8_SSCALED: result = 3; break;
  case VK_FORMAT_B8G8R8_UINT: result = 3; break;
  case VK_FORMAT_B8G8R8_SINT: result = 3; break;
  case VK_FORMAT_B8G8R8_SRGB: result = 3; break;
  case VK_FORMAT_R8G8B8A8_UNORM: result = 4; break;
  case VK_FORMAT_R8G8B8A8_SNORM: result = 4; break;
  case VK_FORMAT_R8G8B8A8_USCALED: result = 4; break;
  case VK_FORMAT_R8G8B8A8_SSCALED: result = 4; break;
  case VK_FORMAT_R8G8B8A8_UINT: result = 4; break;
  case VK_FORMAT_R8G8B8A8_SINT: result = 4; break;
  case VK_FORMAT_R8G8B8A8_SRGB: result = 4; break;
  case VK_FORMAT_B8G8R8A8_UNORM: result = 4; break;
  case VK_FORMAT_B8G8R8A8_SNORM: result = 4; break;
  case VK_FORMAT_B8G8R8A8_USCALED: result = 4; break;
  case VK_FORMAT_B8G8R8A8_SSCALED: result = 4; break;
  case VK_FORMAT_B8G8R8A8_UINT: result = 4; break;
  case VK_FORMAT_B8G8R8A8_SINT: result = 4; break;
  case VK_FORMAT_B8G8R8A8_SRGB: result = 4; break;
  case VK_FORMAT_A8B8G8R8_UNORM_PACK32: result = 4; break;
  case VK_FORMAT_A8B8G8R8_SNORM_PACK32: result = 4; break;
  case VK_FORMAT_A8B8G8R8_USCALED_PACK32: result = 4; break;
  case VK_FORMAT_A8B8G8R8_SSCALED_PACK32: result = 4; break;
  case VK_FORMAT_A8B8G8R8_UINT_PACK32: result = 4; break;
  case VK_FORMAT_A8B8G8R8_SINT_PACK32: result = 4; break;
  case VK_FORMAT_A8B8G8R8_SRGB_PACK32: result = 4; break;
  case VK_FORMAT_A2R10G10B10_UNORM_PACK32: result = 4; break;
  case VK_FORMAT_A2R10G10B10_SNORM_PACK32: result = 4; break;
  case VK_FORMAT_A2R10G10B10_USCALED_PACK32: result = 4; break;
  case VK_FORMAT_A2R10G10B10_SSCALED_PACK32: result = 4; break;
  case VK_FORMAT_A2R10G10B10_UINT_PACK32: result = 4; break;
  case VK_FORMAT_A2R10G10B10_SINT_PACK32: result = 4; break;
  case VK_FORMAT_A2B10G10R10_UNORM_PACK32: result = 4; break;
  case VK_FORMAT_A2B10G10R10_SNORM_PACK32: result = 4; break;
  case VK_FORMAT_A2B10G10R10_USCALED_PACK32: result = 4; break;
  case VK_FORMAT_A2B10G10R10_SSCALED_PACK32: result = 4; break;
  case VK_FORMAT_A2B10G10R10_UINT_PACK32: result = 4; break;
  case VK_FORMAT_A2B10G10R10_SINT_PACK32: result = 4; break;
  case VK_FORMAT_R16_UNORM: result = 2; break;
  case VK_FORMAT_R16_SNORM: result = 2; break;
  case VK_FORMAT_R16_USCALED: result = 2; break;
  case VK_FORMAT_R16_SSCALED: result = 2; break;
  case VK_FORMAT_R16_UINT: result = 2; break;
  case VK_FORMAT_R16_SINT: result = 2; break;
  case VK_FORMAT_R16_SFLOAT: result = 2; break;
  case VK_FORMAT_R16G16_UNORM: result = 4; break;
  case VK_FORMAT_R16G16_SNORM: result = 4; break;
  case VK_FORMAT_R16G16_USCALED: result = 4; break;
  case VK_FORMAT_R16G16_SSCALED: result = 4; break;
  case VK_FORMAT_R16G16_UINT: result = 4; break;
  case VK_FORMAT_R16G16_SINT: result = 4; break;
  case VK_FORMAT_R16G16_SFLOAT: result = 4; break;
  case VK_FORMAT_R16G16B16_UNORM: result = 6; break;
  case VK_FORMAT_R16G16B16_SNORM: result = 6; break;
  case VK_FORMAT_R16G16B16_USCALED: result = 6; break;
  case VK_FORMAT_R16G16B16_SSCALED: result = 6; break;
  case VK_FORMAT_R16G16B16_UINT: result = 6; break;
  case VK_FORMAT_R16G16B16_SINT: result = 6; break;
  case VK_FORMAT_R16G16B16_SFLOAT: result = 6; break;
  case VK_FORMAT_R16G16B16A16_UNORM: result = 8; break;
  case VK_FORMAT_R16G16B16A16_SNORM: result = 8; break;
  case VK_FORMAT_R16G16B16A16_USCALED: result = 8; break;
  case VK_FORMAT_R16G16B16A16_SSCALED: result = 8; break;
  case VK_FORMAT_R16G16B16A16_UINT: result = 8; break;
  case VK_FORMAT_R16G16B16A16_SINT: result = 8; break;
  case VK_FORMAT_R16G16B16A16_SFLOAT: result = 8; break;
  case VK_FORMAT_R32_UINT: result = 4; break;
  case VK_FORMAT_R32_SINT: result = 4; break;
  case VK_FORMAT_R32_SFLOAT: result = 4; break;
  case VK_FORMAT_R32G32_UINT: result = 8; break;
  case VK_FORMAT_R32G32_SINT: result = 8; break;
  case VK_FORMAT_R32G32_SFLOAT: result = 8; break;
  case VK_FORMAT_R32G32B32_UINT: result = 12; break;
  case VK_FORMAT_R32G32B32_SINT: result = 12; break;
  case VK_FORMAT_R32G32B32_SFLOAT: result = 12; break;
  case VK_FORMAT_R32G32B32A32_UINT: result = 16; break;
  case VK_FORMAT_R32G32B32A32_SINT: result = 16; break;
  case VK_FORMAT_R32G32B32A32_SFLOAT: result = 16; break;
  case VK_FORMAT_R64_UINT: result = 8; break;
  case VK_FORMAT_R64_SINT: result = 8; break;
  case VK_FORMAT_R64_SFLOAT: result = 8; break;
  case VK_FORMAT_R64G64_UINT: result = 16; break;
  case VK_FORMAT_R64G64_SINT: result = 16; break;
  case VK_FORMAT_R64G64_SFLOAT: result = 16; break;
  case VK_FORMAT_R64G64B64_UINT: result = 24; break;
  case VK_FORMAT_R64G64B64_SINT: result = 24; break;
  case VK_FORMAT_R64G64B64_SFLOAT: result = 24; break;
  case VK_FORMAT_R64G64B64A64_UINT: result = 32; break;
  case VK_FORMAT_R64G64B64A64_SINT: result = 32; break;
  case VK_FORMAT_R64G64B64A64_SFLOAT: result = 32; break;
  case VK_FORMAT_B10G11R11_UFLOAT_PACK32: result = 4; break;
  case VK_FORMAT_E5B9G9R9_UFLOAT_PACK32: result = 4; break;

  default:
    break;
  }
  return result;
}



RHI_Error OpenGLRHI::InitialiseRHI(Window* window)
{
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    this->sdlContext = SDL_GL_CreateContext(window->GetRawWindow());

    glewExperimental = GL_TRUE;

    glewInit();

    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    SDL_GL_SetSwapInterval(0);

    return RHI_Error::OK;
}

RHI_Error OpenGLRHI::CleanupRHI()
{
    SDL_GL_DeleteContext(this->sdlContext);
    return RHI_Error::OK;
}

RHI_Error OpenGLRHI::LoadShader(ShaderProgram::Shader& shader)
{
    std::filesystem::path absPath = shader.path;

    std::error_code error;
    if(!std::filesystem::exists(absPath, error))
    {
        return RHI_Error::FAILURE;
    }

    std::ifstream sourceStream(absPath.string(), std::ios::ate | std::ios::binary);

    size_t size = sourceStream.tellg();
    shader.source = std::vector<GLchar>(size + 1);
    sourceStream.seekg(0);
    sourceStream.read(shader.source.data(), size);
    shader.source[size] = '\0';

    shader.glData = new OpenGLShaderData();

    if (shader.type == ce::ShaderProgram::ShaderType::VERTEX_SHADER)
    {
        shader.glData->type = GL_VERTEX_SHADER;
    }
    else if (shader.type == ce::ShaderProgram::ShaderType::FRAGMENT_SHADER)
    {
        shader.glData->type = GL_FRAGMENT_SHADER;
    }
    else if (shader.type == ce::ShaderProgram::ShaderType::GEOMETRY_SHADER)
    {
        shader.glData->type = GL_GEOMETRY_SHADER;
    }

    //std::cout << "Loaded shader: " << shader.path << ", type: " << (unsigned int)shader.type << std::endl;

    return this->CompileShader(shader);
}

RHI_Error OpenGLRHI::CompileShader(ShaderProgram::Shader& shader)
{
    if(!shader.glData) return RHI_Error::FAILURE;

	if(shader.glData->id)
	{
		return RHI_Error::OK;
	}

    const char* shaderSourcePtr = shader.source.data();

	shader.glData->id = glCreateShader(shader.glData->type);
	glShaderSource(shader.glData->id, 1, &shaderSourcePtr, NULL);
	glCompileShader(shader.glData->id);

    GLint compile_status = 0;
    glGetShaderiv(shader.glData->id, GL_COMPILE_STATUS, &compile_status);

    if(!compile_status) {
        GLint log_length = 0;
        glGetShaderiv(shader.glData->id, GL_INFO_LOG_LENGTH, &log_length);
        
        std::string log;
        log.resize(log_length);
        glGetShaderInfoLog(shader.glData->id, log_length, NULL, &log[0]);

        std::cout << "Failed to compile shader " << shader.path << std::endl << log << std::endl;
    }

    std::cout << "Compiled " << shader.glData->type << " shader #" << shader.glData->id << std::endl;

    return RHI_Error::OK;
}

RHI_Error OpenGLRHI::CompileShaderProgram(ShaderProgram& program)
{
	program.programId = glCreateProgram();

    if(program.programId == 0) {
        std::cout << "Failed initial shader program creation" << std::endl;
        return RHI_Error::INIT_ERROR;
    }

	for(auto shader : program.shaders)
	{
        auto shaderLocked = shader.lock();
        std::cout << "Attaching shader #" << shaderLocked->glData->id << ": " << shaderLocked->path << std::endl;
		glAttachShader(program.programId, shaderLocked->glData->id);
	}

	// glBindFragDataLocation(program.programId, 0, "outColor");
    // glBindFragDataLocation(program.programId, 0, "diffuse");
    // glBindFragDataLocation(program.programId, 1, "surfacenormal");
	glLinkProgram(program.programId);

    GLenum err;
    while((err = glGetError()) != GL_NO_ERROR)
    {
        std::cout << "Error during shader program compilation: " << err << std::endl;
        return RHI_Error::INIT_ERROR;
    }

	GLint isLinked = 0;
	glGetProgramiv(program.programId, GL_LINK_STATUS, &isLinked);

    if(isLinked == GL_FALSE) {
        GLint log_length = 0;
	    glGetProgramiv(program.programId, GL_INFO_LOG_LENGTH, &log_length);
        
        std::string log;
        log.resize(log_length);
        glGetShaderInfoLog(program.programId, log_length, NULL, &log[0]);

        GLint num_attached_shaders;
	    glGetProgramiv(program.programId, GL_ATTACHED_SHADERS, &num_attached_shaders);

        std::cout << "Failed to compile shader program: " << program.name << " (" << num_attached_shaders << " shaders attached)" << std::endl << log << std::endl;

        return RHI_Error::INIT_ERROR;
    }

	GLint numBlocks;
	glGetProgramiv(program.programId, GL_ACTIVE_UNIFORMS, &numBlocks);

	GLint nameLen;
	glGetProgramiv(program.programId, GL_ACTIVE_UNIFORM_MAX_LENGTH, &nameLen);

	std::vector<GLchar> name;
	name.resize(nameLen);

	program.properties.resize(numBlocks);

	for (int blockIx = 0; blockIx < numBlocks; ++blockIx)
	{
		GLint size;
		GLenum type;
		glGetActiveUniform(program.programId, blockIx, nameLen, NULL, &size, &type, &name[0]);
        std::cout << "Uniform: " << name.data() << std::endl;

		program.properties[blockIx] = { std::string(&name[0]), type };
	}

	return isLinked > 0 ? RHI_Error::OK : RHI_Error::INIT_ERROR;
}

Uint32 OpenGLRHI::GetWindowFlags()
{
    return SDL_WINDOW_OPENGL;
}

RHI_Error OpenGLRHI::DrawFrame(Window* window, std::function<void(void)> drawSceneFunc)
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    drawSceneFunc();

    SDL_GL_SwapWindow(window->GetRawWindow());

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::InitialiseRHI(Window* window)
{
    RHI_Error result;
    
    result = this->CreateInstance(window);
    if(result != RHI_Error::OK) return result;

    result = this->CreateSurface(window);
    if(result != RHI_Error::OK) return result;

    result = this->SelectPhysicalDevice();
    if(result != RHI_Error::OK) return result;

    result = this->CreateLogicalDevice();
    if(result != RHI_Error::OK) return result;

    result = this->CreateSwapChain();
    if(result != RHI_Error::OK) return result;

    result = this->CreateSwapChainImageViews();
    if(result != RHI_Error::OK) return result;

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CleanupRHI()
{
    vkDestroyCommandPool(this->device, this->commandPool, nullptr);

    for (auto framebuffer : this->swapChainFramebuffers) {
        vkDestroyFramebuffer(this->device, framebuffer, nullptr);
    }

    for (auto imageView : this->swapChainImageViews) {
        vkDestroyImageView(this->device, imageView, nullptr);
    }

    vkDestroySwapchainKHR(this->device, this->swapChain, nullptr);
    vkDestroyDevice(this->device, nullptr);
    vkDestroySurfaceKHR(this->instance, this->surface, nullptr);
    vkDestroyInstance(this->instance, nullptr);

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::LoadShader(ShaderProgram::Shader& shader)
{
    std::filesystem::path absPath = shader.path;
    //std::filesystem::path spvPath = absPath.concat(".spv");

    std::error_code error;
    if(!std::filesystem::exists(absPath, error))
    {
        return RHI_Error::FAILURE;
    }

    std::ifstream sourceStream(absPath.string(), std::ios::ate | std::ios::binary);

    size_t size = sourceStream.tellg();
    shader.source = std::vector<char>(size);
    sourceStream.seekg(0);
    sourceStream.read(shader.source.data(), size);

    shader.vulkanData = new VulkanShaderData();

    RHI_Error result = this->CompileShader(shader);
    if (result != RHI_Error::OK)
    {
        return result;
    }

    return this->PopulateShaderDescriptorInfo(shader);
}

RHI_Error VulkanRHI::CompileShader(ShaderProgram::Shader& shader)
{
    shaderc::Compiler compiler;
    shaderc::CompileOptions options;
    shaderc_shader_kind shaderType;

    if (shader.type == ce::ShaderProgram::ShaderType::VERTEX_SHADER)
    {
        shaderType = shaderc_shader_kind::shaderc_glsl_vertex_shader;
    }
    else if (shader.type == ce::ShaderProgram::ShaderType::FRAGMENT_SHADER)
    {
        shaderType = shaderc_shader_kind::shaderc_glsl_fragment_shader;
    }
    else if (shader.type == ce::ShaderProgram::ShaderType::GEOMETRY_SHADER)
    {
        shaderType = shaderc_shader_kind::shaderc_glsl_geometry_shader;
    }

//   SpvCompilationResult CompileGlslToSpv(const char* source_text,
//                                         size_t source_text_size,
//                                         shaderc_shader_kind shader_kind,
//                                         const char* input_file_name, - this is just a name to tag the shader with
//                                         const CompileOptions& options

    shaderc::SpvCompilationResult result = compiler.CompileGlslToSpv(
        shader.source.data(), shader.source.size(), 
        shaderc_shader_kind::shaderc_glsl_vertex_shader, 
        shader.path.filename().string().c_str(), options);
    
    if (result.GetCompilationStatus() != shaderc_compilation_status_success) {
        std::cout << "Failed to compile shader " << shader.path << std::endl 
        << "  Shader compilation output: " << result.GetErrorMessage() << std::endl;
        return RHI_Error::FAILURE;
    }
    
    shader.vulkanData->byteCode.assign(result.cbegin(), result.cend());
    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateShaderModule(ShaderProgram::Shader& shader)
{
    VkShaderModuleCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = shader.vulkanData->byteCode.size() * 4; // size in bytes
    createInfo.pCode = shader.vulkanData->byteCode.data();

    VkResult result = vkCreateShaderModule(this->device, &createInfo, nullptr, &shader.vulkanData->shaderModule);
    
    if(result != VK_SUCCESS)
    {
        return RHI_Error::FAILURE;
    }

    return RHI_Error::OK;
}


void ParseVariableType(VulkanShaderAttributeData& attr_desc, const SpvReflectTypeDescription& type_desc, bool logDetails, const std::string& logPrefix)
{
    switch(type_desc.op)
    {
        case SpvOpTypeVoid:
        {
            std::cout << logPrefix << "UNKOWN TYPE:" << type_desc.op << std::endl;
            break;
        }
        case SpvOpTypeBool:
        {
            attr_desc.type.type_group = ce::GLSLTypeGroups::SCALAR;
            attr_desc.type.scalar_type = ce::GLSLScalarType::BOOL;
            break;
        }
        case SpvOpTypeInt:
        {
            attr_desc.type.type_group = ce::GLSLTypeGroups::SCALAR;
            attr_desc.type.scalar_type = 
                type_desc.traits.numeric.scalar.signedness
                ? ce::GLSLScalarType::INT
                : ce::GLSLScalarType::UINT;
            break;
        }
        case SpvOpTypeFloat:
        {
            attr_desc.type.type_group = ce::GLSLTypeGroups::SCALAR;
            switch (type_desc.traits.numeric.scalar.width) {
                case 32:
                    attr_desc.type.scalar_type = ce::GLSLScalarType::FLOAT;
                    break;
                case 64:
                    attr_desc.type.scalar_type = ce::GLSLScalarType::DOUBLE;
                    break;
                default:
                    std::cout << logPrefix << "TYPE:" << type_desc.op << ", invalid width: " << type_desc.traits.numeric.scalar.width << std::endl;
                    break;
            }
            break;
        }
        case SpvOpTypeVector:
        {
            attr_desc.type.type_group = ce::GLSLTypeGroups::VECTOR;
            attr_desc.type.num_components[0] = type_desc.traits.numeric.vector.component_count;
            break;
        }
        case SpvOpTypeMatrix:
        {
            attr_desc.type.type_group = ce::GLSLTypeGroups::MATRIX;
            attr_desc.type.num_components[0] = type_desc.traits.numeric.matrix.row_count;
            attr_desc.type.num_components[1] = type_desc.traits.numeric.matrix.column_count;
            break;
        }
        case SpvOpTypeImage:
        {
            attr_desc.type.type_group = ce::GLSLTypeGroups::IMAGE;
            break;
        }
        case SpvOpTypeSampler:
        {
            attr_desc.type.type_group = ce::GLSLTypeGroups::SAMPLER;
            break;
        }
        case SpvOpTypeSampledImage:
        {
            std::cout << logPrefix << "UNKOWN TYPE:" << type_desc.op << std::endl;
            break;
        }
        case SpvOpTypeArray:
        {
            attr_desc.type.type_group = ce::GLSLTypeGroups::ARRAY;
            break;
        }
        case SpvOpTypeStruct:
        {
            break;
        }
        default:
        {
            std::cout << logPrefix << "UNKOWN TYPE:" << type_desc.op << std::endl;
            break;
        }
    }
}

// /*! @struct SpvReflectInterfaceVariable

// */
// typedef struct SpvReflectInterfaceVariable {
//   uint32_t                            spirv_id;
//   const char*                         name;
//   uint32_t                            location;
//   SpvStorageClass                     storage_class;
//   const char*                         semantic;
//   SpvReflectDecorationFlags           decoration_flags;
//   SpvBuiltIn                          built_in;
//   SpvReflectNumericTraits             numeric;
//   SpvReflectArrayTraits               array;

//   uint32_t                            member_count;
//   struct SpvReflectInterfaceVariable* members;

//   SpvReflectFormat                    format;

//   // NOTE: SPIR-V shares type references for variables
//   //       that have the same underlying type. This means
//   //       that the same type name will appear for multiple
//   //       variables.
//   SpvReflectTypeDescription*          type_description;

//   struct {
//     uint32_t                          location;
//   } word_offset;
// } SpvReflectInterfaceVariable;
bool ParseInterfaceVariable(VulkanShaderAttributeData& attr_desc, SpvReflectInterfaceVariable& var, bool logDetails, const std::string& logPrefix)
{
    // ignore built-in variables
    if (var.decoration_flags & SPV_REFLECT_DECORATION_BUILT_IN) { 
        if(logDetails) std::cout << logPrefix << "Built In Variable" << std::endl;
        return false;
    }

    attr_desc.location = var.location;
    // Inherited from callee
    attr_desc.binding = 0;
    attr_desc.type.format = static_cast<VkFormat>(var.format);
    attr_desc.offset = 0;  // final offset computed below after sorting.
    attr_desc.name = var.name;

    ParseVariableType(attr_desc, *var.type_description, logDetails, logPrefix);

    if(logDetails)
    {
        std::cout << logPrefix << "Interface Variable(" << std::endl;
        std::cout << logPrefix << "  spirv_id: " << var.spirv_id << std::endl;
        if(var.name) std::cout << logPrefix << "  name: " << var.name << std::endl;
        std::cout << logPrefix << "  type: " << ToStringType(SpvSourceLanguageGLSL, *var.type_description) << std::endl;
        std::cout << logPrefix << "  format: " << ToStringFormat(var.format) << std::endl;
        if(var.type_description && var.type_description->type_name)
        {
            std::cout << logPrefix << "  type_name: " << var.type_description->type_name << std::endl;
        }
        std::cout << logPrefix << "  location: " << var.location << std::endl;
        std::cout << logPrefix << "  storage_class: " << ToStringSpvStorageClass(var.storage_class) << std::endl;
        std::cout << logPrefix << "  decoration_flags: " << var.decoration_flags << std::endl;

    }

    for(int i = 0; i < var.member_count; i++)
    {
        VulkanShaderAttributeData inner_attr_desc;
        //VkVertexInputAttributeDescription inner_attr_desc;
        ParseInterfaceVariable(inner_attr_desc, var.members[i], logDetails, logPrefix + "  ");
    }

    if(logDetails)
    {
        std::cout << logPrefix << ")" << std::endl;
    }

    return true;
}

RHI_Error EnumerateInputOutputAttributes(ShaderProgram::Shader& shader, SpvReflectShaderModule& module, bool logDetails, const std::string& logPrefix)
{
    uint32_t count = 0;
    SpvReflectResult result = spvReflectEnumerateInputVariables(&module, &count, NULL);
    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }

    std::vector<SpvReflectInterfaceVariable*> input_vars(count);
    result = spvReflectEnumerateInputVariables(&module, &count, input_vars.data());
    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }

    if(logDetails)
    {
        std::cout << logPrefix << "Input Variables" << std::endl;
    }

    VkVertexInputBindingDescription& binding_desc = shader.vulkanData->inputBindingDescription;
    // Binding is assumed
    binding_desc.binding = 0;
    binding_desc.stride = 0;
    binding_desc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    std::vector<VulkanShaderAttributeData>& attr_descriptions = shader.vulkanData->inputAttributes;

    for(SpvReflectInterfaceVariable* input_var : input_vars)
    {
        VulkanShaderAttributeData attr_desc;
        //attr_desc.binding = binding_desc.binding;
        if(ParseInterfaceVariable(attr_desc, *input_var, logDetails, logPrefix + "  "))
        {
            attr_descriptions.push_back(attr_desc);
        }
    }

    // Sort attributes by location
    // std::sort(std::begin(attr_descriptions), std::end(attr_descriptions),
    //     [](const VkVertexInputAttributeDescription& a, const VkVertexInputAttributeDescription& b) {
    //     return a.location < b.location; });
    // // Compute final offsets of each attribute, and total vertex stride.
    // for (auto& attribute : attr_descriptions) {
    //     uint32_t format_size = FormatSize(attribute.format);
    //     attribute.offset = binding_desc.stride;
    //     binding_desc.stride += format_size;
    // }

    count = 0;
    result = spvReflectEnumerateOutputVariables(&module, &count, NULL);
    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }

    std::vector<SpvReflectInterfaceVariable*> output_vars(count);
    result = spvReflectEnumerateOutputVariables(&module, &count, output_vars.data());
    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }

    if(logDetails)
    {
        std::cout << logPrefix << "Output Variables" << std::endl;
    }

    for(SpvReflectInterfaceVariable* output_var : output_vars)
    {
        VulkanShaderAttributeData attr_desc;
        ParseInterfaceVariable(attr_desc, *output_var, logDetails, logPrefix + "  ");
    }

    return RHI_Error::OK;
}

void ParseBlockVariable(ShaderProgram::Shader& shader, SpvReflectBlockVariable& block_var, bool logDetails, const std::string& logPrefix)
{
    if(logDetails)
    {
        std::cout << logPrefix << "Block Variable(" << std::endl;
        std::cout << logPrefix << "  spirv_id: " << block_var.spirv_id << std::endl;
        if(block_var.name) std::cout << logPrefix << "  name: " << block_var.name << std::endl;
        std::cout << logPrefix << "  type: " << ToStringType(SpvSourceLanguageGLSL, *block_var.type_description) << std::endl;
        if(block_var.type_description && block_var.type_description->type_name)
        {
            std::cout << logPrefix << "  type_name: " << block_var.type_description->type_name << std::endl;
        }
        std::cout << logPrefix << "  offset: " << block_var.offset << std::endl;
        std::cout << logPrefix << "  absolute_offset: " << block_var.absolute_offset << std::endl;
        std::cout << logPrefix << "  size: " << block_var.size << std::endl;
        std::cout << logPrefix << "  padded_size: " << block_var.padded_size << std::endl;
        std::cout << logPrefix << "  decoration_flags: " << block_var.decoration_flags << std::endl;
    }

    for(int i = 0; i < block_var.member_count; i++)
    {
        ParseBlockVariable(shader, block_var.members[i], logDetails, logPrefix + "  ");
    }

    if(logDetails)
    {
        std::cout << logPrefix << ")" << std::endl;
    }
}

// /*! @struct SpvReflectBlockVariable

// */
// typedef struct SpvReflectBlockVariable {
//   uint32_t                          spirv_id;
//   const char*                       name;
//   uint32_t                          offset;           // Measured in bytes
//   uint32_t                          absolute_offset;  // Measured in bytes
//   uint32_t                          size;             // Measured in bytes
//   uint32_t                          padded_size;      // Measured in bytes
//   SpvReflectDecorationFlags         decoration_flags;
//   SpvReflectNumericTraits           numeric;
//   SpvReflectArrayTraits             array;
//   SpvReflectVariableFlags           flags;

//   uint32_t                          member_count;
//   struct SpvReflectBlockVariable*   members;

//   SpvReflectTypeDescription*        type_description;
// } SpvReflectBlockVariable;

RHI_Error EnumeratePushConstants(ShaderProgram::Shader& shader, SpvReflectShaderModule& module, bool logDetails, const std::string& logPrefix)
{
    uint32_t count = 0;
    SpvReflectResult result = spvReflectEnumeratePushConstantBlocks(&module, &count, NULL);
    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }

    std::vector<SpvReflectBlockVariable*> push_blocks(count);
    result = spvReflectEnumeratePushConstantBlocks(&module, &count, push_blocks.data());
    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }

    if(logDetails)
    {
        std::cout << logPrefix << "Push Constants" << std::endl;
    }

    // TODO[RHI]: Create VkPushConstantRange for each push_block
    // and generate metadata about individual variables and 
    // their offsets so they can be set individually
    for(SpvReflectBlockVariable* push_block : push_blocks)
    {
        ParseBlockVariable(shader, *push_block, logDetails, logPrefix + "  ");
    }

    return RHI_Error::OK;
}

RHI_Error EnumerateDescriptorSets(ShaderProgram::Shader& shader, SpvReflectShaderModule& module, bool logDetails, const std::string& logPrefix)
{
    uint32_t count = 0;
    SpvReflectResult result = spvReflectEnumerateDescriptorSets(&module, &count, NULL);
    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }

    std::vector<SpvReflectDescriptorSet*> desc_sets(count);
    result = spvReflectEnumerateDescriptorSets(&module, &count, desc_sets.data());
    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }

    // Demonstrates how to generate all necessary data structures to create a
    // VkDescriptorSetLayout for each descriptor set in this shader.
    std::vector<DescriptorSetLayoutData> set_layouts(desc_sets.size(), DescriptorSetLayoutData{});
    for (size_t i = 0; i < desc_sets.size(); ++i) {

        const SpvReflectDescriptorSet& set = *(desc_sets[i]);
        DescriptorSetLayoutData& layout = set_layouts[i];
        layout.bindings.resize(set.binding_count);

        if(logDetails)
        {
            std::cout << logPrefix << "Descriptor set (set: " << set.set << " binding_count: " << set.binding_count << ")" << std::endl;
        }

        for (uint32_t binding_idx = 0; binding_idx < set.binding_count; ++binding_idx) {
            
            const SpvReflectDescriptorBinding& refl_binding = *(set.bindings[binding_idx]);
            VkDescriptorSetLayoutBinding& layout_binding = layout.bindings[binding_idx];
            layout_binding.binding = refl_binding.binding;
            layout_binding.descriptorType = static_cast<VkDescriptorType>(refl_binding.descriptor_type);
            layout_binding.descriptorCount = 1;

            if(logDetails)
            {
                std::cout << logPrefix << "  Binding (" << std::endl;
                std::cout << logPrefix << "    name: " << refl_binding.name << std::endl;
                std::cout << logPrefix << "    binding: " << refl_binding.binding << std::endl;
                std::cout << logPrefix << "    input attachment index: " << refl_binding.input_attachment_index << std::endl;
                std::cout << logPrefix << "    type: " << ToStringType(SpvSourceLanguageGLSL, *refl_binding.type_description) << std::endl;
                if(refl_binding.type_description && refl_binding.type_description->type_name)
                {
                    std::cout << logPrefix << "    type_name: " << refl_binding.type_description->type_name << std::endl;
                }
                std::cout << logPrefix << "    descriptor type: " << ToStringDescriptorType(refl_binding.descriptor_type) << std::endl;
                std::cout << logPrefix << "    resource type: " << ToStringResourceType(refl_binding.resource_type) << std::endl;
                std::cout << logPrefix << "    num array dims: " << refl_binding.array.dims_count << std::endl;
                std::cout << logPrefix << "    count: " << refl_binding.count << std::endl;
                std::cout << logPrefix << ")" << std::endl;
            }
            
            if(logDetails && refl_binding.array.dims_count)
            {
                std::cout << logPrefix << "  Array dimensions (";
            }

            for (uint32_t i_dim = 0; i_dim < refl_binding.array.dims_count; ++i_dim) {
                if(logDetails)
                {
                    if(i_dim != 0) std::cout << ", ";
                    std::cout << refl_binding.array.dims[i_dim];
                }

                layout_binding.descriptorCount *= refl_binding.array.dims[i_dim];
            }

            if(logDetails && refl_binding.array.dims_count)
            {
                std::cout << "  )" << std::endl;
            }
            
            layout_binding.stageFlags = static_cast<VkShaderStageFlagBits>(module.shader_stage);
        }

        layout.set_number = set.set;
        layout.create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layout.create_info.bindingCount = set.binding_count;
        layout.create_info.pBindings = layout.bindings.data();

    }

    shader.vulkanData->descSetsLayoutData = set_layouts;

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::PopulateShaderDescriptorInfo(ShaderProgram::Shader& shader)
{
    if(!shader.vulkanData || !shader.vulkanData->byteCode.size())
    {
        return RHI_Error::INVALID_STATE;
    }

    SpvReflectShaderModule module = {};
    SpvReflectResult result = spvReflectCreateShaderModule(
        sizeof(uint32_t) * shader.vulkanData->byteCode.size(),
        shader.vulkanData->byteCode.data(), 
        &module);

    if(result != SPV_REFLECT_RESULT_SUCCESS)
    {
        return RHI_Error::BAD_INPUT;
    }


    if(this->logShaderDescriptors)
    {
        std::cout << "----- Shader Information: " << shader.path.filename() << " -----" << std::endl;
    }

    RHI_Error rhiResult = EnumerateDescriptorSets(shader, module, this->logShaderDescriptors, "  ");
    if(rhiResult != RHI_Error::OK) return rhiResult;

    rhiResult = EnumeratePushConstants(shader, module, this->logShaderDescriptors, "");
    if(rhiResult != RHI_Error::OK) return rhiResult;

    rhiResult = EnumerateInputOutputAttributes(shader, module, this->logShaderDescriptors, "");
    if(rhiResult != RHI_Error::OK) return rhiResult;

    if(this->logShaderDescriptors)
    {
        std::cout << "----- End Shader Information -----" << std::endl << std::flush;

    }

    spvReflectDestroyShaderModule(&module);
    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CompileShaderProgram(ShaderProgram& program)
{
    return RHI_Error::NOT_IMPLEMENTED;
}

RHI_Error VulkanRHI::GetPipelineShaderStageCreateInfo(const ShaderProgram::Shader& shader, VkPipelineShaderStageCreateInfo& createInfo)
{
    createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    
    if (shader.type == ce::ShaderProgram::ShaderType::VERTEX_SHADER)
    {
        createInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    }
    else if (shader.type == ce::ShaderProgram::ShaderType::FRAGMENT_SHADER)
    {
        createInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    }
    else if (shader.type == ce::ShaderProgram::ShaderType::GEOMETRY_SHADER)
    {
        createInfo.stage = VK_SHADER_STAGE_GEOMETRY_BIT;
    }

    createInfo.module = shader.vulkanData->shaderModule;
    createInfo.pName = "main";

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateGraphicsPipeline(std::vector<ShaderProgram::Shader*> shaders)
{
    // // Use SPIRV reflection to properly fill this
    VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
    vertexInputInfo.sType =
    VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = 0;
    vertexInputInfo.pVertexBindingDescriptions = nullptr; // Optional
    vertexInputInfo.vertexAttributeDescriptionCount = 0;
    vertexInputInfo.pVertexAttributeDescriptions = nullptr; // Optional
    // //

    VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    VkPipelineViewportStateCreateInfo viewportState{};

    VkViewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float) this->extent.width;
    viewport.height = (float) this->extent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor{};
    scissor.offset = {0, 0};
    scissor.extent = this->extent;

    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;


    VkPipelineRasterizationStateCreateInfo rasterizer{};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer.depthBiasClamp = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    VkPipelineMultisampleStateCreateInfo multisampling{};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f; // Optional
    multisampling.pSampleMask = nullptr; // Optional
    multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling.alphaToOneEnable = VK_FALSE; // Optional

    VkPipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.colorWriteMask = 
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_TRUE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

    // Uniforms: Properly configure with SPIRV reflection
    VkPipelineLayout pipelineLayout;

    VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 0; // Optional
    pipelineLayoutInfo.pSetLayouts = nullptr; // Optional
    pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional
    VkResult result = vkCreatePipelineLayout(this->device, &pipelineLayoutInfo, nullptr, &pipelineLayout);
    // Make sure to destroy with 'vkDestroyPipelineLayout(device, pipelineLayout, nullptr);'

    if(result != VK_SUCCESS) return RHI_Error::INIT_ERROR;

    VkGraphicsPipelineCreateInfo pipelineInfo{};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = shaders.size();

    std::vector<VkPipelineShaderStageCreateInfo> shaderStages(shaders.size());
    for(int i = 0; i < shaders.size(); i++)
    {
        //GetPipelineShaderStageCreateInfo(shaders[i], &shaderStages[i]);
    }
    pipelineInfo.pStages = shaderStages.data();

    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = nullptr; // Optional
    // pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = nullptr; // Optional
    pipelineInfo.layout = pipelineLayout;
    // pipelineInfo.renderPass = renderPass;
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex = -1; // Optional

    VkPipeline graphicsPipeline;
    result = vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline);
    if(result != VK_SUCCESS) return RHI_Error::INIT_ERROR;
    // destroy later with 'vkDestroyPipeline(device, graphicsPipeline, nullptr);'
    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateRenderPass() {

    // TODO[RHI]: 
    // Have Render Layer/Techniques define this information
    VkAttachmentDescription colorAttachment{};
    // colorAttachment.format = this->surfaceFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;


    VkAttachmentReference colorAttachmentRef{};
    // refers to the shader out param for the colour value in the fragment shader
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;


    VkRenderPass renderPass;


    VkRenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;

    VkResult result = vkCreateRenderPass(this->device, &renderPassInfo, nullptr, &renderPass);
    if(result != VK_SUCCESS) return RHI_Error::INIT_ERROR;
    // Needs destruction 'vkDestroyRenderPass(device, renderPass, nullptr);'
    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateSwapChainFrameBuffers()
{
    this->swapChainFramebuffers.resize(swapChainImageViews.size());

    for (size_t i = 0; i < swapChainImageViews.size(); i++) {
        VkImageView attachments[] = {
            swapChainImageViews[i]
        };

        VkFramebufferCreateInfo framebufferInfo{};
        framebufferInfo.sType =
        VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        // framebufferInfo.renderPass = renderPass;
        framebufferInfo.attachmentCount = 1;
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = this->extent.width;
        framebufferInfo.height = this->extent.height;
        framebufferInfo.layers = 1;

        VkResult result = vkCreateFramebuffer(this->device, &framebufferInfo, nullptr, &this->swapChainFramebuffers[i]);
        if(result != VK_SUCCESS) return RHI_Error::INIT_ERROR;
    }

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateCommandPool()
{
    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    poolInfo.queueFamilyIndex = this->graphicsQueueFamilyIndex;

    VkResult result = vkCreateCommandPool(this->device, &poolInfo, nullptr, &this->commandPool);
    if(result != VK_SUCCESS) return RHI_Error::INIT_ERROR;

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateCommandBuffer()
{
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = this->commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = 1;

    VkResult result = vkAllocateCommandBuffers(this->device, &allocInfo, &this->commandBuffer);
    if(result != VK_SUCCESS) return RHI_Error::INIT_ERROR;

    return RHI_Error::OK;
}

Uint32 VulkanRHI::GetWindowFlags()
{
    return SDL_WINDOW_VULKAN;
}

RHI_Error VulkanRHI::DrawFrame(Window* window, std::function<void(void)> drawSceneFunc)
{
    return RHI_Error::FAILURE;
}

RHI_Error VulkanRHI::RegisterExtension(std::string extensionName)
{
    if(this->physicalDevice != VK_NULL_HANDLE)
    {
        return RHI_Error::INVALID_STATE;
    }

    if(this->IsExtensionSupported(extensionName))
    {
        this->additionalExtensions.push_back(extensionName);
        return RHI_Error::OK;
    }

    return RHI_Error::UNSUPPORTED;
}

bool VulkanRHI::IsExtensionSupported(std::string extensionName)
{
    for(const VkExtensionProperties& extensionProps : this->GetSupportedExtensions())
    {
        if(std::strcmp(extensionName.c_str(), extensionProps.extensionName))
        {
            return true;
        }
    }

    return false;
}

bool VulkanRHI::AreValidationLayersSupported() {
    uint32_t layerCount;

    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    for (const char* layerName : this->validationLayers) {
        bool layerFound = false;
        for (const auto& layerProperties : availableLayers) {
            if (strcmp(layerName, layerProperties.layerName) == 0) {
                layerFound = true;
                break;
            }
        }
        if (!layerFound) {
            return false;
        }
    }
    
    return true;
}

VulkanRHI::SwapChainSupportDetails VulkanRHI::QuerySwapChainSupport(VkPhysicalDevice device)
{
    SwapChainSupportDetails details;

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

    if (formatCount != 0) {
        details.formats.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface,
        &formatCount, details.formats.data());
    }

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

    if (presentModeCount != 0) {
        details.presentModes.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
    }

    return details;
}

RHI_Error VulkanRHI::CreateInstance(Window* window)
{
    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Cutting Edge Game";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "Cutting Edge";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    unsigned int count;
    // get count of required extensions
    if(!SDL_Vulkan_GetInstanceExtensions(window->GetRawWindow(), &count, NULL))
    {
        std::cerr << "Failed to get SDL Vulkan extensions" << std::endl;
        return RHI_Error::INIT_ERROR;
    }

    size_t extensionCount = count + this->additionalExtensions.size();

    std::vector<const char*> names(extensionCount);

    if(names.size() == 0)
    {
        std::cerr << "Failed to get SDL Vulkan extensions" << std::endl;
        return RHI_Error::INIT_ERROR;
    }

    // get names of required extensions
    if(SDL_Vulkan_GetInstanceExtensions(window->GetRawWindow(), &count, names.data()) != SDL_TRUE)
    {
        std::cerr << "Failed to get SDL Vulkan extensions" << std::endl;
        return RHI_Error::INIT_ERROR;        
    }

    // copy additional extensions after required extensions
    for(std::string extension : this->additionalExtensions)
    {
        names.push_back(extension.c_str());
    }

    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = names.data();
    
    if (enableValidationLayers && this->AreValidationLayersSupported()) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(this->validationLayers.size());
        createInfo.ppEnabledLayerNames = this->validationLayers.data();
    } else {
        if(enableValidationLayers)
        {
            std::cout << "Validation layers enabled but not supported" << std::endl;
        }

        createInfo.enabledLayerCount = 0;
    }

    // create the Vulkan instance
    if(vkCreateInstance(&createInfo, nullptr, &this->instance) == VK_SUCCESS)
    {
        return RHI_Error::OK;
    }
    
    std::cerr << "Failed to create Vulkan instance" << std::endl;
    return RHI_Error::INIT_ERROR;
}

RHI_Error VulkanRHI::CreateSurface(Window* window)
{
    if(!SDL_Vulkan_CreateSurface(window->GetRawWindow(), this->instance, &this->surface))
    {
        return RHI_Error::INIT_ERROR;
    }

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::SelectPhysicalDevice()
{
    auto checkDeviceExtensionSupport = [this](VkPhysicalDevice device) {
        uint32_t extensionCount;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

        std::vector<VkExtensionProperties> availableExtensions(extensionCount);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

        std::set<std::string> requiredExtensions(this->deviceExtensions.begin(), this->deviceExtensions.end());

        for (const auto& extension : availableExtensions) {
            requiredExtensions.erase(extension.extensionName);
        }
    
        return requiredExtensions.empty();
    };

    auto isSwapChainAdequate = [this](VkPhysicalDevice device) {
        SwapChainSupportDetails swapChainSupport = this->QuerySwapChainSupport(device);
        return !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
    };

    auto findFlagsInQueueFamilies = [](VkPhysicalDevice device, VkQueueFlags flags)
    {
        VkQueueFlags representedFlags = 0;
        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
        std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

        for (const auto& queueFamily : queueFamilies) {
            representedFlags |= queueFamily.queueFlags;
        }

        return flags == (representedFlags & flags);
    };

    auto rateDeviceSuitability = [](VkPhysicalDevice device) {
        VkPhysicalDeviceProperties deviceProperties;
        VkPhysicalDeviceFeatures deviceFeatures;
        vkGetPhysicalDeviceProperties(device, &deviceProperties);
        vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

        int score = 0;
        // Discrete GPUs have a significant performance advantage
        if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
            score += 1;
        }

        return score;
    };

    auto isDeviceSuitable = [this, findFlagsInQueueFamilies, checkDeviceExtensionSupport, isSwapChainAdequate](VkPhysicalDevice device) {
        return 
            findFlagsInQueueFamilies(device, VK_QUEUE_GRAPHICS_BIT)
            && this->FindQueueFamiliesWithPresentSupport(device).size()
            && checkDeviceExtensionSupport(device)
            && isSwapChainAdequate(device);
    };

    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

    if(deviceCount == 0)
    {
        std::cerr << "No GPU which supports Vulkan detected" << std::endl;
        return RHI_Error::UNSUPPORTED;
    }

    std::vector<VkPhysicalDevice> devices(deviceCount);
    vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

    int currentDeviceSuitability = -1;
    for (const auto& device : devices) {
        if (isDeviceSuitable(device)) {
            int deviceSuitability = rateDeviceSuitability(device);
            
            if(deviceSuitability > currentDeviceSuitability)
            {
                physicalDevice = device;
                currentDeviceSuitability = deviceSuitability;
            }
        }
    }
    
    if (physicalDevice == VK_NULL_HANDLE) {
        std::cerr << "GPU detected, but none found which are suitable" << std::endl;
        return RHI_Error::UNSUPPORTED;
    }

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateLogicalDevice()
{
    auto findQueueFamilyWithFlags = [](VkPhysicalDevice device, VkQueueFlags flags)
    {
        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
        std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

        int i = 0;
        for (const auto& queueFamily : queueFamilies) {
            if(flags == (queueFamily.queueFlags & flags))
            {
                return i; 
            }

            i++;
        }

        return -1;
    };

    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = findQueueFamilyWithFlags(this->physicalDevice, VK_QUEUE_GRAPHICS_BIT);
    queueCreateInfo.queueCount = 1;

    this->graphicsQueueFamilyIndex = queueCreateInfo.queueFamilyIndex;

    float queuePriority = 1.0f;
    queueCreateInfo.pQueuePriorities = &queuePriority;
    queueCreateInfos.push_back(queueCreateInfo);

    std::vector<uint32_t> presentFamilies = this->FindQueueFamiliesWithPresentSupport(this->physicalDevice);
    if(!presentFamilies.size()) return RHI_Error::INIT_ERROR;

    this->presentQueueFamilyIndex = presentFamilies[0];

    if(this->presentQueueFamilyIndex != this->graphicsQueueFamilyIndex)
    {
        queueCreateInfo.queueFamilyIndex = this->presentQueueFamilyIndex;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    VkPhysicalDeviceFeatures deviceFeatures{};

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pEnabledFeatures = &deviceFeatures;

    createInfo.enabledExtensionCount = static_cast<uint32_t>(this->deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = this->deviceExtensions.data();

    // Needed for validation layers on older implementations
    // createInfo.enabledExtensionCount = 0;
    // if (enableValidationLayers) {
    //     createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
    //     createInfo.ppEnabledLayerNames = validationLayers.data();
    // } 
    // else 
    // {
    //     createInfo.enabledLayerCount = 0;
    // }

    VkResult result = vkCreateDevice(this->physicalDevice, &createInfo, nullptr, &this->device);
    if(result != VK_SUCCESS)
    {
        std::cerr << "Failed to create Vulkan logical device" << std::endl;
        return RHI_Error::INIT_ERROR;
    }

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::InitialiseQueueHandles()
{
    vkGetDeviceQueue(this->device, this->graphicsQueueFamilyIndex, 0, &this->graphicsQueue);
    vkGetDeviceQueue(this->device, this->presentQueueFamilyIndex, 0, &this->graphicsQueue);

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateSwapChain()
{
    auto chooseSwapSurfaceFormat = [](const std::vector<VkSurfaceFormatKHR>& availableFormats) 
    {
        for (const auto& availableFormat : availableFormats) {
            if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB &&
                availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) 
            {
                return availableFormat;
            }
        }

        return availableFormats[0];
    };

    auto chooseSwapPresentMode = [](const std::vector<VkPresentModeKHR>& availablePresentModes) 
    {
        for (const auto& availablePresentMode : availablePresentModes) {
            if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return availablePresentMode;
            }
        }
        return VK_PRESENT_MODE_FIFO_KHR;
    };

    auto chooseSwapExtent = [](const VkSurfaceCapabilitiesKHR& capabilities) 
    {
        if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
            return capabilities.currentExtent;
        } 
        else 
        {
            int width = 0;
            int height = 0;
            // Do SDL equivalent? Needed for high dpi screens?
            //glfwGetFramebufferSize(window, &width, &height);
            VkExtent2D actualExtent = {
                static_cast<uint32_t>(width),
                static_cast<uint32_t>(height)
            };
            
            // actualExtent.width = std::clamp(
            //     actualExtent.width,
            //     capabilities.minImageExtent.width,
            //     capabilities.maxImageExtent.width
            // );
            
            // actualExtent.height = std::clamp(
            //     actualExtent.height,
            //     capabilities.minImageExtent.height,
            //     capabilities.maxImageExtent.height
            // );
            std::cerr << "High DPI displays unsupported" << std::endl;
            return actualExtent;
        }
    };

    SwapChainSupportDetails swapChainSupport = this->QuerySwapChainSupport(this->physicalDevice);

    this->surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
    this->presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
    this->extent = chooseSwapExtent(swapChainSupport.capabilities);

    uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;

    if (swapChainSupport.capabilities.maxImageCount > 0 
        && imageCount > swapChainSupport.capabilities.maxImageCount) 
    {
        imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = this->surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = this->surfaceFormat.format;
    createInfo.imageColorSpace = this->surfaceFormat.colorSpace;
    createInfo.imageExtent = this->extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    if (this->graphicsQueueFamilyIndex != this->presentQueueFamilyIndex) {
        uint32_t familyIndices[] = { this->graphicsQueueFamilyIndex, this->presentQueueFamilyIndex };

        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = familyIndices;
    } else {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0; // Optional
        createInfo.pQueueFamilyIndices = nullptr; // Optional
    }

    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    VkResult result = vkCreateSwapchainKHR(this->device, &createInfo, nullptr, &this->swapChain);
    if(result != VK_SUCCESS) return RHI_Error::INIT_ERROR;

    vkGetSwapchainImagesKHR(this->device, this->swapChain, &imageCount, nullptr);
    this->swapChainImages.resize(imageCount);
    vkGetSwapchainImagesKHR(this->device, this->swapChain, &imageCount,
    this->swapChainImages.data());

    return RHI_Error::OK;
}

RHI_Error VulkanRHI::CreateSwapChainImageViews()
{
    this->swapChainImageViews.resize(this->swapChainImages.size());

    for(size_t i = 0; i < swapChainImages.size(); i++)
    {
        VkImageViewCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = this->swapChainImages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = this->surfaceFormat.format;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;
        
        VkResult result = vkCreateImageView(this->device, &createInfo, nullptr, &this->swapChainImageViews[i]);
        if(result != VK_SUCCESS) return RHI_Error::INIT_ERROR;
    }

    return RHI_Error::OK;
}

std::vector<VkExtensionProperties> VulkanRHI::GetSupportedExtensions()
{
    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

    std::vector<VkExtensionProperties> extensions(extensionCount);

    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

    return extensions;
}

std::vector<uint32_t> VulkanRHI::FindQueueFamiliesWithPresentSupport(VkPhysicalDevice device)
{
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

    int i = 0;
    std::vector<uint32_t> supportedFamilies;
    for (const auto& queueFamily : queueFamilies) {
        VkBool32 canPresent;
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, this->surface, &canPresent);
        
        if(canPresent)
        {
            supportedFamilies.push_back(i);
        } 

        i++;
    }

    return supportedFamilies;
}