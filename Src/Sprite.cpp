#include "Sprite.h"

std::string const ce::Sprite::POS_ATTRIB_NAME = "position";
std::string const ce::Sprite::NORMAL_ATTRIB_NAME = "normal";
std::string const ce::Sprite::COLOR_ATTRIB_NAME = "color";
std::string const ce::Sprite::COORD_UNIFORM_NAME = "spritecoords";
std::string const ce::Sprite::MAINTEX_UNIFORM_NAME = "mainTex";

ce::Sprite::Sprite(std::shared_ptr<ce::Sprite::SpriteData> spriteData) : FlippedX(false)
{
	this->spriteData = spriteData;
	this->material = spriteData->material;
	this->component = std::weak_ptr<ce::Component>();
    this->traits.push_back(new ce::ZIndexed());
}

void ce::Sprite::SetSpriteCoordsFromPixelCoords(const glm::vec4 &pixelCoords)
{
	if (this->spriteData->texture != nullptr) {
		auto dim = this->spriteData->texture->GetDimensions();
		this->spriteData->coords = ce::Sprite::CalculateSpriteCoords(pixelCoords, dim);
	}
}

glm::vec4 ce::Sprite::CalculateSpriteCoords(const glm::vec4 &pixelCoords, glm::vec2 textureDimensions)
{
	return pixelCoords / glm::vec4(textureDimensions.x, textureDimensions.y, textureDimensions.x, textureDimensions.y);
}

std::weak_ptr<ce::Sprite::SpriteData> ce::Sprite::GetSpriteData() const
{
	return this->spriteData;
}

std::weak_ptr<ce::Material> ce::Sprite::GetMaterial()
{
	return this->material;
}

void ce::Sprite::_SetMeshData(std::shared_ptr<ce::Mesh::MeshData> meshData)
{
	this->meshData = meshData;
}

std::weak_ptr<ce::Component> ce::Sprite::GetComponent()
{
	return this->component;
}

void ce::Sprite::Draw()
{
	glBindVertexArray(this->meshData->vao);

	glDrawElements(GL_TRIANGLES, this->meshData->elementsCount, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
}
