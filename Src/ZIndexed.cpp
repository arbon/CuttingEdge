#include "ZIndexed.h"

int ce::ZIndexed::GetZIndex() const
{
    return this->zIndex;
}

void ce::ZIndexed::SetZIndex(int zIndex)
{
    this->zIndex = zIndex;
}

bool ce::ZIndexed::operator<(const ce::DrawableTrait & rhs) const
{
    
    return this->GetZIndex() < reinterpret_cast<ZIndexed const&>(rhs).GetZIndex();
}
