#include "DebugRenderLayer.h"
#include "DebugDrawComponent.h"
#include "TextComponent.h"
#include "ToString.h"

ce::DebugRenderLayer::DebugRenderLayer()
{
}

void ce::DebugRenderLayer::Render(ce::Camera & camera)
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ZERO);

    glm::mat4 viewProj = camera.GetViewProjectionMatrix();
    std::string mvp_string = "MVP";
    glm::mat4 mvp;

    auto i = this->drawables.begin();
    while (i != this->drawables.end()) {
        if (auto drawable = i->lock()) {
            auto materialLocked = drawable->GetMaterial().lock();
            auto componentLocked = drawable->GetComponent().lock();
            auto transformLocked = componentLocked->GetTransform().lock();

            mvp = transformLocked->CalcMVPMatrix(viewProj);

            materialLocked->Bind();
            materialLocked->SetMatrix4(mvp_string, mvp);
            materialLocked->UpdateAllUniforms();

            drawable->Draw();
            materialLocked->Unbind();
            ++i;
        }
        else {
            i = this->drawables.erase(i);
        }
    }
}

void ce::DebugRenderLayer::AddDrawable(std::weak_ptr<ce::Drawable> drawable)
{
	auto debug = std::dynamic_pointer_cast<ce::DebugDrawComponent>(drawable.lock());
    auto debugText = std::dynamic_pointer_cast<ce::TextComponent>(drawable.lock());

	if (debug || debugText) {
		this->drawables.push_back(drawable);
	}
}
