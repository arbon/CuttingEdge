#include "FPSCamera.h"

ce::FPSCamera::FPSCamera(std::weak_ptr<Transform> transform, std::weak_ptr<Camera> camera, std::string name) : left(0.0f), forward(0.0f), ce::Component(transform, name)
{
    if (auto cameraLocked = camera.lock()) {
        this->camera = cameraLocked;
    }

    si::Profile& input = ce::World::GetWorld()->Input.GetProfile();
    input.relative(si::BindSrc(si::BindSrcType_Key, SDLK_w, SDLK_s), &this->forward)->setLimit(-1.0f, 1.0f);
    input.relative(si::BindSrc(si::BindSrcType_Key, SDLK_d, SDLK_a), &this->left)->setLimit(-1.0f, 1.0f);
}

std::weak_ptr<ce::Drawable> ce::FPSCamera::GetDrawable()
{
	return std::weak_ptr<ce::Drawable>();
}

std::weak_ptr<ce::Processable> ce::FPSCamera::GetProcessable()
{
	return std::dynamic_pointer_cast<ce::Processable>(this->getPointer());
}

void ce::FPSCamera::Process()
{
	const float speed = 4.0f;

    if (auto transformLocked = this->GetTransform().lock()) {
        transformLocked->SetLocalRotation(glm::rotate(transformLocked->GetLocalRotation(), this->left * ce::Time::GetDeltaTime() * speed, ce::Transform::GlobalUp));
        transformLocked->SetLocalPosition(transformLocked->GetLocalPosition() + transformLocked->GetLocalRotation() * ce::Transform::GlobalForward * this->forward * ce::Time::GetDeltaTime() * speed);
    }
}
