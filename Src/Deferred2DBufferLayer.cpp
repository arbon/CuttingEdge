#include "Deferred2DBufferLayer.h"

ce::Deferred2DBufferLayer::Deferred2DBufferLayer() : zDrawables()
{
    glm::vec2 windowSize = ce::World::GetWorld()->GetWindowSize();

    auto diffuseBuffer = std::make_shared<ce::RenderLayer::TexBuffer>();
    diffuseBuffer->name = "diffuse";
    diffuseBuffer->Width = windowSize.x;
    diffuseBuffer->Height = windowSize.y;

    auto surfaceNormalBuffer = std::make_shared<ce::RenderLayer::TexBuffer>();
    surfaceNormalBuffer->name = "surfacenormal";
    surfaceNormalBuffer->Width = windowSize.x;
    surfaceNormalBuffer->Height = windowSize.y;

    this->outputBuffers.push_back(diffuseBuffer);
    this->outputBuffers.push_back(surfaceNormalBuffer);
}

void ce::Deferred2DBufferLayer::Render(ce::Camera & camera)
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->frameBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDrawBuffers(this->drawBuffers.size(), this->drawBuffers.data());

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glm::mat4 viewProj = camera.GetViewProjectionMatrix();
    std::string mvp_string = "MVP";
    glm::mat4 mvp;

    auto i = this->zDrawables.begin();
    while (i != this->zDrawables.end()) {
        if (auto drawable = i->drawable.lock()) {
            auto materialLocked = drawable->GetMaterial().lock();
            auto componentLocked = drawable->GetComponent().lock();
            auto transformLocked = componentLocked->GetTransform().lock();

            mvp = transformLocked->CalcMVPMatrix(viewProj);

            materialLocked->Bind();
            materialLocked->SetMatrix4(mvp_string, mvp);
            materialLocked->UpdateAllUniforms();

            drawable->Draw();
            materialLocked->Unbind();
            ++i;
        }
        else {
            i = this->zDrawables.erase(i);
        }
    }
}

void ce::Deferred2DBufferLayer::AddDrawable(std::weak_ptr<ce::Drawable> drawable)
{
    if (auto drawableLocked = drawable.lock()) {
        ce::ZIndexed* trait = drawableLocked->GetTrait<ZIndexed>();
        if (trait != nullptr) {
            this->zDrawables.insert(*trait);
        }
    }
}
