#include "ParallaxSpriteComponent.h"
#include "ToString.h"
#include <algorithm>

std::string const ce::ParallaxSpriteComponent::SPRITE_OFFSET_UNIFORM_NAME = "offset";

ce::ParallaxSpriteComponent::ParallaxSpriteComponent(std::weak_ptr<Transform> transform, std::shared_ptr<Sprite> sprite, std::string name) : ce::SpriteComponent(transform, sprite, name)
{
}

void ce::ParallaxSpriteComponent::Initialize()
{
    this->SpriteComponent::Initialize();

    auto shader = ce::ResourceManager::GetShaderProgram(this->shaderProgName);

    if (!shader) {
        shader = std::make_shared<ce::ShaderProgram>(ce::ShaderProgram(this->shaderProgName));

        shader->shaders.push_back(ce::ResourceManager::LoadShader(this->vertShaderPath));
        shader->shaders.push_back(ce::ResourceManager::LoadShader(this->fragShaderPath));

        ce::ResourceManager::CompileShaderProgram(shader);

        std::string shaderError;

        for (auto test : shader->shaders) {
            shader->CheckShaderStatus(*test.lock(), shaderError);
        }

        std::cout << shaderError;
    }

    this->sprite->material = std::make_shared<ce::Material>(shader);

    if (auto materialLocked = this->GetMaterial().lock()) {
        if (auto spriteDataLocked = this->sprite->GetSpriteData().lock()) {
            if (spriteDataLocked->texture) {
                materialLocked->SetTexture(ce::Sprite::MAINTEX_UNIFORM_NAME, spriteDataLocked->texture->data->index);
            }
        }

        float halfX = this->WindowSize.x * 0.5f;
        float halfY = this->WindowSize.y * 0.5f;
        glm::vec4 coords = glm::vec4(0.5f - halfX, 0.5f - halfY, 0.5f + halfX, 0.5f + halfY);
        materialLocked->SetVector4(ce::Sprite::COORD_UNIFORM_NAME, coords);
    }
}

std::weak_ptr<ce::Processable> ce::ParallaxSpriteComponent::GetProcessable()
{
    return std::dynamic_pointer_cast<ce::Processable>(this->getPointer());
}

float wrapf(float num, float div)
{
    return num - div * std::floor(num / div);
}

float clampf(float n, float lower, float upper) {
  return std::max(lower, std::min(n, upper));
}

void ce::ParallaxSpriteComponent::Process()
{
    auto transformLocked = this->transform.lock();
    auto materialLocked = this->GetMaterial().lock();

    if (transformLocked && materialLocked) {
        glm::vec3 worldPos = transformLocked->GetWorldPosition();
        float yOffset = std::min(1.0f - (this->WindowSize.y * 0.5f), std::max(this->WindowSize.y * 0.5f, worldPos.y - this->StartPosition.y));
        glm::vec2 offset = glm::vec2(worldPos.x - this->StartPosition.x, this->StartPosition.y - worldPos.y) * this->DistanceRatio;
        glm::vec2 clampedOffset = glm::vec2(wrapf(offset.x, 1.0f), clampf(offset.y, -0.02f, 0.4f));
        materialLocked->SetVector2(SPRITE_OFFSET_UNIFORM_NAME, clampedOffset);
    }
}
