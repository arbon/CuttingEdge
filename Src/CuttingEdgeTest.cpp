#include <stdio.h>
#include <fstream>
#include <iostream>
#include <memory>

#include "World.h"

#include "gl_includes.h"
#include "ResourceManager.h"
#include "Scene.h"
#include "FPSCamera.h"
#include "MeshComponent.h"
#include "SpriteComponent.h"
#include "SpriteAnimator.h"
#include "SpriteAnimation.h"
#include "Texture.h"

const std::string bearPath = "cutting_edge/Resources/Models/bear.obj";
const std::string bearTexPath = "cutting_edge/Resources/Textures/bear.png";
const std::string deerPath = "cutting_edge/Resources/Models/deer.obj";
const std::string deerTexPath = "cutting_edge/Resources/Textures/deer.png";
const std::string tilesTexPath = "cutting_edge/Resources/Textures/ph-tiles.png";
const std::string playerTexPath = "cutting_edge/Resources/Textures/ph-player.png";
const std::string vertPath = "cutting_edge/Resources/Shaders/simple.vert";
const std::string fragPath = "cutting_edge/Resources/Shaders/simple.frag";
const std::string spriteVertPath = "cutting_edge/Resources/Shaders/simple-sprite.vert";
const std::string spriteFragPath = "cutting_edge/Resources/Shaders/simple-sprite.frag";
const std::string SIMPLE_SHADER = "simple";
const std::string SIMPLE_SPRITE_SHADER = "simple-sprite";
const std::string MAIN_TEX = "mainTex";

// Create and populate a test scene
std::shared_ptr<ce::Scene> CreateDefaultScene() {
    std::shared_ptr<ce::ShaderProgram> program = std::make_shared<ce::ShaderProgram>(ce::ShaderProgram(SIMPLE_SHADER));

    program->shaders.push_back(ce::ResourceManager::LoadShader(vertPath));
    program->shaders.push_back(ce::ResourceManager::LoadShader(fragPath));

    ce::ResourceManager::CompileShaderProgram(program);

    std::shared_ptr<ce::ShaderProgram> sprogram = std::make_shared<ce::ShaderProgram>(SIMPLE_SPRITE_SHADER);

    sprogram->shaders.push_back(ce::ResourceManager::LoadShader(spriteVertPath));
    sprogram->shaders.push_back(ce::ResourceManager::LoadShader(spriteFragPath));

    ce::ResourceManager::CompileShaderProgram(sprogram);

    std::shared_ptr<ce::Scene> defaultScene = std::make_shared<ce::Scene>();

    std::shared_ptr<ce::Texture> bearTex = ce::ResourceManager::LoadTexture(bearTexPath);
    ce::ResourceManager::BufferTexture(bearTex->data);

    std::shared_ptr<ce::Texture> deerTex = ce::ResourceManager::LoadTexture(deerTexPath);
    ce::ResourceManager::BufferTexture(deerTex->data);

    std::shared_ptr<ce::Texture> tilesTex = ce::ResourceManager::LoadTexture(tilesTexPath);
    ce::ResourceManager::BufferTexture(tilesTex->data);

    std::shared_ptr<ce::Texture> playerTex = ce::ResourceManager::LoadTexture(playerTexPath);
    ce::ResourceManager::BufferTexture(playerTex->data);

    auto cTransform = defaultScene->CreateTransform(std::weak_ptr<ce::Transform>(), "Cam Node").lock();
    cTransform->SetLocalRotation(glm::quat(0.0f, glm::vec3(0.0f, 1.0f, 0.0f)));
    cTransform->SetLocalPosition(glm::vec3(0.0f, 0.0f, -10.0f));
    std::shared_ptr<ce::Camera> camera = std::make_shared<ce::Camera>(ce::Camera::ProjectionMode::Perspective, cTransform);

    defaultScene->CreateComponent<ce::FPSCamera>(cTransform, camera, "Camera");
    defaultScene->SetActiveCamera(camera);

    std::weak_ptr<ce::Transform> stuffGroup = defaultScene->CreateTransform(std::weak_ptr<ce::Transform>(), "Animals");
    std::weak_ptr<ce::Transform> spriteGroup = defaultScene->CreateTransform(std::weak_ptr<ce::Transform>(), "Sprites");
    spriteGroup.lock()->SetLocalPosition(glm::vec3(2.0f, 0.0f, -5.0f));

    int cSize = 8;

    std::shared_ptr<ce::Sprite::SpriteData> playerSpriteData = std::shared_ptr<ce::Sprite::SpriteData>(new ce::Sprite::SpriteData {
        playerTex,
        "player",
        std::shared_ptr<ce::Material>(),
        ce::Sprite::CalculateSpriteCoords(glm::vec4(0.0f, 0.0f, 70.0f, 70.0f), playerTex->GetDimensions())
    });

    auto playerRunAnimation = std::make_shared<ce::SpriteAnimation>();
    playerRunAnimation->fps = 6;
    playerRunAnimation->name = "run";
    playerRunAnimation->frames = std::vector<glm::vec4>();
    playerRunAnimation->frames.push_back(glm::vec4(0.0f, 0.0f, 64.0f, 64.0f));
    playerRunAnimation->frames.push_back(glm::vec4(64.0f, 0.0f, 128.0f, 64.0f));
    playerRunAnimation->frames.push_back(glm::vec4(128.0f, 0.0f, 192.0f, 64.0f));
    playerRunAnimation->frames.push_back(glm::vec4(192.0f, 0.0f, 256.0f, 64.0f));
    playerRunAnimation->frames.push_back(glm::vec4(256.0f, 0.0f, 320.0f, 64.0f));
    playerRunAnimation->frames.push_back(glm::vec4(320.0f, 0.0f, 384.0f, 64.0f));
    playerRunAnimation->frames.push_back(glm::vec4(384.0f, 0.0f, 448.0f, 64.0f));
    playerRunAnimation->frames.push_back(glm::vec4(448.0f, 0.0f, 512.0f, 64.0f));

    std::shared_ptr<ce::Transform> player = defaultScene->CreateTransform(spriteGroup, "player").lock();
    player->SetLocalPosition(glm::vec3(0.0f, 0.0f, -2.1f));
    player->SetLocalRotation(glm::quat(glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f))));
    std::shared_ptr<ce::SpriteComponent> playerComponent = defaultScene->CreateComponent<ce::SpriteComponent>(player, std::make_shared<ce::Sprite>(playerSpriteData)).lock();

    auto playerMaterial = std::make_shared<ce::Material>(ce::ResourceManager::GetShaderProgram(SIMPLE_SPRITE_SHADER));
    playerComponent->SetMaterial(playerMaterial);

    playerComponent->GetMaterial().lock()->SetTexture(MAIN_TEX, *playerTex);
    

    std::shared_ptr<ce::SpriteAnimator> playerAnimator = defaultScene->CreateComponent<ce::SpriteAnimator>(player, "pAnimator", playerComponent).lock();
    playerAnimator->Play(playerRunAnimation);

    auto spriteData = std::shared_ptr<ce::Sprite::SpriteData> (new ce::Sprite::SpriteData{
        tilesTex,
        "tile",
        std::shared_ptr<ce::Material>(),
        ce::Sprite::CalculateSpriteCoords(glm::vec4(0.0f, 0.0f, 70.0f, 70.0f), tilesTex->GetDimensions())
    });

    for (int i = 0; i < cSize; i++) {
        for (int j = 0; j < cSize; j++) {
            std::string name = "";
            name = name.append("tile(").append(std::to_string(i)).append(", ").append(std::to_string(j).append(", ").append(")"));
            auto tile = defaultScene->CreateTransform(spriteGroup, name).lock();
            tile->SetLocalPosition(glm::vec3(i - cSize * 0.5, j - cSize * 0.5, 0.0f));
            tile->SetLocalRotation(glm::quat(glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f))));

            auto tileMaterial = std::make_shared<ce::Material>(ce::ResourceManager::GetShaderProgram(SIMPLE_SPRITE_SHADER));

            auto tileComponent = defaultScene->CreateComponent<ce::SpriteComponent>(tile, std::make_shared<ce::Sprite>(spriteData)).lock();
            tileComponent->SetMaterial(tileMaterial);
            tileComponent->GetMaterial().lock()->SetTexture(MAIN_TEX, *tilesTex);
        }
    }

    for (int i = 0; i < cSize; i++) {
        for (int j = 0; j < cSize; j++) {
            for (int k = 0; k < cSize; k++){
                if (k % 2 == 0) {
                    std::string name = "";
                    name = name.append("bear(").append(std::to_string(i)).append(", ").append(std::to_string(j).append(", ").append(std::to_string(k)).append(")"));
                    auto bear = defaultScene->CreateTransform(stuffGroup, name).lock();
                    bear->SetLocalPosition(glm::vec3(i - cSize * 0.5, j - cSize * 0.5, k - cSize * 0.5));
                    bear->SetLocalScale(glm::vec3(0.01f, 0.01f, 0.01f));

                    auto mComponent = ce::ResourceManager::LoadMesh(bearPath);

                    if(!mComponent.empty())
                    {
                        for (int l = 0; l < mComponent.size(); l++) {
                            auto meshComponent = defaultScene->CreateComponent<ce::MeshComponent>(bear, mComponent[l], name).lock();
                            meshComponent->SetMaterial(std::make_shared<ce::Material>(ce::ResourceManager::GetShaderProgram(SIMPLE_SHADER)));
                            meshComponent->GetMaterial().lock()->SetTexture(MAIN_TEX, *bearTex);
                        }
                    } else
                    {
                        return nullptr;
                    }
                }
                else {
                    std::string name = "";
                    name = name.append("deer(").append(std::to_string(i)).append(", ").append(std::to_string(j).append(", ").append(std::to_string(k)).append(")"));
                    auto deer = defaultScene->CreateTransform(stuffGroup, name).lock();
                    deer->SetLocalPosition(glm::vec3(i - cSize * 0.5, j - cSize * 0.5, k - cSize * 0.5));
                    deer->SetLocalScale(glm::vec3(0.01f, 0.01f, 0.01f));

                    auto mComponent = ce::ResourceManager::LoadMesh(deerPath, aiProcess_Triangulate | aiProcess_FlipWindingOrder);
                    if (!mComponent.empty())
                    {
                        for (int l = 0; l < mComponent.size(); l++) {
                            auto meshComponent = defaultScene->CreateComponent<ce::MeshComponent>(deer, mComponent[l], name).lock();
                            meshComponent->SetMaterial(std::make_shared<ce::Material>(ce::ResourceManager::GetShaderProgram(SIMPLE_SHADER)));
                            meshComponent->GetMaterial().lock()->SetTexture(MAIN_TEX, *deerTex);
                        }
                    }
                    else
                    {
                        return std::shared_ptr<ce::Scene>();
                    }
                }
            }
        }
    }

    return defaultScene;
}

int main(int argc, char *argv[]){
    ce::World world = ce::World();

    world.Initialize();
    world.LoadScene(CreateDefaultScene());

    while(true) {
        if(!world.Tick()) break;
    }

    world.Cleanup();

    return 1;
}

