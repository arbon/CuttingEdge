#include "Mesh.h"

std::string const ce::Mesh::POS_ATTRIB_NAME = "position";
std::string const ce::Mesh::NORMAL_ATTRIB_NAME = "normal";
std::string const ce::Mesh::COLOR_ATTRIB_NAME = "color";

ce::Mesh::Mesh(std::shared_ptr<ce::Mesh::MeshData> meshData) {
	this->meshData = meshData;
	this->material = meshData->material;
	this->component = std::weak_ptr<ce::Component>();
}

std::weak_ptr<ce::Mesh::MeshData> ce::Mesh::GetMeshData() const {
	return this->meshData;
}

std::weak_ptr<ce::Material> ce::Mesh::GetMaterial()
{
	return this->material;
}

std::weak_ptr<ce::Component> ce::Mesh::GetComponent()
{
	return this->component;
}

void ce::Mesh::Draw() {
	glBindVertexArray(this->meshData->vao);

	glDrawElements(GL_TRIANGLES, this->meshData->elementsCount, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
}

void ce::Mesh::RetrieveBufferedData() {
    auto meshData = this->meshData;

    glBindVertexArray(meshData->vao);

	glBindBuffer(GL_ARRAY_BUFFER, meshData->vbo);
	std::vector<ce::Vertex> vertices = std::vector<ce::Vertex>(meshData->verticesCount);
    glGetBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(ce::Vertex) * meshData->verticesCount, vertices.data());

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshData->ebo);
	std::vector<GLuint> elements = std::vector<GLuint>(meshData->elementsCount);
    glGetBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(GLuint) * meshData->elementsCount, elements.data());

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	meshData->vertices = vertices;
	meshData->elements = elements;
}
