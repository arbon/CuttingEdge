#include "Camera.h"
#include "World.h"

void * ce::Camera::operator new(size_t size)
{
	return ce::XAlloc::aligned_malloc(size, ce::Camera::ALLOC);
}

void ce::Camera::operator delete(void * p)
{
    ce::XAlloc::aligned_free(p);
}

ce::Camera::Camera(ProjectionMode projectionMode, std::weak_ptr<Transform> transform) : ce::Camera(projectionMode, transform, ce::World::GetWorld()->GetWindowSize().x, ce::World::GetWorld()->GetWindowSize().y)
{
}

ce::Camera::Camera(ce::Camera::ProjectionMode projectionMode, std::weak_ptr<ce::Transform> transform, int width, int height)
{
    auto transformLocked = transform.lock();

    if (!transformLocked)
        return;

	this->projectionMode = projectionMode;

	this->transform = transformLocked;
    transformLocked->SetCacheInverseMatrix(true);

    this->left = -width * 0.5f;
    this->right = width * 0.5f;
    this->top = height * 0.5f;
    this->bottom = -height * 0.5f;

	this->aspect = (float)width / (float)height;
	this->fov = 90.0f;

    this->nearClip = 0.1f;
	this->farClip = 1000.0f;
	this->isProjDirty = true;
}

glm::mat4 ce::Camera::GetProjectionMatrix()
{
	if(this->isProjDirty)
	{
		if(this->projectionMode == Orthographic)
		{
			this->projectionMatrix = glm::ortho(this->left, this->right, this->bottom, this->top, this->nearClip, this->farClip);
		} else
		{
			this->projectionMatrix = glm::perspective(this->fov, this->aspect, this->nearClip, this->farClip);
		}

		this->isProjDirty = false;
	}

	return this->projectionMatrix;
}

glm::mat4 ce::Camera::GetViewMatrix()
{
    if (auto lockedTransform = this->transform.lock()) {
        return lockedTransform->GetInverseWorldMatrix();
    }

    return glm::mat4();
}

glm::mat4 ce::Camera::GetViewProjectionMatrix()
{
    return this->GetProjectionMatrix() * this->GetViewMatrix();
}

glm::vec2 ce::Camera::GetWorldToCameraProjectedPosition(glm::vec3 worldPosition)
{
	glm::vec2 window = ce::World::GetWorld()->GetWindowSize();

    glm::mat4 model = glm::mat4(1.0f);
    glm::vec4 viewport(0.0f, 0.0f, window.x, window.y);

    glm::vec3 projected = glm::project(worldPosition, this->GetViewMatrix(), this->GetProjectionMatrix(), viewport);

	return glm::vec2(projected.x, projected.y);
}

std::weak_ptr<ce::Transform> ce::Camera::GetTransform()
{
	return this->transform;
}

ce::Camera::ProjectionMode ce::Camera::GetProjectionMode() const
{
	return this->projectionMode;
}

void ce::Camera::SetProjectionMode(ce::Camera::ProjectionMode projectionMode)
{
	this->projectionMode = projectionMode;
	this->isProjDirty = true;
}

float ce::Camera::GetLeft() const
{
	return this->left;
}

void ce::Camera::SetLeft(float left)
{
	this->left = left;
	this->isProjDirty = true;
}

float ce::Camera::GetRight() const
{
	return this->right;
}

void ce::Camera::SetRight(float right)
{
	this->right = right;
	this->isProjDirty = true;
}

float ce::Camera::GetTop() const
{
	return this->top;
}

void ce::Camera::SetTop(float top)
{
	this->top = top;
	this->isProjDirty = true;
}

float ce::Camera::GetBottom() const
{
	return this->bottom;
}

void ce::Camera::SetBottom(float bottom)
{
	this->bottom = bottom;
	this->isProjDirty = true;
}

float ce::Camera::GetNear() const
{
	return this->nearClip;
}

void ce::Camera::SetNear(float nearClip)
{
	this->nearClip = nearClip;
	this->isProjDirty = true;
}

float ce::Camera::GetFar() const
{
	return this->farClip;
}

void ce::Camera::SetFar(float farClip)
{
	this->farClip = farClip;
	this->isProjDirty = true;
}

float ce::Camera::GetFOV() const
{
	return this->fov;
}

void ce::Camera::SetFOV(float fov)
{
	this->fov = fov;
	this->isProjDirty = true;
}

float ce::Camera::GetAspect() const
{
	return this->aspect;
}

void ce::Camera::SetAspect(float aspect)
{
	this->aspect = aspect;
	this->isProjDirty = true;
}
