#include "UIRenderLayer.h"

ce::UIRenderLayer::UIRenderLayer()
{
}

void ce::UIRenderLayer::Render(ce::Camera & camera)
{
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    auto i = this->drawables.begin();
    while (i != this->drawables.end()) {
        if (auto drawable = i->lock()) {
            drawable->Draw();
            ++i;
        }
        else {
            i = this->drawables.erase(i);
        }
    }
}

void ce::UIRenderLayer::AddDrawable(std::weak_ptr<ce::Drawable> drawable)
{
    auto ui = std::dynamic_pointer_cast<ce::UIComponent>(drawable.lock());

    if (ui) {
        this->drawables.push_back(drawable);
    }
}
