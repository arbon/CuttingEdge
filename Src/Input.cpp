#include "Input.h"
#include "World.h"

ce::Input::Input() : wrapper(), profile()
{
    this->wrapper.addProfile(&this->profile);
}

si::Profile & ce::Input::GetProfile()
{
    return this->profile;
}

void ce::Input::HandleEvent(SDL_Event& event)
{
    for (auto handler : this->rawInputHandlers) {
        handler(event);
    }

    this->wrapper.handleEvents(&event);
}

void ce::Input::Update()
{
    this->wrapper.update();
}

glm::vec3 ce::Input::ScreenToWorldPosition(glm::vec2 screenPos)
{
    auto world = ce::World::GetWorld();
    auto camera = world->GetScene().lock()->GetActiveCamera().lock().get();
    auto cameraTransform = camera->GetTransform().lock().get();
    glm::vec2 windowSize = world->GetWindowSize();
    glm::vec4 viewport = glm::vec4(0.0f, 0.0f, windowSize.x, windowSize.y);

    return glm::unProject(glm::vec3(screenPos.x, windowSize.y - screenPos.y, 0.0f), glm::inverse(cameraTransform->GetWorldMatrix()), camera->GetProjectionMatrix(), viewport);
}

glm::vec2 ce::Input::WorldToScreenPosition(glm::vec3 worldPos)
{
    auto world = ce::World::GetWorld();
    auto camera = world->GetScene().lock()->GetActiveCamera().lock().get();
    auto cameraTransform = camera->GetTransform().lock().get();
    glm::vec2 windowSize = world->GetWindowSize();
    glm::vec4 viewport = glm::vec4(0.0f, 0.0f, windowSize.x, windowSize.y);

    return glm::project(worldPos, glm::inverse(cameraTransform->GetWorldMatrix()), camera->GetProjectionMatrix(), viewport);
}

void ce::Input::AddRawInputHandler(std::function<void(SDL_Event&)> handler)
{
    this->rawInputHandlers.push_back(handler);
}

glm::vec2 ce::Input::GetMousePosition()
{
    int x, y;
    SDL_GetMouseState(&x, &y);
    return glm::vec2(x, y);
}
