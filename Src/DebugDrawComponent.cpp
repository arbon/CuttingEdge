#include "DebugDrawComponent.h"
#include "TimeUtil.h"
#include "Scene.h"
#include "Camera.h"
#include "ToString.h"

ce::DebugDrawComponent::DebugDrawComponent(std::weak_ptr<ce::Transform> transform, DebugDrawObject drawObject, std::string name) : ce::Component(transform, name), drawObject(drawObject)
{
	timeout = drawObject.LifeTime != -1.0f;
	timeToLive = drawObject.LifeTime;
}

ce::DebugDrawComponent::~DebugDrawComponent()
{
    glDeleteBuffers(1, &this->drawObject.Vbo);
    glDeleteVertexArrays(1, &this->drawObject.Vao);
}

std::weak_ptr<ce::Drawable> ce::DebugDrawComponent::GetDrawable()
{
	return std::dynamic_pointer_cast<ce::Drawable>(this->getPointer());
}

std::weak_ptr<ce::Processable> ce::DebugDrawComponent::GetProcessable()
{
	return std::dynamic_pointer_cast<ce::Processable>(this->getPointer());
}

void ce::DebugDrawComponent::Draw()
{
	glEnable(GL_LINE_SMOOTH);
    glLineWidth(this->drawObject.DrawWidth);
    this->drawObject.Material->SetVector3(this->COLOR_UNIFORM_NAME, this->drawObject.Color);
	this->drawObject.Material->UpdateAllUniforms();

    glBindVertexArray(this->drawObject.Vao);
    glDrawArrays(drawObject.DrawMode, 0, drawObject.Vertices.size() / (this->drawObject.Is2D ? 2 : 3));
    glBindVertexArray(0);
    
    glLineWidth(1);
	glDisable(GL_LINE_SMOOTH);
}

std::weak_ptr<ce::Material> ce::DebugDrawComponent::GetMaterial()
{
	return this->drawObject.Material;
}

std::weak_ptr<ce::Component> ce::DebugDrawComponent::GetComponent()
{
	return this->getPointer();
}

void ce::DebugDrawComponent::Process()
{
	if (!timeout) {
		return;
	}

	if (this->timeToLive <= 0) {
		this->Destroy(this->GetComponent());
	}

	this->timeToLive -= ce::Time::GetDeltaTime();
}
