#include "SpriteComponent.h"

glm::vec2 ce::SpriteComponent::GetSpriteDimensions()
{
    if (this->sprite) {
        if (auto lockedSpriteData = this->sprite->GetSpriteData().lock()) {
            if (lockedSpriteData->texture) {
                return lockedSpriteData->texture->GetDimensions();
            }
        }
    }

	return glm::vec2();
}

glm::vec4 ce::SpriteComponent::GetSpriteCoords()
{
    if (this->sprite) {
        if (auto lockedSpriteData = this->sprite->GetSpriteData().lock()) {
            return lockedSpriteData->coords;
        }
    }

    return glm::vec4();
}

void ce::SpriteComponent::SetSpriteCoords(const glm::vec4 &spritecoords)
{
    if (this->sprite) {
        if (auto lockedSpriteData = this->sprite->GetSpriteData().lock()) {
            lockedSpriteData->coords = this->sprite->FlippedX ? glm::vec4(spritecoords.z, spritecoords.y, spritecoords.x, spritecoords.w) : spritecoords;

            if (auto lockedMaterial = this->GetMaterial().lock()) {
                lockedMaterial->SetVector4(ce::Sprite::COORD_UNIFORM_NAME, lockedSpriteData->coords);
            }
        }
    }
}

void ce::SpriteComponent::SetMaterial(std::shared_ptr<ce::Material> material)
{
    if (!this->sprite) {
        return;
    }

	this->sprite->material = material;
    if (material) {
        glm::vec4 coords = glm::vec4(this->GetSpriteCoords());
        material->SetVector4(ce::Sprite::COORD_UNIFORM_NAME, coords);
    }
}

std::weak_ptr<ce::Material> ce::SpriteComponent::GetMaterial()
{
    if (this->sprite) {
        return this->sprite->GetMaterial();
    }

    return std::weak_ptr<ce::Material>();
}

std::shared_ptr<ce::Sprite>& ce::SpriteComponent::GetSprite()
{
    return this->sprite;
}

ce::SpriteComponent::SpriteComponent(std::weak_ptr<ce::Transform> transform, std::shared_ptr<ce::Sprite> sprite, std::string name) : ce::Component(transform, name)
{
	this->sprite = sprite;
    auto plane = ce::ResourceManager::LoadPrimitiveGeometry(ce::ResourceManager::GEOM_PLANE);
	sprite->_SetMeshData(plane);
    ce::ResourceManager::BufferMesh(plane);

    if (this->sprite) {
        if (auto lockedSpriteData = this->sprite->GetSpriteData().lock()) {
            if (lockedSpriteData->material) {
                this->SetSpriteCoords(lockedSpriteData->coords);
            }
        }
    }
}

std::weak_ptr<ce::Drawable> ce::SpriteComponent::GetDrawable()
{
	return this->sprite;
}

std::weak_ptr<ce::Processable> ce::SpriteComponent::GetProcessable()
{
	return std::weak_ptr<ce::Processable>();
}

void ce::SpriteComponent::Initialize()
{
    if (this->sprite) {
        this->sprite->_SetComponent(this->getPointer());
    }
}
