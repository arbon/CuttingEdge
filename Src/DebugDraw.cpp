#include "DebugDraw.h"
#include "Scene.h"
#include "ResourceManager.h"

std::shared_ptr<ce::Transform> ce::DebugDraw::debugTransform = std::shared_ptr<ce::Transform>();
std::shared_ptr<ce::Material> ce::DebugDraw::material = std::shared_ptr<ce::Material>();

std::string ce::DebugDraw::debugVertShaderPath = "cutting_edge/Resources/Shaders/debug.vert";
std::string ce::DebugDraw::debugFragShaderPath = "cutting_edge/Resources/Shaders/debug.frag";
std::string ce::DebugDraw::debugShaderName = "debug";

std::shared_ptr<ce::Material> ce::DebugDraw::textMaterial = std::shared_ptr<ce::Material>();

std::string ce::DebugDraw::debugTextVertShaderPath = "cutting_edge/Resources/Shaders/debug-text.vert";
std::string ce::DebugDraw::debugTextFragShaderPath = "cutting_edge/Resources/Shaders/debug-text.frag";
std::string ce::DebugDraw::debugTextShaderName = "debug-text";
std::string ce::DebugDraw::debugTextFontPath = "cutting_edge/Resources/Fonts/Delicious-Roman.otf";

std::weak_ptr<ce::DebugDrawComponent> ce::DebugDraw::DrawDebugObject(const std::shared_ptr<ce::Scene>& scene, DebugDrawObject drawObject)
{
	if (!debugTransform) {
		debugTransform = scene->CreateTransform(std::weak_ptr<ce::Transform>(), "DebugDraw").lock();
	}

    if (!material) {
        auto shaderProgram = ce::ResourceManager::GetShaderProgram(debugShaderName);

        if (!shaderProgram) {
            shaderProgram = std::make_shared<ce::ShaderProgram>(ce::ShaderProgram(debugShaderName));

            shaderProgram->shaders.push_back(ce::ResourceManager::LoadShader(debugVertShaderPath));
            shaderProgram->shaders.push_back(ce::ResourceManager::LoadShader(debugFragShaderPath));

            ce::ResourceManager::CompileShaderProgram(shaderProgram);

            std::string shaderError;

            for (auto test : shaderProgram->shaders) {
                shaderProgram->CheckShaderStatus(*test.lock(), shaderError);
            }

            std::cout << shaderError;
        }

        material = std::make_shared<ce::Material>(shaderProgram);
    }

    drawObject.Material = material;

    int dimension = drawObject.Is2D ? 2 : 3;
    GLsizeiptr vertexUnitSize = sizeof(glm::vec3) / 3;

    glGenVertexArrays(1, &drawObject.Vao);
    glBindVertexArray(drawObject.Vao);

    glGenBuffers(1, &drawObject.Vbo);

    glBindBuffer(GL_ARRAY_BUFFER, drawObject.Vbo);
    glBufferData(GL_ARRAY_BUFFER, vertexUnitSize * drawObject.Vertices.size(), &drawObject.Vertices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, dimension, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

	return scene->CreateComponent<ce::DebugDrawComponent>(debugTransform, drawObject);
}

std::weak_ptr<ce::DebugDrawComponent> ce::DebugDraw::DrawCube(const std::shared_ptr<ce::Scene>& scene, glm::vec3 position, glm::quat rotation, glm::vec3 dimensions, glm::vec3 color, float lifetime, float lineWidth)
{
	const std::vector<glm::vec3> points = { 
		{ -0.5f, -0.5f, -0.5f }, { 0.5f, -0.5f, -0.5f },
		{ -0.5f, -0.5f, -0.5f }, { -0.5f, 0.5f, -0.5f },
		{ -0.5f, -0.5f, -0.5f }, { -0.5f, -0.5f, 0.5f },

		{ 0.5f, 0.5f, 0.5f }, { 0.5f, 0.5f, -0.5f },
		{ 0.5f, 0.5f, 0.5f }, { -0.5f, 0.5f, 0.5f },
		{ 0.5f, 0.5f, 0.5f }, { 0.5f, -0.5f, 0.5f },

		{ 0.5f, -0.5f, -0.5f }, { 0.5f, 0.5f, -0.5f },
		{ 0.5f, -0.5f, -0.5f }, { 0.5f, -0.5f, 0.5f },

		{ -0.5f, -0.5f, 0.5f }, { 0.5f, -0.5f, 0.5f },
		{ -0.5f, -0.5f, 0.5f }, { -0.5f, 0.5f, 0.5f },

		{ -0.5f, 0.5f, -0.5f }, { -0.5f, 0.5f, 0.5f },
		{ -0.5f, 0.5f, -0.5f }, { 0.5f, 0.5f, -0.5f }
	};

	DebugDrawObject drawObject;
	drawObject.Color = color;
	drawObject.Is2D = false;
	drawObject.DrawMode = GL_LINES;
	drawObject.LifeTime = lifetime;
	drawObject.DrawWidth = lineWidth;

    glm::mat4 matrix = ce::Transform::CalculateMatrixFromComponents(position, rotation, dimensions);

	for (glm::vec3 point : points) {
        glm::vec4 transformedPoint = matrix * glm::vec4(point.x, point.y, point.z, 1.0f);

        drawObject.Vertices.push_back(transformedPoint.x);
        drawObject.Vertices.push_back(transformedPoint.y);
        drawObject.Vertices.push_back(transformedPoint.z);
	}

	return DrawDebugObject(scene, drawObject);
}

std::weak_ptr<ce::DebugDrawComponent> ce::DebugDraw::DrawLines(const std::shared_ptr<ce::Scene>& scene, std::vector<glm::vec3> positions, glm::vec3 color, float lifetime, float lineWidth)
{
    DebugDrawObject drawObject;
    drawObject.Color = color;
    drawObject.Is2D = false;
    drawObject.DrawMode = GL_LINE_STRIP;
    drawObject.LifeTime = lifetime;
    drawObject.DrawWidth = lineWidth;

    float* floatPositions = reinterpret_cast<float*>(positions.data());
    drawObject.Vertices.insert(drawObject.Vertices.end(), floatPositions, floatPositions + positions.size() * 3);
    
    return DrawDebugObject(scene, drawObject);
}

std::weak_ptr<ce::DebugDrawComponent> ce::DebugDraw::DrawLine(const std::shared_ptr<ce::Scene>& scene, glm::vec3 start, glm::vec3 end, glm::vec3 color, float lifetime, float lineWidth)
{
    DebugDrawObject drawObject;
    drawObject.Color = color;
    drawObject.Is2D = false;
    drawObject.DrawMode = GL_LINE_STRIP;
    drawObject.LifeTime = lifetime;
    drawObject.DrawWidth = lineWidth;

    drawObject.Vertices.push_back(start.x);
    drawObject.Vertices.push_back(start.y);
    drawObject.Vertices.push_back(start.z);
    drawObject.Vertices.push_back(end.x);
    drawObject.Vertices.push_back(end.y);
    drawObject.Vertices.push_back(end.z);

    return DrawDebugObject(scene, drawObject);
}

std::weak_ptr<ce::TextComponent> ce::DebugDraw::DrawText(const std::shared_ptr<ce::Scene>& scene, std::string text, glm::vec3 minBound, glm::vec3 maxBound, float fontSize, glm::vec3 color, float lifetime)
{
	if (!debugTransform) {
		debugTransform = scene->CreateTransform(std::weak_ptr<ce::Transform>(), "DebugDraw").lock();
	}

    if (!textMaterial) {
        auto shaderProgram = ce::ResourceManager::GetShaderProgram(debugTextShaderName);

        if (!shaderProgram) {
            shaderProgram = std::make_shared<ce::ShaderProgram>(ce::ShaderProgram(debugTextShaderName));

            shaderProgram->shaders.push_back(ce::ResourceManager::LoadShader(debugTextVertShaderPath));
            shaderProgram->shaders.push_back(ce::ResourceManager::LoadShader(debugTextFragShaderPath));

            ce::ResourceManager::CompileShaderProgram(shaderProgram);

            std::string shaderError;

            for (auto test : shaderProgram->shaders) {
                shaderProgram->CheckShaderStatus(*test.lock(), shaderError);
            }

            std::cout << shaderError;
        }

        textMaterial = std::make_shared<ce::Material>(shaderProgram);
    }

    TextComponentParams params = TextComponentParams(debugTextFontPath, text, textMaterial, minBound, maxBound, fontSize, color, lifetime, true);

	return scene->CreateComponent<ce::TextComponent>(debugTransform, params);
}