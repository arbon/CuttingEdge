#include "Platformer2DPlayerMovement.h"
#include "ToString.h"

ce::Platformer2DPlayerMovement::Platformer2DPlayerMovement(std::weak_ptr<Transform> transform, std::shared_ptr<STRigidbody> rigidbody, std::string name) 
	: right(false), left(false), currentJumpBoostRemaining(0.0f), delayFrame(false), jumpGuard(0), useDelayFrame(false), useJumpGuard(true), ce::Component(transform, name)
{
	this->rigidbody = rigidbody;
	this->jumpBoostAccel = 4.0f;
	this->jumpVelocity = 14.0f;
	this->maxJumpBoostTime = 0.6f;
	this->maxVelocityBounds = glm::vec4(-7.0f, 7.0f, -44.0f, 44.0f);
	this->xAccel = 0.8f;
	this->airXAccel = 1.5f;
	this->xVelocity = 3.5f;
}

void ce::Platformer2DPlayerMovement::MoveLeft(bool start)
{
	this->left = start;

	if (this->Animator) {
		if (!this->right && this->left) {
			if (this->RunAnim) {
				this->Animator->Play(this->RunAnim);
			}
			else {
				this->Animator->Stop();
			}
		}
		else {
			if (this->IdleAnim) {
				this->Animator->Play(this->IdleAnim);
			}
			else {
				this->Animator->Stop();
			}
		}
	}

// hack to allow direction change at same time as jump if move is called prior to jump (for ai controller)
	// if(this->left)
	// {
	// 	if(!(this->rigidbody->isJumping || (this->jumpGuard > 0 && this->useJumpGuard)))
	// 	{
	// 		this->rigidbody->SetVelocity({ std::min(this->rigidbody->GetVelocity().x, -this->xVelocity), this->rigidbody->GetVelocity().y });
	// 	}
	// }

	if(this->left) delayFrame = true;
}

void ce::Platformer2DPlayerMovement::MoveRight(bool start)
{
	this->right = start;

	if (this->Animator) {
		if (!this->left && this->right) {
			if (this->RunAnim) {
				this->Animator->Play(this->RunAnim);
			}
			else {
				this->Animator->Stop();
			}
		}
		else {
			if (this->IdleAnim) {
				this->Animator->Play(this->IdleAnim);
			}
			else {
				this->Animator->Stop();
			}
		}
	}

// hack to allow direction change at same time as jump if move is called prior to jump (for ai controller)
	// if(this->right)
	// {
	// 	if(!(this->rigidbody->isJumping || (this->jumpGuard > 0 && this->useJumpGuard)))
	// 	{
	// 		this->rigidbody->SetVelocity({ std::max(this->rigidbody->GetVelocity().x, this->xVelocity), this->rigidbody->GetVelocity().y });
	// 	}
	// }

	if(this->right) delayFrame = true;
}

void ce::Platformer2DPlayerMovement::Jump(bool start, bool force)
{
	if ((!this->rigidbody->isJumping || force) && start) {
		//this->rigidbody->body->ApplyForceToCenter({ 0.0f, 950.0f }, true);		
		this->rigidbody->SetVelocity(this->rigidbody->GetVelocity() + glm::vec2(0.0f, this->jumpVelocity));
		this->currentJumpBoostRemaining = this->maxJumpBoostTime;
		this->jumpGuard = 5;
	}

	if(!start)
	{
		this->currentJumpBoostRemaining = 0.0f;
	}
}

std::weak_ptr<ce::Drawable> ce::Platformer2DPlayerMovement::GetDrawable()
{
	return std::weak_ptr<ce::Drawable>();
}

std::weak_ptr<ce::Processable> ce::Platformer2DPlayerMovement::GetProcessable()
{
	return std::dynamic_pointer_cast<ce::Processable>(this->getPointer());
}

void ce::Platformer2DPlayerMovement::Process()
{
    float right = this->right ? this->left ? 0.0f : 1.0f : this->left ? -1.0f : 0.0f;

    auto transformLocked = this->transform.lock();
    if (right && this->Sprite) {
        this->Sprite->FlippedX = right == -1.0f;
    }

	glm::vec2 velocity = this->rigidbody->GetVelocity();
	glm::vec2 acceleration = glm::vec2(0.0f, 0.0f);
	float timeStep = ce::World::GetWorld()->GetPhysicsTickTime();

	if(this->rigidbody->isJumping || (this->jumpGuard > 0 && this->useJumpGuard))
	{
  		float jumpBoostAmt = this->currentJumpBoostRemaining > 0.0f ? std::min(this->currentJumpBoostRemaining, timeStep) : 0.0f;
		jumpBoostAmt = std::floor(jumpBoostAmt / timeStep) * timeStep;
		this->currentJumpBoostRemaining = std::max(0.0f, this->currentJumpBoostRemaining - timeStep);
		acceleration.y = jumpBoostAmt * this->jumpBoostAccel / timeStep;// * timeStep * timeStep;
		acceleration.x = this->airXAccel * right;
		jumpGuard--;
	} else 
	{
		if(!((velocity.x > 0.0f && right > 0.0f) || (velocity.x < 0.0f && right < 0.0f))){
			velocity.x = this->xVelocity * right;
		}

		acceleration.x = this->xAccel * right;
	}

	this->rigidbody->SetVelocity(velocity); //glm::vec2(speed * right, this->rigidbody->GetVelocity().y));

	if(this->delayFrame && this->useDelayFrame)
	{
		acceleration.x = 0.0f;
		delayFrame = false;
	}

	// if(velocity.x < this->maxVelocityBounds.y && velocity.x + acceleration.x > this->maxVelocityBounds.y)
	// {
	// 	std::cout << "pre-max vel x: " << velocity.x << " pos: " << this->rigidbody->body->GetPosition().x << "\n";
	// }

	this->rigidbody->ApplyAcceleration(acceleration);

	velocity = this->rigidbody->GetVelocity();

	velocity.x = std::min(this->maxVelocityBounds.y, std::max(this->maxVelocityBounds.x, velocity.x));
	velocity.y = std::min(this->maxVelocityBounds.w, std::max(this->maxVelocityBounds.z, velocity.y));
	this->rigidbody->SetVelocity(velocity);

}