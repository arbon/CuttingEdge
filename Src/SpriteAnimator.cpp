#include "SpriteAnimator.h"

ce::SpriteAnimator::SpriteAnimator(std::weak_ptr<ce::Transform> transform, std::string name, std::shared_ptr<ce::SpriteComponent> sprite) : ce::Component(transform, name)
{
	this->playPosition = 0.0f;
	this->playRate = 1.0f;
	this->isPlaying = false;
	this->sprite = sprite;
}

void ce::SpriteAnimator::Play(std::string name)
{
	for(auto anim : this->animations)
	{
		if (anim->name == name) {
			this->playPosition = 0.0f;
			this->currentAnimation = anim;
			this->isPlaying = true;
			return;
		}
	}

	this->Stop();
}

void ce::SpriteAnimator::Play(std::shared_ptr<ce::SpriteAnimation> animation)
{
	this->currentAnimation = animation;
	if(this->CanPlay()) {
		this->playPosition = 0.0f;
		this->isPlaying = true;
		return;
	}

	this->Stop();
}

void ce::SpriteAnimator::Stop()
{
	this->isPlaying = false;
	this->playPosition = 0.0f;
}

std::weak_ptr<ce::Drawable> ce::SpriteAnimator::GetDrawable()
{
	return std::weak_ptr<ce::Drawable>();
}

std::weak_ptr<ce::Processable> ce::SpriteAnimator::GetProcessable()
{
	return std::dynamic_pointer_cast<ce::Processable>(this->getPointer());
}

void ce::SpriteAnimator::Process()
{
	if (this->isPlaying && this->currentAnimation) {
		float delta = ce::Time::GetDeltaTime();
		int frames = this->currentAnimation->frames.size();
		int currentFrameIndex = floor(this->playPosition * frames);
		float nextFramePos = this->playPosition * frames + this->currentAnimation->fps * delta * this->playRate;
		this->playPosition = nextFramePos / ((float)frames);
		this->playPosition = this->playPosition - (long) this->playPosition;
		int nextFrameIndex = floor(this->playPosition * frames);
        //std::cout << nextFramePos << std::endl;

		if (currentFrameIndex != nextFrameIndex && this->CanPlay()) {
			glm::vec4 nextFrame = this->currentAnimation->frames[nextFrameIndex];
			this->sprite->SetSpriteCoords(ce::Sprite::CalculateSpriteCoords(nextFrame, this->sprite->GetSpriteDimensions()));
		}
	}
}

bool ce::SpriteAnimator::CanPlay()
{
	return this->currentAnimation != nullptr && this->sprite != nullptr;
}

