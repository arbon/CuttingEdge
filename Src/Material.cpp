#include "Material.h"
#include <iostream>

GLuint ce::Material::boundProgram = 0;

ce::Material::Material() : shader()
{
}

ce::Material::Material(std::shared_ptr<ce::ShaderProgram> shader) : propTest(shader->properties.size()), propertyData(shader->properties.size())
{
	this->shader = shader;

	for (unsigned int i = 0; i < shader->properties.size(); i++)
	{
		this->propertyMap[shader->properties[i].first] = i;

		this->propTest[i] = shader->properties[i].first;
	}
}

void ce::Material::Bind()
{
	if(ce::Material::boundProgram == this->shader->programId)
	{
		return;
	}

	glUseProgram(this->shader->programId);
    ce::Material::boundProgram = this->shader->programId;
}

void ce::Material::SetFloat(std::string name, GLfloat& data)
{
    if (this->propertyMap.find(name) == this->propertyMap.end()) {
        return;
    }

	this->propertyData[this->propertyMap[name]] = std::make_shared<GLfloat>(data);
}

void ce::Material::SetVector2(std::string name, glm::vec2& data)
{
    if (this->propertyMap.find(name) == this->propertyMap.end()) {
        return;
    }

	this->propertyData[this->propertyMap[name]] = std::make_shared<glm::vec2>(data);
}

void ce::Material::SetVector3(std::string name, glm::vec3& data)
{
    if (this->propertyMap.find(name) == this->propertyMap.end()) {
        return;
    }

    this->propertyData[this->propertyMap[name]] = std::make_shared<glm::vec3>(data);
}

void ce::Material::SetVector4(std::string name, glm::vec4& data)
{
    if (this->propertyMap.find(name) == this->propertyMap.end()) {
        return;
    }

    this->propertyData[this->propertyMap[name]] = std::make_shared<glm::vec4>(data);
}

void ce::Material::SetMatrix4(std::string name, const glm::mat4& data)
{
    if (this->propertyMap.find(name) == this->propertyMap.end()) {
        return;
    }

	for (unsigned int i = 0; i < propTest.size(); i++)
	{
		if(propTest[i] == name)
		{
			this->propertyData[i] = std::make_shared<glm::mat4>(data);
			return;
		}
	}

	//this->propertyData[this->propertyMap[name]] = &data[0][0];
}

void ce::Material::SetTexture(std::string name, ce::Texture& data)
{
    if (this->propertyMap.find(name) == this->propertyMap.end()) {
        return;
    }

	this->propertyData[this->propertyMap[name]] = std::make_shared<GLuint>(data.data->index);
}

void ce::Material::SetTexture(std::string name, GLuint & data)
{
    if (this->propertyMap.find(name) == this->propertyMap.end()) {
        return;
    }

    this->propertyData[this->propertyMap[name]] = std::make_shared<GLuint>(data);

}

void ce::Material::UpdateAllUniforms()
{
	unsigned int samplerCount = 0;

	for (unsigned int i = 0; i < this->shader->properties.size(); i++)
	{
		auto propType = &this->shader->properties[i];
		std::shared_ptr<void>& pData = this->propertyData[i];

		if(pData == nullptr)
		{
			continue;
		}

		switch (propType->second) {
		case GL_FLOAT_MAT4:
        {
            glUniformMatrix4fv(i, 1, GL_FALSE, (GLfloat*)pData.get());
            break;
        }
        case GL_FLOAT_VEC2:
        {
            glm::vec2 data = *(glm::vec2*)pData.get();
            glUniform2f(i, data[0], data[1]);
            break;
        }
        case GL_FLOAT_VEC3:
        {
            glm::vec3 data = *(glm::vec3*)pData.get();
            glUniform3f(i, data[0], data[1], data[2]);
            break;
        }
		case GL_FLOAT_VEC4:
        {
            glm::vec4 data = *(glm::vec4*)pData.get();
            glUniform4f(i, data[0], data[1], data[2], data[3]);
            break;
        }
		case GL_FLOAT:
			glUniform1f(i, *(GLfloat*)pData.get());
			break;
		case GL_SAMPLER_2D:
			glActiveTexture(GL_TEXTURE0 + samplerCount);
			glBindTexture(GL_TEXTURE_2D, *(GLuint*)pData.get());
            glUniform1i(i, samplerCount);
			++samplerCount;
			break;
        case GL_SAMPLER_2D_ARRAY:
            glActiveTexture(GL_TEXTURE0 + samplerCount);
            glBindTexture(GL_TEXTURE_2D_ARRAY, *(GLuint*)pData.get());
            glUniform1i(i, samplerCount);
            ++samplerCount;
            break;
		}
	}
}

void ce::Material::Unbind()
{
	if (ce::Material::boundProgram == this->shader->programId)
	{
		glUseProgram(0);
        ce::Material::boundProgram = 0;
	}
}

void ce::Material::SetShader(std::shared_ptr<ce::ShaderProgram> shader)
{
	this->shader = shader;

	this->propertyData.clear();
	this->propertyMap.clear();

	this->propertyData.resize(shader->properties.size());

	for (unsigned int i = 0; i < shader->properties.size(); i++)
	{
		this->propertyMap[shader->properties[i].first] = i;
	}

	this->UpdateAllUniforms();
}
