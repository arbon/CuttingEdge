#include "World.h"
#include "Input.h"
#include "RHI.h"

ce::World* ce::World::instance = nullptr;

ce::World::World() :
    dt(1.0f/60.0f),
    accumulator(0.0f),
    t(0.0f),
    Input(),
    ShowFPS(false),
    platformUtils(nullptr),
    commandLine() {
}

void ce::World::Initialize(WorldConfig worldConfig)
{
    ce::World::instance = this;

    this->commandLine = CommandLine(worldConfig.CommandLineArguments);
    this->platformUtils = std::make_unique<PlatformUtils>(
        #if BAZEL_RUNFILES_ENABLED
            worldConfig.CommandLineArguments[0],
            worldConfig.RunfilesRepository
        #endif
    );
    this->logfile.open("../log.txt");

#ifdef _WIN32
    RedirectIOToConsole();
#endif

    SDL_Init(SDL_INIT_VIDEO);

    if(*this->commandLine.GetKeyValueArgument("--rhi") == "opengl")
    {
        this->rhi = std::make_shared<OpenGLRHI>();
    } else {
        this->rhi = std::make_shared<VulkanRHI>();
    }
    
    CreateWindowParameters mainWindowParams;
    mainWindowParams.name = worldConfig.WindowName;
    mainWindowParams.width = worldConfig.WindowWidth;
    mainWindowParams.height = worldConfig.WindowHeight;
    mainWindowParams.rhi = this->rhi;

    this->mainWindow = std::make_unique<Window>(mainWindowParams);
    if(this->rhi->InitialiseRHI(this->mainWindow.get()) != RHI_Error::OK)
    {
        std::cerr << "Failed to intialise RHI" << std::endl;
        return;
    }

    auto error = glGetError();
    if(error)
    {
        std::cout << "ERROR: " << error << std::endl;
    }

    ce::Time();
    ce::Time::isFixedTimeStep = true;
    ce::Time::fixedTimeStep = this->dt;

    for (auto pSystem : this->physicsSystems) {
        pSystem->Initialize();
    }
}

int ce::World::Tick() {
    if(!this->scene) {
        return 1;
    }

    this->Input.Update();

    while (SDL_PollEvent(&this->windowEvent))
    {
        this->Input.HandleEvent(this->windowEvent);

        if (this->windowEvent.type == SDL_QUIT) return 0;
        if (this->windowEvent.type == SDL_KEYUP &&
            this->windowEvent.key.keysym.sym == SDLK_ESCAPE) return 0;
    }

    this->frameCount++;
    this->totalTime += ce::Time::GetRealDeltaTime();
    ce::Time::Tick();

    this->accumulator += ce::Time::GetRealDeltaTime();

    if (this->ShowFPS && this->accumulator >= this->dt) {
        //std::cout << std::to_string(ce::Time::GetDeltaTime()) << "\n";
        std::cout << std::to_string(1.0f / this->accumulator) << "\n";
    }

    while (this->accumulator >= this->dt) {

        this->scene->Process();
        for (auto pSystem : this->physicsSystems) {
            pSystem->Step(this->dt);
        }
        this->t += this->dt;
        this->accumulator -= this->dt;
    }

    this->rhi->DrawFrame(this->mainWindow.get(), [this]() { this->scene->Draw(); });

    return 1;
}

void ce::World::LoadScene(std::string path) {

}

void ce::World::LoadScene(std::shared_ptr<ce::Scene> scene) {
    this->scene = scene;
}

void ce::World::AddPhysicsSystem(std::shared_ptr<ce::PhysicsSystem> physics) {
    this->physicsSystems.push_back(physics);
}

void ce::World::Cleanup() {
    this->logfile << "Average frame latency: " << this->totalTime / this->frameCount << "\n";
    this->logfile << "Average frame rate: " << this->frameCount / this->totalTime << "\n";

    this->logfile.close();
    this->rhi->CleanupRHI();
    SDL_Quit();
}

std::weak_ptr<ce::Scene> ce::World::GetScene() const
{
    return this->scene;
}

std::weak_ptr<ce::RHI> ce::World::GetRHI() const
{
    return this->rhi;
}

const ce::PlatformUtils& ce::World::GetPlatformUtils() const
{
    return *this->platformUtils;
}

double ce::World::GetTimeSinceWorldCreation() const
{
    return this->totalTime;
}

const ce::CommandLine& ce::World::GetCommandLine() const
{
    return this->commandLine;
}

glm::vec2 ce::World::GetWindowSize()
{
    return this->mainWindow->GetWindowSize();
}

ce::World* ce::World::GetWorld()
{
    return ce::World::instance;
}

float ce::World::GetPhysicsTickTime()
{
    return this->dt;
}
