#include "AnonymousProcessable.h"

ce::AnonymousProcessable::AnonymousProcessable(std::function<void()> process) : process(process)
{
}

void ce::AnonymousProcessable::Process()
{
    if (this->process) {
        this->process();
    }
}
