#include "Vertex.h"

// GLuint const ce::Vertex::SIZE = sizeof(GLfloat) * 12;
// GLuint const ce::Vertex::POS_OFFSET = 0;
// GLuint const ce::Vertex::NORMAL_OFFSET = sizeof(GLfloat) * 3;
// GLuint const ce::Vertex::COLOR_OFFSET = sizeof(GLfloat) * 6;
// GLuint const ce::Vertex::UV_OFFSET = sizeof(GLfloat) * 10;

ce::Vertex::Vertex() : position(), normal(), color(), uv()
{
}

ce::Vertex::Vertex(glm::vec3 position) : normal(), color(), uv()
{
	this->position = position;
}

ce::Vertex::Vertex(glm::vec3 position, glm::vec3 normal) : color(), uv()
{
	this->position = position;
	this->normal = normal;
}

ce::Vertex::Vertex(glm::vec3 position, glm::vec3 normal, glm::vec4 color) : uv()
{
	this->position = position;
	this->normal = normal;
	this->color = color;
}

ce::Vertex::Vertex(glm::vec3 position, glm::vec3 normal, glm::vec4 color, glm::vec2 uv)
{
	this->position = position;
	this->normal = normal;
	this->color = color;
	this->uv = uv;
}

ce::Vertex::Vertex(glm::vec3 position, glm::vec3 normal, glm::vec3 color) : uv()
{
    this->position = position;
    this->normal = normal;
    this->color = glm::vec4(color.x, color.y, color.z, 1.0f);
}

ce::Vertex::Vertex(glm::vec3 position, glm::vec3 normal, glm::vec3 color, glm::vec2 uv)
{
    this->position = position;
    this->normal = normal;
    this->color = glm::vec4(color.x, color.y, color.z, 1.0f);
    this->uv = uv;
}