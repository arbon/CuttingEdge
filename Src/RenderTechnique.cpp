#include "RenderTechnique.h"

ce::RenderTechnique::RenderTechnique() : layers(), initializedBuffers()
{
}

void ce::RenderTechnique::InitializeLayers()
{
	for (unsigned int i = 0; i < this->layers.size(); i++)
	{
		auto inputReqs = this->layers[i]->GetRequiredInputs();

		auto inputs = std::vector<std::shared_ptr<ce::RenderLayer::TexBuffer>>();

		for (unsigned int j = 0; j < inputReqs.size(); j++)
		{
			auto reqBuffer = this->initializedBuffers.at(inputReqs[j]->name);

			inputs.push_back(reqBuffer);
		}

		this->layers[i]->Initialize(inputs);

		auto outputs = this->layers[i]->GetOutputBuffers();

		for (unsigned int j = 0; j < outputs.size(); j++)
		{
			this->initializedBuffers[outputs[j]->name] = outputs[j];
		}
	}
}

void ce::RenderTechnique::Render(ce::Camera& camera)
{
	for (unsigned int i = 0; i < this->layers.size(); i++)
	{
		this->layers[i]->Render(camera);
	}
}

void ce::RenderTechnique::AddDrawable(std::weak_ptr<ce::Drawable> drawable)
{
    for (auto layer = this->layers.begin(); layer != this->layers.end(); ++layer)
    {
        (**layer).AddDrawable(drawable);
    }
}
