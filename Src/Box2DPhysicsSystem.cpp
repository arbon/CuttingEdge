#include "Box2DPhysicsSystem.h"

ce::Box2DPhysicsSystem::Box2DPhysicsSystem(b2Vec2 gravity) : world(b2World(gravity)) {
}

void ce::Box2DPhysicsSystem::Initialize()
{

}

void ce::Box2DPhysicsSystem::Step(float dt)
{
	this->world.Step(dt, this->velocityIterations, this->positionIterations);
}
