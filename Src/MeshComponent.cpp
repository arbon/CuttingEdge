#include "MeshComponent.h"

void ce::MeshComponent::SetMaterial(std::shared_ptr<ce::Material> material)
{
    if(this->mesh)
        this->mesh->material = material;
}

std::weak_ptr<ce::Material> ce::MeshComponent::GetMaterial()
{
	return mesh ? this->mesh->GetMaterial() : std::weak_ptr<ce::Material>();
}

ce::MeshComponent::MeshComponent(std::weak_ptr<ce::Transform> transform, std::shared_ptr<ce::Mesh> mesh, std::string name) : ce::Component(transform, name)
{
    if (mesh) {
        this->mesh = mesh;

        if (auto meshDataLocked = mesh->GetMeshData().lock()) {
            ce::ResourceManager::BufferMesh(meshDataLocked);
        }
    }
}

std::weak_ptr<ce::Drawable> ce::MeshComponent::GetDrawable()
{
	return this->mesh;
}

std::weak_ptr<ce::Processable> ce::MeshComponent::GetProcessable()
{
	return std::weak_ptr<ce::Processable>();
}