#include "UIEventListener.h"

ce::UIEventListener::UIEventListener(std::function<void(RML_CORE::Event&event)> listener) : listener(listener)
{
}

void ce::UIEventListener::ProcessEvent(RML_CORE::Event & event)
{
    this->listener(event);
}
