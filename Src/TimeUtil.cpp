#include "TimeUtil.h"

float ce::Time::deltaTime = 0.0f;
long long ce::Time::currentTime = 0;
ce::Time* ce::Time::instance = nullptr;
bool ce::Time::isFixedTimeStep = false;
float ce::Time::fixedTimeStep = 0.0f;

std::unordered_map<std::string, long long> ce::Time::flags = std::unordered_map<std::string, long long>();

ce::Time::Time()
{
	if(instance != nullptr)
	{
		throw 1;
	}

	instance = this;

	isFixedTimeStep = false;
	fixedTimeStep = 0.0f;
	currentTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
}

void ce::Time::Tick()
{
	long long newTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
	deltaTime = (newTime - currentTime) * 0.000001f;

	currentTime = newTime;
}

float ce::Time::GetRealDeltaTime()
{
	return deltaTime;
}

float ce::Time::GetDeltaTime()
{
	return isFixedTimeStep ? fixedTimeStep : deltaTime;
}

float ce::Time::Flag(std::string flag)
{
	long long cTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

	if(!flags.count(flag))
	{
		flags[flag] = cTime;
		return 0.0f;
	}

	float dTime = (cTime - flags[flag]) * 0.000001f;
	flags[flag] = cTime;

	return dTime;
}
