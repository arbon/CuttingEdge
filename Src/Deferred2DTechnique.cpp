#include "Deferred2DTechnique.h"

#include "Deferred2DRenderLayer.h"
#include "Deferred2DBufferLayer.h"
#include "UIRenderLayer.h"
#include "DebugRenderLayer.h"

ce::Deferred2DTechnique::Deferred2DTechnique() : ce::RenderTechnique()
{
    this->layers.push_back(std::unique_ptr<ce::Deferred2DBufferLayer>(new ce::Deferred2DBufferLayer()));
    this->layers.push_back(std::unique_ptr<ce::Deferred2DRenderLayer>(new ce::Deferred2DRenderLayer()));
	this->layers.push_back(std::unique_ptr<ce::UIRenderLayer>(new ce::UIRenderLayer()));
	this->layers.push_back(std::unique_ptr<ce::DebugRenderLayer>(new ce::DebugRenderLayer()));
}

void ce::Deferred2DTechnique::InitializeLayers()
{
    if(!this->isVulkan)
    {
        RenderTechnique::InitializeLayers();
        return;
    }

    VkAttachmentDescription colorAttachment{};
    // colorAttachment.format = this->surfaceFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;


    VkAttachmentReference colorAttachmentRef{};
    // refers to the shader out param for the colour value in the fragment shader
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;


    VkRenderPass renderPass;


    VkRenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
}

