#include "TextComponent.h"
#include "TimeUtil.h"
#include "Scene.h"
#include "Camera.h"
#include "World.h"
#include "ToString.h"
#include "PlatformUtils.h"
#include <ft2build.h>
#include <freetype/freetype.h>
// #include FT_FREETYPE_H This is the recommended way to include the above header but compile_commands generation complains

std::vector<ce::FontCharacter> ce::TextComponent::Characters = std::vector<ce::FontCharacter>();
std::string ce::TextComponent::DefaultFontPath = "cutting_edge/Resources/Fonts/Delicious-Roman.otf";
unsigned int ce::TextComponent::DefaultNumChars = 233;
unsigned int ce::TextComponent::DefaultPixelHeight = 24;

void ce::TextComponent::LoadCharacters(std::string fontPath, unsigned int numCharacters, unsigned int pixelHeight)
{
	std::string resolvedFontPath = ce::ResolvePath(fontPath).string();

	FT_Library ft;
	if (FT_Init_FreeType(&ft))
	{
		std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
	}

	FT_Face face;
	if (FT_New_Face(ft, resolvedFontPath.c_str(), 0, &face))
	{
		std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
		return;
	}

	FT_Set_Pixel_Sizes(face, 0, pixelHeight);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction
  
	for (GLubyte c = 0; c < numCharacters; c++)
	{
		// Load character glyph 
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}
		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Now store character for later use
		FontCharacter character = {
			texture, 
			glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			static_cast<GLuint>(face->glyph->advance.x)
		};
		Characters.push_back(character);
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ft);
}

ce::TextComponent::TextComponent(std::weak_ptr<ce::Transform> transform, ce::TextComponentParams params, std::string name) : ce::Component(transform, name), Params(params), timeout(false)
{
	if(!Characters.size()) LoadCharacters(Params.FontPath == "" ? DefaultFontPath : Params.FontPath, DefaultNumChars, DefaultPixelHeight);

	glGenVertexArrays(1, &this->vao);
	glGenBuffers(1, &this->vbo);
	glBindVertexArray(this->vao);
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	if(Params.Lifetime != -1.0f)
	{
		this->timeout = true;
		this->timeToLive = Params.Lifetime;
	}

	if(Params.Is2D)
	{
		glm::vec2 screenSize = World::GetWorld()->GetWindowSize();
		this->mvp = glm::ortho(0.0f, screenSize.x, 0.0f, screenSize.y) * glm::translate(glm::mat4(1.0f), params.MinBound);
	}

}

ce::TextComponent::~TextComponent()
{
    glDeleteBuffers(1, &this->vbo);
    glDeleteVertexArrays(1, &this->vao);
}

std::weak_ptr<ce::Drawable> ce::TextComponent::GetDrawable()
{
	return std::dynamic_pointer_cast<ce::Drawable>(this->getPointer());
}

std::weak_ptr<ce::Processable> ce::TextComponent::GetProcessable()
{
	return std::dynamic_pointer_cast<ce::Processable>(this->getPointer());
}

void ce::TextComponent::Draw()
{
	if(this->Params.Is2D)
	{
		float x = 0.0f;
		float scale = 1.0f;

		std::string::const_iterator c;
		for (c = Params.Text.begin(); c != Params.Text.end(); c++)
		{
			FontCharacter ch = Characters[(unsigned int)*c];
			x += (ch.Bearing.x + ch.Size.x + (ch.Advance >> 6)) * scale;
		}

		float textScale = (this->Params.MaxBound.x - this->Params.MinBound.x) / x;

		const std::string mvp_string = "MVP";
		this->Params.Material->SetMatrix4(mvp_string, this->mvp * glm::scale(glm::mat4(1.0f), glm::vec3(textScale)));
	}

    this->Params.Material->SetVector3("color", this->Params.Colour);
	this->Params.Material->UpdateAllUniforms();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 

    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(this->vao);

	float scale = 1.0f;
	float x = 0.0f;
	float y = 0.0f;

    // Iterate through all characters
    std::string::const_iterator c;
    for (c = Params.Text.begin(); c != Params.Text.end(); c++)
    {
        FontCharacter ch = Characters[(unsigned int)*c];

        GLfloat xpos = x + ch.Bearing.x * scale;
        GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

        GLfloat w = ch.Size.x * scale;
        GLfloat h = ch.Size.y * scale;
        // Update VBO for each character
        GLfloat vertices[6][4] = {
            { xpos,     ypos + h,   0.0, 0.0 },            
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }           
        };
        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); 
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
    }
    
	glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
	glBlendFunc(GL_ONE, GL_ZERO);
}

std::weak_ptr<ce::Material> ce::TextComponent::GetMaterial()
{
	return this->Params.Material;
}

std::weak_ptr<ce::Component> ce::TextComponent::GetComponent()
{
	return this->getPointer();
}

void ce::TextComponent::Process()
{
	if (!timeout) {
		return;
	}

	if (this->timeToLive <= 0) {
		this->Destroy(this->GetComponent());
	}

	this->timeToLive -= ce::Time::GetDeltaTime();
}
