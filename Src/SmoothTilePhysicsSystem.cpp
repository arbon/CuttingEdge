#include "SmoothTilePhysicsSystem.h"
#include "STCollider.h"
#include "STRigidbody.h"
#include "ToString.h"
#include <iostream>

ce::SmoothTilePhysicsSystem::SmoothTilePhysicsSystem(std::shared_ptr<ce::TileSystem> tileSystem) : tileSystem(tileSystem)
{
}

void ce::SmoothTilePhysicsSystem::Initialize()
{
	this->world = std::make_shared<b2World>( b2Vec2(0.0f, this->gravity) );
    this->world->SetContactListener(&this->contactListener);
}

void ce::SmoothTilePhysicsSystem::Step(float dt)
{
	for (auto rigidbody : this->rigidbodies) {
        if (!rigidbody)
            continue;

        auto lockedTransform = rigidbody->GetTransform().lock();
        if (!lockedTransform)
            continue;

        rigidbody->body->SetTransform({lockedTransform->GetWorldPosition().x, lockedTransform->GetWorldPosition().y}, 0.0f);
    }

    this->world->Step(dt, 1.0f, 1.0f);

	float tileWidth = this->tileSystem->GetTileWidth();
	for (auto rigidbody : this->rigidbodies) {
        if (!rigidbody)
            continue;

        auto lockedTransform = rigidbody->GetTransform().lock();
        if (!lockedTransform)
            continue;

        auto box2Position = rigidbody->body->GetPosition();
        glm::vec2 worldPos = glm::vec2(lockedTransform->GetWorldPosition().x, lockedTransform->GetWorldPosition().y);
        
        if (!rigidbody->SmoothTileEnabled) {
            lockedTransform->SetWorldPosition(glm::vec3(box2Position.x, box2Position.y, lockedTransform->GetWorldPosition().z));
        }

        bool collided = false;

        float xDistance = box2Position.x - worldPos.x;
        float yDistance = box2Position.y - worldPos.y;
		float counter = abs(xDistance);
		int dir = xDistance >= 0 ? 1 : -1;
		while (counter > 0.0f) {
			worldPos += glm::vec2(
				counter >= tileWidth
				? dir * tileWidth
				: dir * counter
				, 0.0f);
			counter -= tileWidth;
			if (this->GetCollisionPosition(worldPos, rigidbody->dimensions, glm::vec2(dir, 0.0f), rigidbody->isJumping ? 0.0f : rigidbody->stepHeight)) {
                rigidbody->SetVelocity(glm::vec2(0.0f, rigidbody->GetVelocity().y));
                collided = true;
				break;
			}
		}

		counter = abs(yDistance);
		dir = yDistance >= 0 ? 1 : -1;
		while (counter > 0.0f) {
			worldPos += glm::vec2(
				0.0f,
				counter >= tileWidth
				? dir * tileWidth
				: dir * counter
				);
			counter -= tileWidth;
			if (this->GetCollisionPosition(worldPos, rigidbody->dimensions, glm::vec2(0.0f, dir), 0.0f)) {
                rigidbody->SetVelocity(glm::vec2(rigidbody->GetVelocity().x, 0.0f));
                if (dir < 0) {
                    rigidbody->isJumping = false;
                }

                collided = true;
                break;
            }
            else {
                rigidbody->isJumping = true;
            }
		}

        //std::cout << "\nInPhysics pos: " << ce::ToString(glm::vec3(worldPos.x, worldPos.y, lockedTransform->GetWorldPosition().z));
        lockedTransform->SetWorldPosition(glm::vec3(worldPos.x, worldPos.y, lockedTransform->GetWorldPosition().z));
        rigidbody->body->SetTransform({ worldPos.x, worldPos.y }, rigidbody->body->GetAngle());
        //std::cout << "\nTransform pos: " << ce::ToString(glm::vec3(lockedTransform->GetWorldPosition()));

        if (rigidbody->isCollidingWithWorld != collided) {
            STCollision collision;
            collision.collider = nullptr;
            collision.other = nullptr;
            collision.otherComponent = this->tileSystem->GetComponent();
            collision.normal = glm::normalize(glm::vec2(xDistance, yDistance));

            if (collided) {
                rigidbody->StartCollide(collision);
            }
            else {
                rigidbody->EndCollide(collision);
            }
        }

        rigidbody->isCollidingWithWorld = collided;
	}
}

bool ce::SmoothTilePhysicsSystem::GetCollisionPosition(glm::vec2 &position, glm::vec2 dim, glm::vec2 direction, float stepHeight)
{
	float tileWidth = this->tileSystem->GetTileWidth();
	float halfTileWidth = this->tileSystem->GetTileWidth() * 0.5f;
    glm::vec2 startPos = position - glm::vec2(dim.x * halfTileWidth, dim.y * halfTileWidth);
	for (int i = dim.y; i >= 0; i--) {
		for (int j = 0; j <= dim.x; j++) {
            glm::vec2 startCoord = this->tileSystem->WorldPosToTileCoord(startPos);
			//glm::vec2 checkPos = startPos + glm::vec2(j * tileWidth, i * tileWidth);
            //Tile& tile = this->tileSystem->GetTile(this->tileSystem->WorldPosToTileCoord(checkPos));
            Tile& tile = this->tileSystem->GetOffsetTile(startCoord, glm::vec2(j * tileWidth, i * tileWidth));
            glm::vec2 tilePos = this->tileSystem->TileCoordToWorldPos(tile.GetCoord());
			if (this->tileSystem->IsTileBlocked(tile)) {
				if (tile.IsSloped()) {
					unsigned int angle = tile.GetSlopeAngle();
					if ((angle == 1 && direction.x > 0) || (angle == 2 && direction.x < 0)) {
						if (direction.x != 0 && (startPos.y + stepHeight * tileWidth) >= (tilePos.y - halfTileWidth)) {
							glm::vec2 stepPosition = glm::vec2(position.x, tilePos.y + fmod(position.x, tileWidth) * direction.x - halfTileWidth);
							if (!this->GetCollisionPosition(position, dim, direction, stepHeight - 1.0f)) {
                                position.y = stepPosition.y;
								return false;
							}
						}
					}
				}
				if (direction.x != 0 && (startPos.y + stepHeight * tileWidth) >= (tilePos.y + tileWidth)) {
                    glm::vec2 stepPosition = glm::vec2(position.x, tilePos.y + tileWidth + halfTileWidth);
                    position.y = stepPosition.y;
                    if (!this->GetCollisionPosition(stepPosition, dim, direction, stepHeight - 1.0f)) {
                        position.y = stepPosition.y;
                        return false;
					}
                }

                position = glm::vec2(abs(direction.y) * position.x - direction.x * (tileWidth + 0.01f) + abs(direction.x) * (tilePos.x + halfTileWidth), abs(direction.x) * position.y - direction.y * tileWidth + abs(direction.y) * (tilePos.y + halfTileWidth));
				return true;
			}
		}
	}

	return false;
}

void ce::STContactListener::BeginContact(b2Contact * contact)
{
    b2WorldManifold worldManifold;
    contact->GetWorldManifold(&worldManifold);

    STCollision collision;
    collision.collider = contact->GetFixtureA();
    collision.other = contact->GetFixtureB();
    collision.otherComponent = static_cast<Component*>(contact->GetFixtureB()->GetBody()->GetUserData())->getPointer();
    collision.contactPoints[0] = glm::vec2(worldManifold.points[0].x, worldManifold.points[0].y);
    collision.contactPoints[1] = glm::vec2(worldManifold.points[1].x, worldManifold.points[1].y);
    collision.normal = glm::vec2(worldManifold.normal.x, worldManifold.normal.y);

    void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
    if (bodyUserData)
        static_cast<STRigidbody*>(bodyUserData)->StartCollide(collision);

    collision.collider = contact->GetFixtureB();
    collision.other = contact->GetFixtureA();
    collision.otherComponent = static_cast<Component*>(contact->GetFixtureA()->GetBody()->GetUserData())->getPointer();

    bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
    if (bodyUserData)
        static_cast<STRigidbody*>(bodyUserData)->StartCollide(collision);

}

void ce::STContactListener::EndContact(b2Contact * contact)
{
    b2WorldManifold worldManifold;
    contact->GetWorldManifold(&worldManifold);

    STCollision collision;
    collision.collider = contact->GetFixtureA();
    collision.other = contact->GetFixtureB();
    collision.otherComponent = static_cast<Component*>(contact->GetFixtureB()->GetBody()->GetUserData())->getPointer();
    collision.contactPoints[0] = glm::vec2(worldManifold.points[0].x, worldManifold.points[0].y);
    collision.contactPoints[1] = glm::vec2(worldManifold.points[1].x, worldManifold.points[1].y);
    collision.normal = glm::vec2(worldManifold.normal.x, worldManifold.normal.y);

    //check if fixture A was a ball
    void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
    if (bodyUserData)
        static_cast<STRigidbody*>(bodyUserData)->EndCollide(collision);

    collision.collider = contact->GetFixtureB();
    collision.other = contact->GetFixtureA();
    collision.otherComponent = static_cast<Component*>(contact->GetFixtureA()->GetBody()->GetUserData())->getPointer();

    //check if fixture B was a ball
    bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
    if (bodyUserData)
        static_cast<STRigidbody*>(bodyUserData)->EndCollide(collision);
}
