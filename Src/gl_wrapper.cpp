#include "gl_wrapper.h"

void GL::bindVertexArray(int vao){
    glBindVertexArray(vao);
}

void GL::activeTexture(int index) {
    glActiveTexture(index);
}