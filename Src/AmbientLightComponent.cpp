#include "AmbientLightComponent.h"

ce::AmbientLightComponent::AmbientLightComponent(std::weak_ptr<Transform> transform, float intensity, std::string name) : light(std::make_shared<DeferredAmbientLight>()), ce::Component(transform, name)
{
    this->light->GetMaterial().lock()->SetFloat(this->light->INTESITY_UNIFORM_NAME, intensity);
}

void ce::AmbientLightComponent::Initialize()
{
    this->light->_SetComponent(this->getPointer());
}

std::weak_ptr<ce::Drawable> ce::AmbientLightComponent::GetDrawable()
{
    return this->light;
}

std::weak_ptr<ce::Processable> ce::AmbientLightComponent::GetProcessable()
{
    return std::weak_ptr<ce::Processable>();
}
