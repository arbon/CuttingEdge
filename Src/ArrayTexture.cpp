#include "ArrayTexture.h"

ce::ArrayTexture::ArrayTexture() : Index(0), Width(0), Height(0)
{
}

void ce::ArrayTexture::AddTexture(std::shared_ptr<ce::Texture> texture)
{
    if (!texture) {
        return;
    }

    if (!this->Textures.size()) {
        auto dims = texture->GetDimensions();

        this->Width = dims.x;
        this->Height = dims.y;
    }

    this->Textures.push_back(texture);
}
