#include "RenderLayer.h"

void ce::RenderLayer::Initialize(std::vector<std::shared_ptr<ce::RenderLayer::TexBuffer>> inputBuffers)
{
	this->inputBuffers = inputBuffers;

	if(this->outputBuffers.size() < 1)
	{
		return;
	}

	glGenFramebuffers(1, &this->frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, this->frameBuffer);

    this->drawBuffers = std::vector<GLenum>(this->outputBuffers.size());

	for (unsigned int i = 0; i < this->outputBuffers.size(); i++)
	{
        auto buffer = this->outputBuffers[i];

		glGenTextures(1, &buffer->Buffer);
		glBindTexture(GL_TEXTURE_2D, buffer->Buffer);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,  GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,  GL_CLAMP_TO_EDGE);

		glTexImage2D(buffer->Target, buffer->Level, buffer->InternalFormat, buffer->Width, buffer->Height, buffer->Border, buffer->Format, buffer->DataType, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, buffer->Buffer, buffer->Level);

        this->drawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;

        buffer->FBO = this->frameBuffer;
	}

    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (Status != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "FB error, status: " << Status << "\n";
    }

	//glGenRenderbuffers(1, &this->stencilBuffer);
	//glBindRenderbuffer(GL_RENDERBUFFER, this->stencilBuffer);
	//glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, this->outputBuffers[0]->Width, this->outputBuffers[0]->Height);
	//glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, this->stencilBuffer);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

std::vector<std::shared_ptr<ce::RenderLayer::TexBuffer>> ce::RenderLayer::GetRequiredInputs()
{
	return this->inputBufferReqs;
}

std::vector<std::shared_ptr<ce::RenderLayer::TexBuffer>> ce::RenderLayer::GetOutputBuffers()
{
	return this->outputBuffers;
}

void ce::RenderLayer::AddDrawable(std::weak_ptr<ce::Drawable> drawable)
{
    drawables.push_back(drawable);
}
