#include "Transform.h"
#include "Component.h"
#include "Camera.h"

const glm::vec3 ce::Transform::GlobalForward = glm::vec3(0.0f, 0.0f, -1.0f);
const glm::vec3 ce::Transform::GlobalUp = glm::vec3(0.0f, 1.0f, 0.0f);
const glm::vec3 ce::Transform::GlobalRight = glm::vec3(1.0f, 0.0f, 0.0f);

void * ce::Transform::operator new(size_t size)
{
	return ce::XAlloc::aligned_malloc(size, ce::Transform::ALLOC);
}

void ce::Transform::operator delete(void * p)
{
    ce::XAlloc::aligned_free(p);
}

ce::Transform::Transform(std::weak_ptr<ce::Scene> scene, std::weak_ptr<ce::Transform> parent, std::string name)
	: cacheInverseMatrix(true)
{
	this->parent = parent;

	this->scene = scene;
	this->name = name;
	this->localPosition = glm::vec3();
	this->localRotation = glm::quat();
	this->localScale = glm::vec3(1.0f, 1.0f, 1.0f);
	this->dirtyFlags = dirty_All;
}

ce::Transform::Transform(std::weak_ptr<ce::Scene> scene, glm::vec3 position, glm::quat rotation, glm::vec3 scale, std::weak_ptr<ce::Transform> parent, std::string name)
	: cacheInverseMatrix(true)
{
	this->parent = parent;

	this->scene = scene;
	this->name = name;
	this->localPosition = position;
	this->localRotation = rotation;
	this->localScale = scale;
	this->dirtyFlags = dirty_All;
}

glm::mat4 ce::Transform::CalculateMatrixFromComponents(glm::vec3 position, glm::quat rotation, glm::vec3 scale)
{
    glm::mat4 translateMat = glm::translate(glm::mat4(1.0f), position);
    glm::mat4 rotateMat = glm::mat4_cast(rotation);
    glm::mat4 scaleMat = glm::scale(glm::mat4(1.0f), scale);

    return translateMat * rotateMat * scaleMat;
}

std::weak_ptr<ce::Scene> ce::Transform::GetScene() const
{
	return this->scene;
}

std::weak_ptr<ce::Transform> ce::Transform::GetParent() const
{
	return this->parent;
}

void ce::Transform::SetParent(std::weak_ptr<ce::Transform> parent, bool preserveWorld)
{
    auto parentLocked = parent.lock();

	if (preserveWorld) {
		if (parentLocked) {
            this->localPosition = this->GetWorldPosition() - parentLocked->GetWorldPosition();
            this->localRotation = this->GetWorldRotation() * glm::inverse(parentLocked->GetWorldRotation());
            this->localScale = this->GetWorldScale() / parentLocked->GetWorldScale();
		}
		else {
            this->localPosition = this->GetWorldPosition();
            this->localRotation = this->GetWorldRotation();
            this->localScale = this->GetWorldScale();
		}
	}
	else {
		this->dirtyFlags = dirty_All;
        this->UpdateChildrenDirty();
	}

	if (auto validParent = this->parent.lock()) {
		validParent->removeChildRef(this->getPointer());
	}

	this->parent = parent;

	parentLocked->_AddChildRef(this->getPointer());
}

std::vector<std::shared_ptr<ce::Transform>>& ce::Transform::GetChildren()
{
	return this->children;
}

glm::vec3 ce::Transform::GetLocalPosition() const
{
	return this->localPosition;
}

void ce::Transform::SetLocalPosition(glm::vec3 position)
{
	this->localPosition = position;
	this->dirtyFlags |= dirty_Position | dirty_Matrix;
    this->UpdateChildrenDirty();
}

glm::vec3 ce::Transform::GetWorldPosition()
{
	if (!(this->dirtyFlags & dirty_Position)) {
		return this->worldPosition;
	}

    auto parentLocked = this->parent.lock();

	if (!parentLocked) {
		this->worldPosition = this->GetLocalPosition();
	}
	else {
		this->worldPosition = parentLocked->GetWorldPosition() + this->GetLocalPosition();
	}

	this->dirtyFlags &= ~dirty_Position;

	return this->worldPosition;
}

void ce::Transform::SetWorldPosition(glm::vec3 position)
{
    auto parentLocked = this->parent.lock();

	if (!parentLocked) {
		this->localPosition = position;
	}
	else {
		this->localPosition = position - parentLocked->GetWorldPosition();
	}

	this->worldPosition = position;
    this->dirtyFlags &= ~dirty_Position;
    this->dirtyFlags |= dirty_Matrix;

    this->UpdateChildrenDirty();
}

glm::quat ce::Transform::GetLocalRotation() const
{
	return this->localRotation;
}

void ce::Transform::SetLocalRotation(glm::quat rotation)
{
	this->localRotation = rotation;
	this->dirtyFlags |= dirty_Rotation | dirty_Matrix;
    this->UpdateChildrenDirty();
}

glm::quat ce::Transform::GetWorldRotation()
{
	if (!(this->dirtyFlags & dirty_Rotation)) {
		return this->worldRotation;
	}

    auto parentLocked = this->parent.lock();

	if (!parentLocked) {
		this->worldRotation = this->GetLocalRotation();
	}
	else {
		this->worldRotation = parentLocked->GetWorldRotation() * this->GetLocalRotation();
	}

	this->dirtyFlags &= ~dirty_Rotation;

	return this->worldRotation;
}

void ce::Transform::SetWorldRotation(glm::quat rotation)
{
    auto parentLocked = this->parent.lock();

	if (!parentLocked) {
		this->localRotation = rotation;
	}
	else {
		this->localRotation = rotation * inverse(parentLocked->GetWorldRotation());
	}

	this->worldRotation = rotation;
	this->dirtyFlags &= ~dirty_Rotation;
    this->UpdateChildrenDirty();
}

glm::vec3 ce::Transform::GetLocalScale() const
{
	return this->localScale;
}

void ce::Transform::SetLocalScale(glm::vec3 scale)
{
	this->localScale = scale;
	this->dirtyFlags |= dirty_Scale | dirty_Matrix;
    this->UpdateChildrenDirty();
}

glm::vec3 ce::Transform::GetWorldScale()
{
	if (!(this->dirtyFlags & dirty_Scale)) {
		return this->worldScale;
	}

    auto parentLocked = this->parent.lock();

	if (!parentLocked) {
		this->worldScale = this->GetLocalScale();
	}
	else {
		this->worldScale = parentLocked->GetWorldScale() * this->GetLocalScale();
	}

	this->dirtyFlags &= ~dirty_Scale;

	return this->worldScale;
}

void ce::Transform::SetWorldScale(glm::vec3 scale)
{
    auto parentLocked = this->parent.lock();

	if (!parentLocked) {
		this->localScale = scale;
	}
	else {
		glm::vec3 inverseWorldScale = parentLocked->GetWorldScale();
		inverseWorldScale = glm::vec3(1.0f / inverseWorldScale.x, 1.0f / inverseWorldScale.y, 1.0f / inverseWorldScale.z);
		this->localScale = scale * inverseWorldScale;
	}

	this->worldScale = scale;
	this->dirtyFlags &= ~dirty_Scale;
    this->UpdateChildrenDirty();
}

glm::vec3 ce::Transform::GetForward()
{
	glm::vec3 forward = GlobalForward * GetWorldRotation();

	return forward;
}

glm::mat4x4 ce::Transform::GetWorldMatrix()
{
	if (!(this->dirtyFlags & dirty_Matrix)) {
		return this->worldMatrix;
	}

	this->CalcWorldMatrix();

	this->dirtyFlags &= ~dirty_Matrix;

	return this->worldMatrix;
}

glm::mat4x4 ce::Transform::GetInverseWorldMatrix()
{
	if (this->dirtyFlags & dirty_Matrix) {
		this->CalcWorldMatrix();
		this->dirtyFlags &= ~dirty_Matrix;
	}

	if(this->cacheInverseMatrix)
	{
		return this->inverseWorldMatrix;
	}

	return glm::inverse(this->worldMatrix);
}

void ce::Transform::SetCacheInverseMatrix(bool doCache)
{
	this->cacheInverseMatrix = doCache;
}

void ce::Transform::CalcWorldMatrix()
{
	this->worldMatrix = CalculateMatrixFromComponents(this->GetWorldPosition(), this->GetWorldRotation(), this->GetWorldScale());

	if(this->cacheInverseMatrix)
	{
		this->inverseWorldMatrix = glm::inverse(this->worldMatrix);
	}
}

void ce::Transform::UpdateChildrenDirty()
{
    for (auto child : this->children) {
        child->dirtyFlags |= dirty_All;
        child->UpdateChildrenDirty();
    }
}

glm::mat4 ce::Transform::CalcMVPMatrix(const glm::mat4& viewProj)
{
	return viewProj * this->GetWorldMatrix();
}

void ce::Transform::_AddComponent(std::shared_ptr<ce::Component> component)
{
	this->components.push_back(component);
}

void ce::Transform::_RemoveComponent(std::shared_ptr<ce::Component> component)
{
	this->components.erase(std::find(this->components.begin(), this->components.end(), component));
}

std::vector<std::shared_ptr<ce::Component>> ce::Transform::FindAllComponents(int depth)
{
	std::vector<std::shared_ptr<ce::Component>> components = std::vector<std::shared_ptr<ce::Component>>();

	for (std::shared_ptr<ce::Component> component : this->components) {
		components.push_back(component);
	}

	if (depth) {
		for (std::shared_ptr<ce::Transform> transform : this->children) {
			std::vector<std::shared_ptr<ce::Component>> childComponents = transform->FindAllComponents(depth - 1);
			components.insert(components.end(), childComponents.begin(), childComponents.end());
		}
	}

	return components;
}

template<typename C>
std::vector<std::shared_ptr<C>> ce::Transform::FindComponents(int depth)
{
	std::vector<std::shared_ptr<C>> components = std::vector<std::shared_ptr<C>>();

	for (std::shared_ptr<ce::Component> component : this->components) {
        std::shared_ptr<C> castComponent = dynamic_cast<std::shared_ptr<C>>(component);

		if (castComponent != nullptr) {
			components.push_back(castComponent);
		}
	}

	if (depth) {
		for (std::shared_ptr<ce::Transform> transform : this->children) {
			std::vector<std::shared_ptr<C>> childComponents = transform->FindComponents<std::shared_ptr<C>>(depth - 1);
			components.insert(components.end(), childComponents.begin(), childComponents.end());
		}
	}

	return components;
}

void ce::Transform::removeChildRef(std::shared_ptr<ce::Transform> child)
{
	this->children.erase(std::find(this->children.begin(), this->children.end(), child));
}

void ce::Transform::_AddChildRef(std::shared_ptr<ce::Transform> child)
{
	this->children.push_back(child);
}
