#include "PlatformUtils.h"
#include "World.h"

// #ifdef _WIN32
// #include <windows.h>    //GetModuleFileNameW
// #endif

std::filesystem::path ce::PlatformUtils::GetBaseDirectory() const
{
    return std::filesystem::current_path();

    //auto path = std::filesystem::current_path();
    // auto resources_path = path.append("resources");
    // auto runfiles_path = path.append("advance.runfiles").append("_main");
    // if(!std::filesystem::exists(resources_path) && std::filesystem::exists(runfiles_path)) {
    //     std::filesystem::current_path(runfiles_path);
    // }

// #ifdef _WIN32
//     wchar_t path[MAX_PATH] = { 0 };
//     GetModuleFileNameW(NULL, path, MAX_PATH);
//     return std::filesystem::canonical(path).remove_filename();
// #else
//     return std::filesystem::canonical("/proc/self/exe").remove_filename();
// #endif
}

std::filesystem::path ce::PlatformUtils::GetResourceDirectory() const
{
    return GetBaseDirectory() / "resources";
}

std::filesystem::path ce::PlatformUtils::ResolvePath_Internal(std::string path) const
{
    #ifdef BAZEL_RUNFILES_ENABLED
        if(this->runfiles != nullptr){
            std::string runfiles_path = this->runfiles->Rlocation(path);

            #ifdef DEBUG_BAZEL_RUNFILES
                std::cout << "Original path: " << path << " Resolved path: " << runfiles_path << std::endl;
            #endif

            if(!runfiles_path.empty()) {
                return std::filesystem::canonical(runfiles_path);
            }
        }
    #endif

    return std::filesystem::canonical(GetBaseDirectory() / path);
}

std::filesystem::path ce::ResolvePath(std::string path)
{
    auto& platform_utils = ce::World::GetWorld()->GetPlatformUtils();
    return platform_utils.ResolvePath_Internal(path);
}
