#include "ResourceManager.h"
#include <fstream>
#include <iostream>

#include "RHI.h"
#include "World.h"
#include "PlatformUtils.h"

unsigned int const ce::ResourceManager::GEOM_PLANE = 0;
unsigned int const ce::ResourceManager::GEOM_SCREEN_QUAD = 1;

std::vector<std::string> const ce::ResourceManager::GEOM_NAMES = std::vector<std::string>{
	"Plane",
    "Screen_Quad"
};

std::vector<std::pair<std::vector<ce::Vertex>, std::vector<GLuint>>> const ce::ResourceManager::GEOMS = std::vector<std::pair<std::vector<ce::Vertex>, std::vector<GLuint>>>{
	std::pair<std::vector<ce::Vertex>, std::vector<GLuint>>(
		std::vector<ce::Vertex> {
			ce::Vertex(glm::vec3(-0.5f, 0.5f, 0.0f), glm::vec3(), glm::vec3(), glm::vec2(0.0f, 0.0f)),
			ce::Vertex(glm::vec3(0.5f, 0.5f, 0.0f), glm::vec3(), glm::vec3(), glm::vec2(1.0f, 0.0f)),
			ce::Vertex(glm::vec3(0.5f, -0.5f, 0.0f), glm::vec3(), glm::vec3(), glm::vec2(1.0f, 1.0f)),
			ce::Vertex(glm::vec3(-0.5f, -0.5f, 0.0f), glm::vec3(), glm::vec3(), glm::vec2(0.0f, 1.0f))
		},
		std::vector<GLuint>{
			3,1,0,
			3,2,1
		}
	),
    std::pair<std::vector<ce::Vertex>, std::vector<GLuint>>(
        std::vector<ce::Vertex> {
        ce::Vertex(glm::vec3(-1.0f, 1.0f, 0.0f), glm::vec3(), glm::vec3(), glm::vec2(0.0f, 1.0f)),
            ce::Vertex(glm::vec3(1.0f, 1.0f, 0.0f), glm::vec3(), glm::vec3(), glm::vec2(1.0f, 1.0f)),
            ce::Vertex(glm::vec3(1.0f, -1.0f, 0.0f), glm::vec3(), glm::vec3(), glm::vec2(1.0f, 0.0f)),
            ce::Vertex(glm::vec3(-1.0f, -1.0f, 0.0f), glm::vec3(), glm::vec3(), glm::vec2(0.0f, 0.0f))
    },
        std::vector<GLuint>{
            3,1,0,
            3,2,1
        }
    )
};

std::unique_ptr<Assimp::Importer> ce::ResourceManager::importer = std::make_unique<Assimp::Importer>();

std::unordered_map<std::string, std::vector<std::shared_ptr<ce::Mesh::MeshData>>> ce::ResourceManager::meshMap = std::unordered_map<std::string, std::vector<std::shared_ptr<ce::Mesh::MeshData>>>();

std::unordered_map<std::string, std::shared_ptr<ce::Texture::TextureData>> ce::ResourceManager::textureMap = std::unordered_map<std::string, std::shared_ptr<ce::Texture::TextureData>>();

std::unordered_map<std::string, std::shared_ptr<ce::ShaderProgram::Shader>> ce::ResourceManager::shaderMap = std::unordered_map<std::string, std::shared_ptr<ce::ShaderProgram::Shader>>();

std::unordered_map<std::string, std::shared_ptr<ce::ShaderProgram>> ce::ResourceManager::shaderProgramMap = std::unordered_map<std::string, std::shared_ptr<ce::ShaderProgram>>();

std::shared_ptr<ce::RHI> GetRHI()
{
	return ce::World::GetWorld()->GetRHI().lock();
}

std::vector<std::shared_ptr<ce::Mesh>> ce::ResourceManager::LoadMesh(std::string path, unsigned importFlags)
{
    std::vector<std::shared_ptr<ce::Mesh>> meshes = std::vector<std::shared_ptr<ce::Mesh>>();

    std::vector<std::shared_ptr<ce::Mesh::MeshData>> meshData = LoadMeshData(path, importFlags);

	for (auto meshSection : meshData) {
		meshes.push_back(std::make_shared<ce::Mesh>(meshSection));
	}

	return meshes;
}

std::shared_ptr<ce::Mesh::MeshData> ce::ResourceManager::LoadPrimitiveGeometry(unsigned int GEOMETRY)
{
    std::vector<std::shared_ptr<ce::Mesh::MeshData>> meshData = meshMap[GEOM_NAMES[GEOMETRY]];

	if (meshData.empty()) {
        std::shared_ptr<ce::Mesh::MeshData> mesh = std::make_shared<ce::Mesh::MeshData>();
		meshData = std::vector<std::shared_ptr<ce::Mesh::MeshData>>();
		std::pair<std::vector<ce::Vertex>, std::vector<GLuint>> primitive = GEOMS[GEOMETRY];

		mesh->verticesCount = primitive.first.size();

		mesh->elementsCount = primitive.second.size();

		mesh->elements = primitive.second;

		mesh->vertices = primitive.first;

		mesh->name = GEOM_NAMES[GEOMETRY];

		mesh->path = GEOM_NAMES[GEOMETRY];

		meshData.push_back(mesh);

		meshMap[GEOM_NAMES[GEOMETRY]] = meshData;
	}

	return meshData[0];
}

std::vector<std::shared_ptr<ce::Mesh::MeshData>> ce::ResourceManager::LoadMeshData(std::string path, unsigned int importFlags)
{
    std::vector<std::shared_ptr<Mesh::MeshData>> meshData = meshMap[path];

	if (meshData.empty()) {
		const struct aiScene* modelData = importer->ReadFile(path, importFlags);

		if (modelData == nullptr) {
            meshData = std::vector<std::shared_ptr<Mesh::MeshData>>();
            return meshData;
		}

        std::vector<std::shared_ptr<ce::Mesh::MeshData>> meshes = std::vector<std::shared_ptr<ce::Mesh::MeshData>>();

		for (unsigned int i = 0; i < modelData->mNumMeshes; i++)
		{
			aiMesh* mesh = modelData->mMeshes[i];

			meshes.push_back(std::make_shared<ce::Mesh::MeshData>());

			meshes[i]->verticesCount = mesh->mNumVertices;

			meshes[i]->elementsCount = mesh->mNumFaces * 3;

			meshes[i]->elements = CreateFlatElementArray(mesh->mFaces, mesh->mNumFaces);

			meshes[i]->vertices = CreateFlatVertexArray(*mesh);

			meshes[i]->name = std::string(mesh->mName.C_Str());

			meshes[i]->path = path;
		}

		meshMap[path] = meshes;
	}

	return meshMap[path];
}

bool ce::ResourceManager::DeleteMeshData(std::string name)
{
    return meshMap.erase(name);
}

bool ce::ResourceManager::AddMeshData(std::shared_ptr<ce::Mesh::MeshData> meshData)
{
	if(meshMap[meshData->name].empty() && meshData->vertices.size())
	{
		meshMap[meshData->name].push_back(meshData);
		return true;
	}

	return false;
}

std::vector<std::shared_ptr<ce::Mesh::MeshData>> ce::ResourceManager::GetMeshData(std::string path)
{
	return meshMap[path];
}

bool ce::ResourceManager::BufferMesh(std::shared_ptr<ce::Mesh::MeshData>& meshData) {

	if (meshData->vao != 0 || meshData->vbo != 0 || meshData->ebo != 0) {
		return false;
	}

	// Create Vertex Array Object
	glGenVertexArrays(1, &(meshData->vao));
	glBindVertexArray(meshData->vao);

	// Create a Vertex Buffer Object and copy the vertex data to it
	glGenBuffers(1, &(meshData->vbo));

	glBindBuffer(GL_ARRAY_BUFFER, meshData->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(ce::Vertex) * meshData->vertices.size(), (GLfloat*)meshData->vertices.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(ce::Vertex), (const GLvoid*)offsetof(ce::Vertex, position));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(ce::Vertex), (const GLvoid*)offsetof(ce::Vertex, normal));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(ce::Vertex), (const GLvoid*)offsetof(ce::Vertex, color));

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(ce::Vertex), (const GLvoid*)offsetof(ce::Vertex, uv));

	// Create an element array
	glGenBuffers(1, &(meshData->ebo));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshData->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * meshData->elements.size(), meshData->elements.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Data buffered to GPU so remove it from memory
	meshData->elements.clear();
	meshData->vertices.clear();

	return true;
}

bool ce::ResourceManager::CompileShaderProgram(std::shared_ptr<ce::ShaderProgram>& program)
{
	if(shaderProgramMap[program->name])
	{
		return false;
	}

	RHI_Error result = GetRHI()->CompileShaderProgram(*program);
	if(result != RHI_Error::OK)
	{
		std::cerr << "Failed to compile shader program: " << program->name << std::endl;
		return false;
	}

	shaderProgramMap[program->name] = program;

	return true;
}

std::shared_ptr<ce::ShaderProgram> ce::ResourceManager::GetShaderProgram(std::string name)
{
	return shaderProgramMap[name];
}

std::shared_ptr<ce::Texture> ce::ResourceManager::LoadTexture(std::string path)
{
    std::shared_ptr<ce::Texture::TextureData> texData = textureMap[path];
	std::string resolvedPath = ce::ResolvePath(path).string();

	if(!texData)
	{
		texData = std::make_shared<ce::Texture::TextureData>();
		texData->data = stbi_load(resolvedPath.c_str(), &texData->width, &texData->height, &texData->components, 0);

		if(texData->data == nullptr)
		{
			std::cout << "LoadTexture failure, reason: " << stbi_failure_reason() << ", path: " << path << std::endl;
			return nullptr;
		}

		texData->path = path;

		std::string name = path;

		int sIndex = path.find_last_of('/');
		int eIndex = path.find_last_of('.');

		texData->name = path.substr(sIndex, eIndex - sIndex);

		textureMap[path] = texData;
	}

	return std::make_shared<ce::Texture>(texData);
}

std::shared_ptr<ce::Texture> ce::ResourceManager::LoadTexture(std::shared_ptr<Texture::TextureData> texData)
{
    textureMap[texData->name] = texData;
    return std::make_shared<ce::Texture>(texData);
}

bool ce::ResourceManager::DeleteTexture(std::string name)
{
    return textureMap.erase(name);
}

bool ce::ResourceManager::BufferTexture(std::shared_ptr<ce::Texture::TextureData>& texData)
{
	if(texData->index != 0)
	{
		return false;
	}

	glGenTextures(1, &texData->index);

	glBindTexture(GL_TEXTURE_2D, texData->index);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	if (texData->components == 3)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texData->width, texData->height, 0, GL_RGB, GL_UNSIGNED_BYTE, texData->data);
	} else if (texData->components == 4)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texData->width, texData->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texData->data);
	}

	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

bool ce::ResourceManager::BufferArrayTexture(std::shared_ptr<ArrayTexture>& arrayTexture)
{
    if (arrayTexture->Index != 0)
    {
        return false;
    }

    glGenTextures(1, &arrayTexture->Index);

    glBindTexture(GL_TEXTURE_2D_ARRAY, arrayTexture->Index);

    GLsizei width = arrayTexture->Width;
    GLsizei height = arrayTexture->Height;
    GLsizei layerCount = arrayTexture->Textures.size();
    GLsizei mipLevelCount = 1;

    glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipLevelCount, GL_RGBA8, width, height, layerCount);

    for (int i = 0; i < layerCount; i++) {
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, arrayTexture->Width, arrayTexture->Height, 1, GL_RGBA, GL_UNSIGNED_BYTE, arrayTexture->Textures[i]->data->data);
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);

    return true;
}

std::shared_ptr<ce::ShaderProgram::Shader> ce::ResourceManager::LoadShader(std::filesystem::path path)
{
    std::shared_ptr<ce::ShaderProgram::Shader> shader = shaderMap[path.string()];
	std::filesystem::path resolvedPath = ce::ResolvePath(path.string());

	if (!shader)
	{
		shader = std::make_shared<ce::ShaderProgram::Shader>();

		shader->path = resolvedPath;

		std::string extension = resolvedPath.extension().string();

		if (extension == ShaderProgram::VERT_EXTENSION)
		{
			shader->type = ShaderProgram::ShaderType::VERTEX_SHADER;
		}
		else if (extension == ShaderProgram::FRAG_EXTENSION)
		{
			shader->type = ShaderProgram::ShaderType::FRAGMENT_SHADER;
		}
		else if (extension == ShaderProgram::GEOM_EXTENSION)
		{
			shader->type = ShaderProgram::ShaderType::GEOMETRY_SHADER;
		}

		RHI_Error result = GetRHI()->LoadShader(*shader);
		if(result != RHI_Error::OK)
		{
			std::cerr << "Failed to load shader: " << path << std::endl;
			return nullptr;
		}

		shaderMap[path.string()] = shader;
	}

	return shader;
}

std::vector<ce::Vertex> ce::ResourceManager::CreateFlatVertexArray(aiMesh& mesh) {
	std::vector<ce::Vertex> flatVertices = std::vector<ce::Vertex>(mesh.mNumVertices);

	for (unsigned int i = 0; i < mesh.mNumVertices; i++) {
		aiVector3D pos = mesh.mVertices[i];

		flatVertices[i].position = glm::vec3(pos.x, pos.y, pos.z);

		if (mesh.HasNormals())
		{
			aiVector3D normal = mesh.mNormals[i];
			flatVertices[i].normal = glm::vec3(normal.x, normal.y, normal.z);
		}
		if (mesh.HasVertexColors(0))
		{
			aiColor4D color = mesh.mColors[0][i];
			flatVertices[i].color = glm::vec4(color.r, color.g, color.b, 1.0f);
		}
		if(mesh.HasTextureCoords(0))
		{
			auto texCoords = mesh.mTextureCoords[0][i];
			flatVertices[i].uv = glm::vec2(texCoords.x, 1.0f - texCoords.y);
		}
	}

	return flatVertices;
}

std::vector<GLuint> ce::ResourceManager::CreateFlatElementArray(aiFace* elements, unsigned int numElems) {
	std::vector<GLuint> flatElements = std::vector<GLuint>(numElems * 3);

	for (unsigned int i = 0; i < numElems; i++) {

		int index = i * 3;

		flatElements[index] = elements[i].mIndices[0];
		flatElements[index + 1] = elements[i].mIndices[1];
		flatElements[index + 2] = elements[i].mIndices[2];
	}

	return flatElements;
}
