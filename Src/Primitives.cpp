#include "Primitives.h"

const std::string ce::Primitives::QUAD_NAME = "quad";
const std::vector<ce::Vertex> ce::Primitives::QUAD_VERTS =
{
	{glm::vec3(-0.5f, 0.0f, -0.5f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec2(0.0f, 1.0f)},
	{glm::vec3(-0.5f, 0.0f, 0.5f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(0.0f, 0.0f)},
	{glm::vec3(0.5f, 0.0f, 0.5f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(1.0f, 0.0f)},
	{glm::vec3(0.5f, 0.0f, -0.5f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(1.0f, 1.0f)}
};

const std::vector<unsigned int> ce::Primitives::QUAD_ELEMENTS = {3, 1, 0, 2, 1, 3};

std::shared_ptr<ce::Mesh> ce::Primitives::CreateQuad()
{
	auto pairQuadData = ce::ResourceManager::GetMeshData(QUAD_NAME);

    std::shared_ptr<ce::Mesh::MeshData> quadData = std::shared_ptr<ce::Mesh::MeshData>();

	if(pairQuadData.empty())
	{
		quadData->name = QUAD_NAME;

		quadData->elements = QUAD_ELEMENTS;

		quadData->vertices = QUAD_VERTS;

		quadData->elementsCount = 6;
		quadData->verticesCount = 4;
	} else
	{
		quadData = pairQuadData[0];
	}

	return std::make_shared<ce::Mesh>(quadData);
}