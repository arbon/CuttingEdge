Requisites  
Windows: Visual Studio (tested with ver 2015), git, Cmake (min 3.8)  
Linux: Cmake (min 3.8), git, make, gcc  

1. Clone the project
2. Create a subdirectory under the project root in which to build the binaries
3. In terminal in created subdirectory, run command "cmake ../"

Windows
1. Open CuttingEdge.sln and build CuttingEdge project (not solution)

Linux
1. In terminal, run command "make"