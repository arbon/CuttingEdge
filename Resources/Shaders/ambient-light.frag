#version 330
uniform sampler2D diffuse;
uniform sampler2D surfacenormal;
uniform float intensity;
in vec3 onormal;
in vec3 ocolor;
in vec2 texcoord;
out vec4 outColor;
void main() {
   vec4 surface = texture(surfacenormal, texcoord);
   outColor = vec4(0,0,0,surface.a) + texture(diffuse, texcoord) * intensity;
}