#version 330
uniform sampler2D mainTex;
uniform float noTex;
in vec3 onormal;
in vec4 ocolor;
in vec2 texcoord;
out vec4 outColor;
void main() {
   vec4 color = texture(mainTex, texcoord);
   outColor = (vec4(noTex, noTex, noTex, noTex) + color) * ocolor;
}