#version 150 core
const vec2 tcMin = vec2(0,0);
const vec2 tcMax = vec2(1,1);

uniform sampler2D mainTex;
uniform vec4 spritecoords;
in vec3 onormal;
in vec3 ocolor;
in vec2 texcoord;
out vec4 outColor;

void main() {
vec2 uv = spritecoords.xy + (spritecoords.zw - spritecoords.xy) * texcoord;
outColor = texture(mainTex, uv);
}
