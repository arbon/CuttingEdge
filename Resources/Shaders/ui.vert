#version 330
uniform mat4 translation;
in vec3 position;
in vec3 normal;
in vec4 color;
in vec2 uv;
out vec3 onormal;
out vec4 ocolor;
out vec2 texcoord;
void main() {
   gl_Position = translation * vec4(position.xyz, 1.0);
   onormal = normal;
   ocolor = color;
   texcoord = uv;
}