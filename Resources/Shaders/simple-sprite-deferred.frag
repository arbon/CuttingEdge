#version 330
#extension GL_ARB_separate_shader_objects : enable
const vec2 tcMin = vec2(0,0);
const vec2 tcMax = vec2(1,1);

uniform sampler2D mainTex;
uniform vec4 spritecoords;
in vec3 onormal;
in vec3 ocolor;
in vec2 texcoord;
layout (location = 0) out vec4 diffuse; 
layout (location = 1) out vec4 surfacenormal; 

void main() {
    vec2 uv = spritecoords.xy + (spritecoords.zw - spritecoords.xy) * texcoord;
    diffuse = texture(mainTex, uv);
    surfacenormal = vec4(0.0, 0.0, 1.0, 0.0);
}
