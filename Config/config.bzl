
def config_select(
    windows = None,
    windows_debug = None,
    windows_release = None,
    linux = None,
    linux_debug = None,
    linux_release = None,
    debug = None,
    release = None,
    default = None,
    os_default = None,
    config_default = None,
):
    module_name = native.module_name()
    label_prefix = "" if module_name == "cutting_edge" else ("@" + module_name)

    if os_default == None:
        os_default = default

    if config_default == None:
        config_default = default

    config_select_contents = {} if debug == None else {
        label_prefix + "//Config:cfg_dbg": debug
    }
    config_select_contents |= {} if release == None else {
        label_prefix + "//Config:cfg_release": release
    }
    config_select_contents |= {} if config_default == None else {
        "//conditions:default": config_default
    }

    os_select_contents = {} if windows == None else {
        "@bazel_tools//src/conditions:windows": windows
    }
    os_select_contents |= {} if linux == None else {
        "@bazel_tools//src/conditions:linux": linux
    }
    os_select_contents |= {} if os_default == None else {
        "//conditions:default": os_default
    }

    select_contents = {} if windows_debug == None else {
        label_prefix + "//Config:cfg_win64_debug" : windows_debug
    }
    select_contents |= {} if windows_release == None else {
        label_prefix + "//Config:cfg_win64_release" : windows_release
    }

    select_contents |= {} if linux_debug == None else {
        label_prefix + "//Config:cfg_linux_debug" : linux_debug
    }
    select_contents |= {} if linux_release == None else {
        label_prefix + "//Config:cfg_linux_release" : linux_release
    }
    select_contents |= {} if default == None else {
        "//conditions:default": default
    }

    combined_selects = None
    if config_select_contents != {}:
        combined_selects = select(config_select_contents)
    if os_select_contents != {}:
        os_select = select(os_select_contents)
        combined_selects = os_select if combined_selects == None else combined_selects + os_select
    if select_contents != {}:
        inner_select = select(select_contents)
        combined_selects = inner_select if combined_selects == None else combined_selects + inner_select

    return combined_selects
        
