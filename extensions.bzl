load("//Libs:vulkan.bzl", "vulkan_library")
load("//Libs:glew.bzl", "glew_library")

def _vulkan_library_ext_impl(_ctx):
    vulkan_library(
        name = "vulkan_library",
    )

vulkan_library_ext = module_extension(
    implementation = _vulkan_library_ext_impl,
)

def _glew_library_ext_impl(_ctx):
    glew_library()

glew_library_ext = module_extension(
    implementation = _glew_library_ext_impl,
)