#pragma once
#include <string>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

namespace ce {
    std::string ToString(glm::vec2 vec, const int precision = 6);
    std::string ToString(glm::vec3 vec, const int precision = 6);
    std::string ToString(glm::vec4 vec, const int precision = 6);
    std::string ToString(glm::mat4 vec, const int precision = 6);
}