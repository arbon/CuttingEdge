#pragma once
#include <memory>
#include "RenderLayer.h"
#include "Component.h"

namespace ce {
    class ForwardTransparentLayer : public RenderLayer
    {
        void Render(Camera& camera) override;
    };
}
