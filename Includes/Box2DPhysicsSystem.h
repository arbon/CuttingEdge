#pragma once
#include <memory>
#include "PhysicsSystem.h"
#include "Box2D/Box2D.h"

namespace ce
{
    class Box2DPhysicsSystem : public PhysicsSystem {
    public:
        b2World world;
        int velocityIterations = 6;
        int positionIterations = 2;

        Box2DPhysicsSystem(b2Vec2 gravity = { 0.0f, -9.8f });

        void Initialize() override;
        void Step(float dt) override;
    };
}
