#pragma once
#include <vector>
#include <string>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include "RenderLayer.h"
#include <vulkan/vulkan.h>

namespace ce {
    class RenderTechnique {

    protected:
        bool isVulkan;

        std::vector<std::unique_ptr<RenderLayer>> layers;

        std::unordered_map<std::string, std::shared_ptr<RenderLayer::TexBuffer>> initializedBuffers;

    public:
        RenderTechnique();
        virtual void InitializeLayers();
        void Render(ce::Camera& camera);
        void AddDrawable(std::weak_ptr<ce::Drawable> drawable);
    };
}
