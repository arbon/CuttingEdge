#pragma once
#include <iostream>
#include <memory>
#include "SpriteComponent.h"
#include "ExportMacro.h"

namespace ce {
    class ParallaxSpriteComponent : public ce::SpriteComponent, public ce::Processable {
        std::string vertShaderPath = "cutting_edge/Resources/Shaders/parallax-sprite-deferred.vert";
        std::string fragShaderPath = "cutting_edge/Resources/Shaders/parallax-sprite-deferred.frag";
        std::string shaderProgName = "parallax-sprite";

    public:
        DLL_EXPORT static std::string const SPRITE_OFFSET_UNIFORM_NAME;

        float DistanceRatio = 0.5f;
        glm::vec2 StartPosition = glm::vec2(0.0f, 0.0f);
        glm::vec2 WindowSize = glm::vec2(0.4f, 0.8f);

        ParallaxSpriteComponent(std::weak_ptr<Transform> transform, std::shared_ptr<Sprite> sprite, std::string name = "ParallaxSpriteComponent");

        virtual void Initialize() override;

        virtual std::weak_ptr<Processable> GetProcessable() override;

        virtual void Process() override;
    };
}
