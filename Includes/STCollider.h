#pragma once
#include <memory>
#include "Component.h"
#include "Box2D/Box2D.h"

namespace ce {
    class SmoothTilePhysicsSystem;

    class STCollider : public Component {
    public:
        std::weak_ptr<b2Fixture> fixture;

        STCollider(std::weak_ptr<ce::Transform> transform, std::weak_ptr<ce::SmoothTilePhysicsSystem> physics, std::shared_ptr<b2Shape> shape, std::shared_ptr<b2Body> body, std::string name = "STCollider");

        // Inherited via Component
        std::weak_ptr<Drawable> GetDrawable() override;
        std::weak_ptr<Processable> GetProcessable() override;
    };
}
