#pragma once
#include <unordered_set>
#include <vector>
#include <memory>
#include <iostream>
#include "gl_includes.h"
#include <string>
#include "Drawable.h"
#include "Camera.h"

namespace ce {
    class RenderLayer {
    public:
        struct TexBuffer
        {
            std::string name;
            GLuint Buffer;
            GLenum Target;
            GLint InternalFormat;
            GLenum Format;
            GLsizei Width;
            GLsizei Height;
            GLint Level;
            GLint Border;
            GLenum DataType;
            GLuint FBO;

            //const GLvoid* Data;

            TexBuffer() : name("default"), Buffer(0), Target(GL_TEXTURE_2D), InternalFormat(GL_RGBA), Format(GL_RGBA), Width(0), Height(0), Level(0), Border(0), DataType(GL_FLOAT), FBO(0)
            {
            }
        };

    protected:
        std::vector<std::shared_ptr<TexBuffer>> inputBufferReqs;
        std::vector<std::shared_ptr<TexBuffer>> inputBuffers;
        std::vector<std::shared_ptr<TexBuffer>> outputBuffers;

        std::vector<GLenum> drawBuffers;

        GLuint frameBuffer;
        GLuint stencilBuffer;

        std::vector<std::weak_ptr<ce::Drawable>> drawables;

    public:
        virtual void Initialize(std::vector<std::shared_ptr<TexBuffer>> inputBuffers);

        std::vector<std::shared_ptr<TexBuffer>> GetRequiredInputs();

        std::vector<std::shared_ptr<TexBuffer>> GetOutputBuffers();

        virtual void AddDrawable(std::weak_ptr<ce::Drawable> drawable);

        virtual void Render(Camera& camera) = 0;

        virtual ~RenderLayer() {};
    };
}