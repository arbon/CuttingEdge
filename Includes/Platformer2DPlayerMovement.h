#pragma once

#include <memory>
#include "Component.h"
#include "STRigidbody.h"
#include "Input.h"
#include "World.h"
#include "SpriteAnimation.h"
#include "SpriteAnimator.h"

namespace ce {
    class Platformer2DPlayerMovement : public Component, public Processable {
    protected:
        bool right;
        bool left;
        bool dummy;

        bool useDelayFrame;
        bool useJumpGuard;
        bool delayFrame;
        int jumpGuard;
        float currentJumpBoostRemaining;

    public:
        float jumpBoostAccel;
        float jumpVelocity;
        float maxJumpBoostTime;
        glm::vec4 maxVelocityBounds;
        float xAccel;
        float airXAccel;
        float xVelocity;

        std::shared_ptr<ce::Sprite> Sprite;
        std::shared_ptr<ce::SpriteAnimator> Animator;
        std::shared_ptr<ce::SpriteAnimation> IdleAnim;
        std::shared_ptr<ce::SpriteAnimation> RunAnim;

        std::shared_ptr<STRigidbody> rigidbody;

        Platformer2DPlayerMovement(std::weak_ptr<Transform> transform, std::shared_ptr<STRigidbody> rigidbody, std::string name = "Platformer2DPlayerMovement");

		void MoveLeft(bool start = false);
		void MoveRight(bool start = false);
		void Jump(bool start = false, bool force = false);

        // Inherited via Component
        virtual std::weak_ptr<Drawable> GetDrawable() override;
        virtual std::weak_ptr<Processable> GetProcessable() override;

        // Inherited via Processable
        virtual void Process() override;
    };
}
