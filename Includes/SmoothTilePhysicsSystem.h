#pragma once
#include <memory>
#include "PhysicsSystem.h"
#include "Box2D/Box2D.h"
#include <vector>
#include "STRigidbody.h"
#include "TileSystem.h"
#include <math.h>

namespace ce {
    class STContactListener : public b2ContactListener {
    public:
        void BeginContact(b2Contact* contact);
        void EndContact(b2Contact* contact);
    };

    class STCollider;
    class STRigidbody;

    class SmoothTilePhysicsSystem : public PhysicsSystem {
    public:
        std::shared_ptr<b2World> world;
        std::vector<std::shared_ptr<STRigidbody>> rigidbodies;
        std::shared_ptr<TileSystem> tileSystem;

        SmoothTilePhysicsSystem(std::shared_ptr<TileSystem> tileSystem);
        void Initialize() override;
        void Step(float dt) override;

    private:
        float const gravity = -19.81f;
        STContactListener contactListener;

        bool GetCollisionPosition(glm::vec2 &position, glm::vec2 dim, glm::vec2 direction, float stepHeight = 1.0f);
    };
}
