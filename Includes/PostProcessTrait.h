#pragma once
#include "DrawableTrait.h"

namespace ce {
    struct PostProcessTrait : public DrawableTrait {
    private:
        int priority;

    public:
        PostProcessTrait();
        int GetPriority() const;
        void SetPriority(int priority);

        // Inherited via DrawableTrait
        virtual bool operator<(const ce::DrawableTrait & rhs) const override;
    };
}
