#pragma once
#include <memory>
#include "Tile.h"
#include <glm/gtc/matrix_transform.hpp>

namespace ce {
    class Component;
    class TileSystem {
    public:
        virtual Tile& GetTile(glm::vec2 coord) = 0;
        virtual Tile& GetOffsetTile(glm::vec2 tilePos, glm::vec2 offset) = 0;
        virtual glm::vec2 WorldPosToTileCoord(glm::vec2 worldPos) = 0;
        virtual glm::vec2 TileCoordToWorldPos(glm::vec2 tileCoord) = 0;
        virtual float GetTileWidth() = 0;
        virtual bool IsTileBlocked(Tile& tile) = 0;
        virtual std::shared_ptr<Component> GetComponent() = 0;
    };
}
