#pragma once
#include <memory>
#include "RenderLayer.h"
#include "Component.h"

namespace ce {
    class ForwardOpaqueLayer : public RenderLayer
    {
        void Render(Camera& camera) override;
    };
}
