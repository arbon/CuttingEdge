#pragma once
#include <functional>
#include "Processable.h"

namespace ce {
    class AnonymousProcessable : public ce::Processable {
    
        std::function<void()> process;

    public:
        AnonymousProcessable(std::function<void()> process);

        // Inherited via Processable
        virtual void Process() override;
    };
}
