#pragma once
#include <memory>

namespace ce {
    class Drawable;

    struct DrawableTrait {
        std::weak_ptr<Drawable> drawable;

        virtual bool operator < (const ce::DrawableTrait& rhs) const { return false; };
    };
}
