#pragma once

namespace ce {
    class Processable {
    public:
        bool shouldProcess = true;

        //Function which will be called prior to draw calls every frame
        virtual void Process() = 0;

        virtual ~Processable() {};
    };
}
