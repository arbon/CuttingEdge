#pragma once
#include <memory>
#include <vector>
#include "Material.h"
#include "DrawableTrait.h"

namespace ce {
    class Component;

    class Drawable {
    protected:
        std::vector<ce::DrawableTrait*> traits;
        std::weak_ptr<Component> component;

    public:
        //Function which will be called in order to draw an inheriting object every frame
        virtual void Draw() = 0;

        virtual std::weak_ptr<Material> GetMaterial() = 0;

        virtual std::weak_ptr<Component> GetComponent() = 0;

        virtual void _SetComponent(std::weak_ptr<Component> component);

        template<typename T>
        T* GetTrait() {
            for (auto trait : this->traits) {
                if (T* asTrait = dynamic_cast<T*>(trait)) {
                    return asTrait;
                }
            }

            return nullptr;
        }
    };
}
