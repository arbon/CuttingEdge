#pragma once
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <functional>
#include <glm/gtc/matrix_transform.hpp>
#include "gl_includes.h"
#include "sdlinput.h"

namespace ce {
    class World;

    class Input {
    private:
        std::vector<std::function<void(SDL_Event&)>> rawInputHandlers;

        si::SDLInput wrapper;

        si::Profile profile;

    public:
        explicit Input();

        si::Profile& GetProfile();

        void HandleEvent(SDL_Event& event);

        void Update();

        static glm::vec3 ScreenToWorldPosition(glm::vec2 screenPos);
        static glm::vec2 WorldToScreenPosition(glm::vec3 worldPos);

        void AddRawInputHandler(std::function<void(SDL_Event&)> handler);

        static glm::vec2 GetMousePosition();
    };
}
