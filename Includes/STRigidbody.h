#pragma once
#include <memory>
#include <functional>
#include <glm/gtc/matrix_transform.hpp>
#include "Component.h"
#include "Box2D/Box2D.h"

namespace ce {
    class SmoothTilePhysicsSystem;
    class World;

    struct STCollision {
        b2Fixture* collider; 
        b2Fixture* other;
        std::shared_ptr<Component> otherComponent;
        glm::vec2 contactPoints[2];
        glm::vec2 normal;
    };

    class STRigidbody : public Component {
        std::vector<std::function<void(STCollision)>> startCollisionCallbacks;
        std::vector<std::function<void(STCollision)>> endCollisionCallbacks;
    
    public:
        bool isJumping;
        bool isCollidingWithWorld;
        bool SmoothTileEnabled;

        glm::vec2 dimensions = glm::vec2(1.0f, 1.0f);
        float stepHeight = 1.0f;
        b2Body* body;

        STRigidbody(std::weak_ptr<Transform> transform, std::string name = "STRigidbody");

        void SetVelocity(glm::vec2 velocity);
        glm::vec2 GetVelocity();

        void ApplyAcceleration(glm::vec2 acceleration);

        // Inherited via Component
        virtual std::weak_ptr<Drawable> GetDrawable() override;
        virtual std::weak_ptr<Processable> GetProcessable() override;

        void AddStartCollisionCallback(std::function<void(STCollision)> callback);
        void AddEndCollisionCallback(std::function<void(STCollision)> callback);

        void StartCollide(STCollision collision);
        void EndCollide(STCollision collision);
    };
}
