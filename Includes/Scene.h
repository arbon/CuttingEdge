#pragma once
#include <algorithm>
#include <vector>
#include <memory>
#include <list>
#include "Transform.h"
#include <unordered_set>
#include "Component.h"
#include "Camera.h"
#include "Drawable.h"
#include "Processable.h"
#include "Material.h"
#include "RenderTechnique.h"
#include "ForwardTechnique.h"

namespace ce {
    class Scene : public std::enable_shared_from_this<Scene> {
    private:
        std::vector<std::shared_ptr<Transform>> rootTransforms;
        std::vector<std::weak_ptr<Drawable>> drawables;
        std::list<std::weak_ptr<Processable>> processables;
        std::shared_ptr<Camera> activeCamera;
        std::unique_ptr<RenderTechnique> defaultTechnique;

        std::vector<std::weak_ptr<Transform>> toDeleteTransforms;
        std::vector<std::weak_ptr<Component>> toDeleteComponents;

        glm::mat4 mvp;

        std::shared_ptr<Scene> getPointer() {
            return shared_from_this();
        }

        void AddDrawable(std::weak_ptr<ce::Drawable> drawable);
        void AddProcessable(std::weak_ptr<ce::Processable> processable);

        void _DeleteTransform(std::weak_ptr<Transform> transform);

        void _DeleteComponent(std::weak_ptr<Component> component);

    public:
        Scene();
        Scene(std::unique_ptr<RenderTechnique> renderTechnique);

        void Process();

        void Draw();
        void Draw(RenderTechnique& technique);
        void Draw(Material& material);

        std::weak_ptr<Camera> GetActiveCamera() const;
        void SetActiveCamera(std::shared_ptr<Camera> camera);

        std::weak_ptr<Transform> CreateTransform(std::weak_ptr<Transform> parent, std::string name = "transform");

        void InitComponent(std::weak_ptr<Component> component);

        std::vector<std::weak_ptr<Drawable>> GetDrawables() const;

        const std::unique_ptr<ce::RenderTechnique>& GetDefaultRenderTechnique() const;

        void DeleteTransform(std::weak_ptr<Transform> transform);

        void DeleteComponent(std::weak_ptr<Component> component);

        template<typename C, typename... Args>
        std::weak_ptr<C> CreateComponent(Args&&... args)
        {
            std::shared_ptr<Component> newComponent = std::shared_ptr<Component>(new C(std::forward<Args>(args)...));

            auto transformLocked = newComponent->GetTransform().lock();

            if (transformLocked) {
                transformLocked->_AddComponent(newComponent);
            }

            newComponent->Initialize();

            if(auto drawableLocked = newComponent->GetDrawable().lock()) {
                drawableLocked->_SetComponent(newComponent);
            }

            this->AddDrawable(newComponent->GetDrawable());        
            this->AddProcessable(newComponent->GetProcessable());

            return std::static_pointer_cast<C>(newComponent);
        }
    };
}
