#pragma once

#if defined(CE_UI_RMLUI)
    #define RML_CORE Rml
    #define RML_CONTROLS Rml
    #define RML_DEBUGGER Rml::Debugger
    #define RML_FONT_DATABASE Rml
    #include <RmlUi/Core.h>
    #include <RmlUi/Debugger.h>

    #define RML_VERTEX_COLOUR_TYPE RML_CORE::ColourbPremultiplied

    namespace RML_CORE {
        typedef ElementPtr ElementPtrWrapper;

        inline Element* GetElementRawPtr(ElementPtrWrapper& Ptr) {
            return Ptr.get();
        }

        inline void SetXMLAttribute(RML_CORE::XMLAttributes& attributes, const char* key, const char* value)
        {
            attributes[key] = value;
        }

        inline const char* GetRmlCString(const RML_CORE::String& RmlString){
            return RmlString.c_str();
        }

        inline std::string GetRmlStdString(const RML_CORE::String& RmlString){
            return RmlString;
        }

        struct ContextDeleter
        {
            void operator()(RML_CORE::Context *p) const { 
                RML_CORE::Shutdown();
            }
        };
    }

#elif defined(CE_UI_ROCKET)
    #define RML_CORE Rocket::Core
    #define RML_CONTROLS Rocket::Controls
    #define RML_DEBUGGER Rocket::Debugger
    #define RML_FONT_DATABASE Rocket::Core::FontDatabase
    #include <Rocket/Core.h>
    #include <Rocket/Controls.h>
    #include <Rocket/Debugger.h>

    #define RML_VERTEX_COLOUR_TYPE RML_CORE::Colourb

    namespace RML_CORE {
        typedef Element* ElementPtrWrapper;
    
        inline Element* GetElementRawPtr(ElementPtrWrapper& Ptr) {
            return Ptr;
        }

        inline void SetXMLAttribute(RML_CORE::XMLAttributes& attributes, const char* key, const char* value)
        {
            attributes.Set(key, value);
        }

        inline const char* GetRmlCString(const RML_CORE::String& RmlString){
            return RmlString.CString();
        }

        inline std::string GetRmlStdString(const RML_CORE::String& RmlString){
            return std::string(RmlString.CString());
        }

        struct ContextDeleter
        {
            void operator()(RML_CORE::Context *p) const { 
                p->RemoveReference();
                RML_CORE::Shutdown();
            }
        };
    }

#endif