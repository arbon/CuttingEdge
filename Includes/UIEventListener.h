#pragma once
#include <functional>

#include "UI.h"

namespace ce {
    class UIEventListener : public RML_CORE::EventListener {
    public:
        std::function<void(RML_CORE::Event& event)> listener;

        UIEventListener(std::function<void(RML_CORE::Event & event)> listener);

        // Inherited via EventListener
        virtual void ProcessEvent(RML_CORE::Event & event) override;
    };
}
