#pragma once
#include <string>
#include "PostProcessEffect.h"
#include "ResourceManager.h"

class DeferredAmbientLight : public ce::PostProcessEffect {
    const std::string ambientVertPath = "cutting_edge/Resources/Shaders/ambient-light.vert";
    const std::string ambientFragPath = "cutting_edge/Resources/Shaders/ambient-light.frag";

public:
    const std::string INTESITY_UNIFORM_NAME = "intensity";

    DeferredAmbientLight();
};