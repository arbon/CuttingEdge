#pragma once

#include <unordered_map>
#include <memory>
#include <filesystem>
#include "gl_includes.h"
#include "Mesh.h"
#include <string>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Texture.h"
#include "ShaderProgram.h"
#include "stb_image_header.h"
#include "ExportMacro.h"
#include "ArrayTexture.h"

namespace ce {
    class ResourceManager {
    public:
        static DLL_EXPORT unsigned int const GEOM_PLANE;
        static DLL_EXPORT unsigned int const GEOM_SCREEN_QUAD;

        static DLL_EXPORT std::vector<std::string> const GEOM_NAMES;
        static std::vector<std::pair<std::vector<Vertex>, std::vector<GLuint>>> const GEOMS;

        static std::shared_ptr<Mesh::MeshData> LoadPrimitiveGeometry(unsigned int GEOMETRY);

        // Return new Mesh created with MeshData from meshMap, 
        // if MeshData for given path is not in meshMap, 
        // loads it in first
        static std::vector<std::shared_ptr<Mesh>> LoadMesh(std::string path, unsigned int importFlags = aiProcess_Triangulate);

        // Return MeshData from meshMap,
        // if MeshData for given path is not in meshMap, 
        // loads it in first
        static std::vector<std::shared_ptr<Mesh::MeshData>> LoadMeshData(std::string path, unsigned int importFlags);

        static bool DeleteMeshData(std::string name);

        static bool AddMeshData(std::shared_ptr<Mesh::MeshData> meshData);

        static std::vector<std::shared_ptr<Mesh::MeshData>> GetMeshData(std::string path);

        // Attempt to upload mesh data to GPU
        // if mesh data isn't buffered, then generate buffers for it, upload them to the GPU, 
        // and populate meshData fields (vbo, ebo) with buffer indices and return true
        // if data for a mesh is already buffered, do nothing and return false
        static bool BufferMesh(std::shared_ptr<Mesh::MeshData>& meshData);

        static std::shared_ptr<Texture> LoadTexture(std::string path);

        static std::shared_ptr<Texture> LoadTexture(std::shared_ptr<Texture::TextureData> texData);

        static bool DeleteTexture(std::string name);

        static bool BufferTexture(std::shared_ptr<Texture::TextureData>& texData);

        static bool BufferArrayTexture(std::shared_ptr<ArrayTexture>& arrayTexture);

        static bool CompileShaderProgram(std::shared_ptr<ShaderProgram>& shader);

        static std::shared_ptr<ShaderProgram> GetShaderProgram(std::string name);

        static std::shared_ptr<ShaderProgram::Shader> LoadShader(std::filesystem::path path);

    private:
        static std::unique_ptr<Assimp::Importer> importer;

        // unordered_map which stores data necessary to buffer and draw meshes
        static std::unordered_map<std::string, std::vector<std::shared_ptr<Mesh::MeshData>>> meshMap;

        static std::unordered_map<std::string, std::shared_ptr<Texture::TextureData>> textureMap;

        static std::unordered_map<std::string, std::shared_ptr<ShaderProgram::Shader>> shaderMap;

        static std::unordered_map<std::string, std::shared_ptr<ShaderProgram>> shaderProgramMap;

        //Generate flat array for vertex buffer from array of vertex vectors
        static std::vector<Vertex> CreateFlatVertexArray(aiMesh& mesh);

        //Generate flat array for element buffer from array of element vectors
        static std::vector<GLuint> CreateFlatElementArray(aiFace* elements, unsigned int numElems);
    };
}
