#pragma once
#include <set>
#include "RenderLayer.h"
#include "ZIndexed.h"
#include "World.h"

namespace ce {
    class Deferred2DBufferLayer : public ce::RenderLayer {
    private:
        std::multiset<ZIndexed> zDrawables;

    public:
        Deferred2DBufferLayer();
        virtual void Render(ce::Camera & camera) override;
        virtual void AddDrawable(std::weak_ptr<ce::Drawable> drawable) override;
    };
}