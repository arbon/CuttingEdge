#pragma once
#include "RenderTechnique.h"
#include "ForwardOpaqueLayer.h"
#include "ForwardTransparentLayer.h"

namespace ce {
    class ForwardTechnique : public RenderTechnique
    {
    public:
        ForwardTechnique();
    };
}
