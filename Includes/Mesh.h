#pragma once
#include "gl_includes.h"
#include <vector>
#include <string>
#include <memory>
#include "Component.h"
#include "Drawable.h"
#include <glm/gtc/type_ptr.hpp>
#include "Vertex.h"
#include "Material.h"

namespace ce {
    class Mesh : public Drawable {
    public:
        struct MeshData {
            std::string path;
            std::string name;
            GLuint vao;
            GLuint vbo;
            GLuint ebo;
            std::vector<GLuint> elements;
            std::vector<Vertex> vertices;
            GLuint elementsCount;
            GLuint verticesCount;
            std::shared_ptr<Material> material;
        };

        static std::string const POS_ATTRIB_NAME;
        static std::string const NORMAL_ATTRIB_NAME;
        static std::string const COLOR_ATTRIB_NAME;

        std::shared_ptr<Material> material;

        Mesh(std::shared_ptr<MeshData> meshData);

        // Gets mesh data
        std::weak_ptr<MeshData> GetMeshData() const;

        std::weak_ptr<Material> GetMaterial() override;

        std::weak_ptr<Component> GetComponent() override;

        void Draw() override;

        void RetrieveBufferedData();

    private:
        std::shared_ptr<MeshData> meshData;
    };
}
