#pragma once

#include <functional>
#include <vector>

#include <vulkan/vulkan.h>
#include "gl_includes.h"
#include "ShaderProgram.h"

namespace ce
{
    class Window;

    enum class RHI_Error
    {
        OK,
        FAILURE,
        INIT_ERROR,
        INVALID_STATE,
        BAD_INPUT,
        UNSUPPORTED,
        NOT_IMPLEMENTED,
        NOT_READY
    };

    enum class RHI_TYPE
    {
        UNKNOWN,
        OPENGL,
        VULKAN
    };

    class RHI 
    {
    public:
        virtual RHI_Error InitialiseRHI(Window* window) = 0;
        virtual Uint32 GetWindowFlags() = 0;
        virtual RHI_Error DrawFrame(Window* window, std::function<void(void)> drawSceneFunc) = 0;
        virtual RHI_Error CleanupRHI() = 0;
        virtual RHI_TYPE GetRHIType() = 0;

        virtual RHI_Error LoadShader(ShaderProgram::Shader& shader) = 0;
        virtual RHI_Error CompileShaderProgram(ShaderProgram& program) = 0;
    };

    class OpenGLRHI : public RHI
    {
    public:
        virtual RHI_Error InitialiseRHI(Window* window) override;
        virtual Uint32 GetWindowFlags() override;
        virtual RHI_Error DrawFrame(Window* window, std::function<void(void)> drawSceneFunc) override;
        virtual RHI_Error CleanupRHI() override;
        virtual RHI_TYPE GetRHIType() override { return RHI_TYPE::OPENGL; };

        virtual RHI_Error LoadShader(ShaderProgram::Shader& shader) override;
        virtual RHI_Error CompileShaderProgram(ShaderProgram& program) override;

        RHI_Error CompileShader(ShaderProgram::Shader& shader);

    private:
        SDL_GLContext sdlContext;
    };

    class VulkanRHI : public RHI
    {
    public:
        struct SwapChainSupportDetails {
            VkSurfaceCapabilitiesKHR capabilities;
            std::vector<VkSurfaceFormatKHR> formats;
            std::vector<VkPresentModeKHR> presentModes;
        };

        virtual RHI_Error InitialiseRHI(Window* window) override;
        virtual Uint32 GetWindowFlags() override;
        virtual RHI_Error DrawFrame(Window* window, std::function<void(void)> drawSceneFunc) override;
        virtual RHI_Error CleanupRHI() override;
        virtual RHI_TYPE GetRHIType() override { return RHI_TYPE::VULKAN; };
        RHI_Error GetSwapChainImageFormat(VkSurfaceFormatKHR& OutFormat) {
            if (swapChain == VK_NULL_HANDLE) {
                return RHI_Error::NOT_READY;
            }

            OutFormat = this->surfaceFormat;
            return RHI_Error::OK;
        };

        virtual RHI_Error LoadShader(ShaderProgram::Shader& shader) override;
        virtual RHI_Error CompileShaderProgram(ShaderProgram& program) override;

        RHI_Error CompileShader(ShaderProgram::Shader& shader);
        RHI_Error CreateShaderModule(ShaderProgram::Shader& shader);
        RHI_Error PopulateShaderDescriptorInfo(ShaderProgram::Shader& shader);

        RHI_Error GetPipelineShaderStageCreateInfo(const ShaderProgram::Shader& shader, VkPipelineShaderStageCreateInfo& createInfo);
        RHI_Error CreateGraphicsPipeline(std::vector<ShaderProgram::Shader*> shaders);


        RHI_Error RegisterExtension(std::string extensionName);
        bool IsExtensionSupported(std::string extensionName);
        bool AreValidationLayersSupported();
        SwapChainSupportDetails QuerySwapChainSupport(VkPhysicalDevice device);

    private:
        VkInstance instance;
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
        VkDevice device;

        uint32_t graphicsQueueFamilyIndex;
        VkQueue graphicsQueue;

        uint32_t presentQueueFamilyIndex;
        VkQueue presentQueue;

        VkSurfaceKHR surface;

        VkCommandPool commandPool;
        VkCommandBuffer commandBuffer;
        
        VkSwapchainKHR swapChain = VK_NULL_HANDLE;
        VkSurfaceFormatKHR surfaceFormat;
        VkPresentModeKHR presentMode;
        VkExtent2D extent;
        std::vector<VkImage> swapChainImages;
        std::vector<VkImageView> swapChainImageViews;
        std::vector<VkFramebuffer> swapChainFramebuffers;

        std::vector<std::string> additionalExtensions;

        const std::vector<const char*> validationLayers = {
            "VK_LAYER_KHRONOS_validation"
        };

        #ifdef NDEBUG
        const bool enableValidationLayers = false;
        #else
        const bool enableValidationLayers = true;
        #endif

        const std::vector<const char*> deviceExtensions = {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };

        bool logShaderDescriptors = true;

        RHI_Error CreateInstance(Window* window);
        RHI_Error CreateSurface(Window* window);
        RHI_Error SelectPhysicalDevice();
        RHI_Error CreateLogicalDevice();
        RHI_Error InitialiseQueueHandles();
        RHI_Error CreateSwapChain();
        RHI_Error CreateSwapChainImageViews();
        RHI_Error CreateGraphicsPipeline();
        RHI_Error CreateCommandBuffer();
        RHI_Error CreateCommandPool();
        RHI_Error CreateSwapChainFrameBuffers();
        RHI_Error CreateRenderPass();

        std::vector<VkExtensionProperties> GetSupportedExtensions();
        std::vector<uint32_t> FindQueueFamiliesWithPresentSupport(VkPhysicalDevice device);
    };
}
