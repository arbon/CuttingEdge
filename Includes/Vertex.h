#pragma once

#include <array>

#include "gl_includes.h"
#include <vulkan/vulkan.h>
#include <glm/gtc/matrix_transform.hpp>

namespace ce {
    class Vertex {

    public:
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec4 color;
        glm::vec2 uv;

        static VkVertexInputBindingDescription GetBindingDescription() {
            VkVertexInputBindingDescription bindingDescription{};
            bindingDescription.binding = 0;
            bindingDescription.stride = sizeof(Vertex);
            bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            return bindingDescription;
        }

        static std::array<VkVertexInputAttributeDescription, 4> GetAttributeDescriptions() {
            std::array<VkVertexInputAttributeDescription, 4> attributeDescriptions{};
            attributeDescriptions[0].binding = 0;
            attributeDescriptions[0].location = 0;
            attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
            attributeDescriptions[0].offset = offsetof(Vertex, position);

            attributeDescriptions[1].binding = 0;
            attributeDescriptions[1].location = 0;
            attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
            attributeDescriptions[1].offset = offsetof(Vertex, normal);

            attributeDescriptions[2].binding = 0;
            attributeDescriptions[2].location = 0;
            attributeDescriptions[2].format = VK_FORMAT_R32G32B32A32_SFLOAT;
            attributeDescriptions[2].offset = offsetof(Vertex, color);

            attributeDescriptions[3].binding = 0;
            attributeDescriptions[3].location = 0;
            attributeDescriptions[3].format = VK_FORMAT_R32G32_SFLOAT;
            attributeDescriptions[3].offset = offsetof(Vertex, uv);

            return attributeDescriptions;
        }

        Vertex();
        explicit Vertex(glm::vec3 position);
        Vertex(glm::vec3 position, glm::vec3 normal);
        Vertex(glm::vec3 position, glm::vec3 normal, glm::vec4 color);
        Vertex(glm::vec3 position, glm::vec3 normal, glm::vec3 color);
        Vertex(glm::vec3 position, glm::vec3 normal, glm::vec4 color, glm::vec2 uv);
        Vertex(glm::vec3 position, glm::vec3 normal, glm::vec3 color, glm::vec2 uv);
    };
}
