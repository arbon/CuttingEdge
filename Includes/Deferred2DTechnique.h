#pragma once
#include <memory>
#include "RenderTechnique.h"

namespace ce {
    class Deferred2DTechnique : public ce::RenderTechnique {
    public:
        Deferred2DTechnique();
        virtual void InitializeLayers() override;
    };
}
