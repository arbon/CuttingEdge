#pragma once
#include <cstddef>
#include <stdlib.h>

namespace ce {
    //cross platform aligned allocation and freeing of memory
    class XAlloc {
    public:
        static void * aligned_malloc(size_t size, size_t alignment);
        static void aligned_free(void * p);
    };
}
