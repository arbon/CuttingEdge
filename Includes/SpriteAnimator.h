#pragma once
#include <vector>
#include <memory>
#include "SpriteAnimation.h"
#include "SpriteComponent.h"
#include "Processable.h"
#include "TimeUtil.h"
#include <math.h>

namespace ce {
    class SpriteAnimator : public Processable, public Component {
    public:
        std::vector<std::shared_ptr<SpriteAnimation>> animations;
        std::shared_ptr<SpriteAnimation> currentAnimation;
        std::shared_ptr<SpriteComponent> sprite;

        float playPosition;
        float playRate;
        bool isPlaying;

        SpriteAnimator(std::weak_ptr<Transform> transform, std::string name, std::shared_ptr<SpriteComponent> sprite);

        void Play(std::string name);
        void Play(std::shared_ptr<SpriteAnimation> animation);

        void Stop();

        std::weak_ptr<Drawable> GetDrawable() override;

        std::weak_ptr<Processable> GetProcessable() override;

        void Process() override;

    private:

        bool CanPlay();
    };
}