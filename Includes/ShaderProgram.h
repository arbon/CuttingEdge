#pragma once
#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <unordered_map>
#include <filesystem>
#include "gl_includes.h"
#include <vulkan/vulkan.h>

namespace ce {
    enum class GLSLStorageType {
        UNKNOWN,
        PUSH_CONSTANT,
        UNIFORM,
        STORAGE_BUFFER
    };

    enum class GLSLTypeGroups {
        UNKNOWN,
        SCALAR,
        VECTOR,
        MATRIX,
        STRUCT,
        SAMPLER,
        IMAGE,
        ARRAY
    };

    enum class GLSLScalarType {
        NA,
        BOOL,
        INT,
        UINT,
        FLOAT,
        DOUBLE
    };

    struct DescriptorSetLayoutData {
        uint32_t set_number;
        VkDescriptorSetLayoutCreateInfo create_info;
        std::vector<VkDescriptorSetLayoutBinding> bindings;
    };

    struct VulkanShaderTypeInfo
    {
        GLSLTypeGroups type_group;
        GLSLScalarType scalar_type;
        unsigned int num_components[2];
        VkFormat format;
    };

    struct VulkanShaderAttributeData
    {
        std::string name;
        bool is_input;
        VulkanShaderTypeInfo type;
        uint32_t location;
        uint32_t offset;
        uint32_t binding;
    };

    struct VulkanShaderVariableData
    {
        std::string name;
        VulkanShaderTypeInfo type;
        uint32_t set;
        uint32_t binding;
    };

    struct VulkanShaderData
    {
        std::vector<uint32_t> byteCode;
        VkShaderModule shaderModule;
        std::vector<DescriptorSetLayoutData> descSetsLayoutData;
        VkVertexInputBindingDescription inputBindingDescription;
        std::vector<VulkanShaderAttributeData> inputAttributes;
        std::vector<VulkanShaderAttributeData> outputAttributes;
        std::vector<VulkanShaderVariableData> shaderVariables;
        std::vector<VkVertexInputAttributeDescription> inputAttributeDecriptions;
        std::vector<VkPushConstantRange> pushConstantBlocks;
    };

    struct VulkanShaderProgramData
    {

    };

    struct OpenGLShaderData
    {
        GLuint id;
        GLuint type;
    };

    struct OpenGLShaderProgramData
    {
        GLuint programId;
    };

    class ShaderProgram
    {
    public:

        const static char* VERT_EXTENSION;
        const static char* FRAG_EXTENSION;
        const static char* GEOM_EXTENSION;

        enum class ShaderType
        {
            VERTEX_SHADER,
            FRAGMENT_SHADER,
            GEOMETRY_SHADER,
            UNKNOWN
        };

        struct Shader
        {
            ~Shader()
            {
                if(vulkanData) delete vulkanData;
                if(glData) delete glData;
            }

            std::filesystem::path path;
            std::vector<char> source;
            ShaderType type = ShaderType::UNKNOWN;
            VulkanShaderData* vulkanData = nullptr;
            OpenGLShaderData* glData = nullptr;
        };

        // Todo: Remove
        GLuint programId;

        std::string name;

        std::vector<std::weak_ptr<Shader>> shaders;

        std::vector<std::pair<std::string, GLuint>> properties;

        explicit ShaderProgram(std::string name);

        bool CheckShaderStatus(const Shader& shader, std::string &info);
    };
}
