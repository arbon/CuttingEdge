#pragma once
#include <memory>
#include "Component.h"
#include "Mesh.h"
#include "ResourceManager.h"

namespace ce {
    class MeshComponent : public Component {
        std::shared_ptr<Mesh> mesh;

    public:
        MeshComponent(std::weak_ptr<Transform> transform, std::shared_ptr<Mesh> mesh, std::string name = "MeshComponent");

        void SetMaterial(std::shared_ptr<Material> material);
        std::weak_ptr<Material> GetMaterial();

        std::weak_ptr<Drawable> GetDrawable() override;
        std::weak_ptr<Processable> GetProcessable() override;
    };
}
