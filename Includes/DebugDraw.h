#pragma once

#include "DebugDrawComponent.h"
#include "TextComponent.h"

namespace ce {
	class DebugDraw {
        static std::shared_ptr<ce::Transform> debugTransform;
        static std::shared_ptr<ce::Material> material;
        static std::string debugVertShaderPath;
        static std::string debugFragShaderPath;
        static std::string debugShaderName;

        static std::shared_ptr<ce::Material> textMaterial;
        static std::string debugTextVertShaderPath;
        static std::string debugTextFragShaderPath;
        static std::string debugTextShaderName;
        static std::string debugTextFontPath;

public:
        static std::weak_ptr<DebugDrawComponent> DrawDebugObject(const std::shared_ptr<ce::Scene>& scene, DebugDrawObject drawObject);

        static std::weak_ptr<DebugDrawComponent> DrawCube(const std::shared_ptr<ce::Scene>& scene, glm::vec3 position, glm::quat rotation = glm::quat(), glm::vec3 dimensions = glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3 color = { 1.0f, 0.0f, 0.0f }, float lifetime = -1.0f, float lineWidth = 1.0f);

        static std::weak_ptr<DebugDrawComponent> DrawLines(const std::shared_ptr<ce::Scene>& scene, std::vector<glm::vec3> positions, glm::vec3 color = { 1.0f, 0.0f, 0.0f }, float lifetime = -1.0f, float lineWidth = 1.0f);
        static std::weak_ptr<DebugDrawComponent> DrawLine(const std::shared_ptr<ce::Scene>& scene, glm::vec3 start, glm::vec3 end, glm::vec3 color = { 1.0f, 0.0f, 0.0f }, float lifetime = -1.0f, float lineWidth = 1.0f);
        static std::weak_ptr<TextComponent> DrawText(const std::shared_ptr<ce::Scene>& scene, std::string text, glm::vec3 minBound, glm::vec3 maxBound, float fontSize = 12.0f, glm::vec3 color = { 1.0f, 0.0f, 0.0f }, float lifetime = -1.0f);

	};
}
