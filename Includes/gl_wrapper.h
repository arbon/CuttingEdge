#include "gl_includes.h"

namespace GL {
    void bindVertexArray(int vao);
    void activeTexture(int index);
}