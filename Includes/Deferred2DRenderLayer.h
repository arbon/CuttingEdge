#pragma once
#include <set>
#include "RenderLayer.h"
#include "PostProcessEffect.h"
#include "PostProcessTrait.h"

namespace ce {
    class Deferred2DRenderLayer : public ce::RenderLayer {

        std::multiset<ce::PostProcessTrait> ppDrawables;

    public:
        Deferred2DRenderLayer();

        virtual void Render(ce::Camera & camera) override;
        virtual void AddDrawable(std::weak_ptr<ce::Drawable> drawable) override;
    };
}