#pragma once
#include <string>
#include <vector>
#include <glm/gtc/matrix_transform.hpp>

namespace ce {
    class SpriteAnimation {
    public:
        std::string name;
        std::vector<glm::vec4> frames;
        float fps;
    };
}