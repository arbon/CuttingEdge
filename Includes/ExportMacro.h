#ifndef _CuttingEdge_DLLDEFINES_H_
#define _CuttingEdge_DLLDEFINES_H_

#if !defined(CUTTINGEDGE_STATIC)
    #if defined (_WIN32) 
      #if defined(CuttingEdge_EXPORTS)
        #define DLL_EXPORT __declspec(dllexport)
      #else
        #define  DLL_EXPORT __declspec(dllimport)
      #endif /* CuttingEdge_EXPORTS */
    #else /* defined (_WIN32) */
     #define DLL_EXPORT
    #endif
#else
    #define DLL_EXPORT
#endif

#endif /* _CuttingEdge_DLLDEFINES_H_ */