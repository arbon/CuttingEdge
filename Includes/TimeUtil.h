#pragma once
#include <chrono>
#include <string>
#include <iostream>
#include <unordered_map>

namespace ce {
    class Time {
    private:
        static float deltaTime;
        static long long currentTime;

        static std::unordered_map<std::string, long long> flags;

        static Time* instance;

    public:
        static bool isFixedTimeStep;
        static float fixedTimeStep;

        Time();

        static void Tick();

		static float GetDeltaTime();
		static float GetRealDeltaTime();

        static float Flag(std::string flag);
    };
}
