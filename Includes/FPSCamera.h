#pragma once
#include <memory>
#include "Component.h"
#include "Camera.h"
#include "TimeUtil.h"
#include "Input.h"
#include "Processable.h"
#include "World.h"

namespace ce {
    class FPSCamera : public Component, public Processable
    {
    private:
        float forward;
        float left;

        std::shared_ptr<Camera> camera;

    public:
        FPSCamera(std::weak_ptr<Transform> transform, std::weak_ptr<Camera> camera, std::string name = "FPSCamera");

        std::weak_ptr<Drawable> GetDrawable() override;

        std::weak_ptr<Processable> GetProcessable() override;

        void Process() override;
    };
}
