#pragma once
#include <unordered_map>

#include "UI.h"
#include "Component.h"
#include "LibRocketRenderInterface.h"
#include "LibRocketSystemInterface.h"
#include "World.h"

namespace ce {
    class UIComponent : public ce::Component, public ce::Processable, public ce::Drawable {

        std::unordered_map<std::string, RML_CORE::ElementDocument*> loadedDocuments;

    public:
        std::shared_ptr<ce::LibRocketRenderInterface> renderInterface;
        std::shared_ptr<ce::LibRocketSystemInterface> systemInterface;
        std::unique_ptr<RML_CORE::Context, RML_CORE::ContextDeleter> context;

        UIComponent(std::weak_ptr<Transform> transform, std::string name = "UI Component");

        // Inherited via Component
        virtual std::weak_ptr<Drawable> GetDrawable() override;
        virtual std::weak_ptr<Processable> GetProcessable() override;

        // Inherited via Processable
        virtual void Process() override;

        // Inherited via Drawable
        virtual void Draw() override;
        virtual std::weak_ptr<Material> GetMaterial() override;
        virtual std::weak_ptr<Component> GetComponent() override;

        RML_CORE::ElementDocument* LoadDocument(std::string path);

        bool DoPassMouseInput();
    };
}