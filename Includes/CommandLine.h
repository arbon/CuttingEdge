#pragma once
#include <vector>
#include <string>

namespace ce {
    class CommandLine {
        std::vector<std::string> RawCommandLine;
        std::vector<std::string> PositionalArguments;
        std::vector<std::pair<std::string, std::string>> KeyValueArguments;

    public:
        CommandLine() {};
        CommandLine(const std::vector<std::string>& RawCommandLine);

        const std::string& GetExecutablePath() const;
        const std::vector<std::string>& GetPositionalArguments() const;
        const std::string* GetKeyValueArgument(const std::string& Key) const;
    };
}
