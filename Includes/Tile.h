#pragma once
#include <glm/gtc/matrix_transform.hpp>

namespace ce {
    class Tile {
    public:
        virtual glm::vec2 GetCoord() { return{ 0,0 }; };
        virtual bool IsSloped() { return false; };
        virtual unsigned int GetSlopeAngle() { return 0; };
    };
}
