#pragma once
#include <vector>
#include <memory>
#include "Texture.h"

namespace ce {
    class ArrayTexture {
    public:
        std::vector<std::shared_ptr<ce::Texture>> Textures;

        GLuint Index;

        GLuint Width;
        GLuint Height;

        ArrayTexture();

        void AddTexture(std::shared_ptr<ce::Texture> texture);
    };
}
