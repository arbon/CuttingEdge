#pragma once

namespace ce {
    class PhysicsSystem {
    public:
        virtual void Initialize() = 0;
        virtual void Step(float dt) = 0;
    };
}
