#pragma once
#include <memory>
#include <glm/gtc/matrix_transform.hpp>
#include "Transform.h"
#include "XAlloc.h"

namespace ce {
    class Camera {
    public:
        enum ProjectionMode
        {
            Perspective,
            Orthographic
        };

        explicit Camera(ProjectionMode projectionMode, std::weak_ptr<Transform> transform);
        explicit Camera(ProjectionMode projectionMode, std::weak_ptr<Transform> transform, int width, int height);

    private:
        const static int ALLOC = 16;

        ProjectionMode projectionMode;

        std::weak_ptr<Transform> transform;

        glm::mat4 projectionMatrix;

        bool isProjDirty;

        float left;
        float right;
        float top;
        float bottom;

        float nearClip;
        float farClip;

        float fov;
        float aspect;

    public:
        glm::mat4 GetProjectionMatrix();

        glm::mat4 GetViewMatrix();

        glm::mat4 GetViewProjectionMatrix();

        std::weak_ptr<Transform> GetTransform();

        glm::vec2 GetWorldToCameraProjectedPosition(glm::vec3 worldPosition);

        ProjectionMode GetProjectionMode() const;
        void SetProjectionMode(ProjectionMode projectionMode);

        float GetLeft() const;
        void SetLeft(float left);

        float GetRight() const;
        void SetRight(float right);

        float GetTop() const;
        void SetTop(float top);

        float GetBottom() const;
        void SetBottom(float bottom);

        float GetNear() const;
        void SetNear(float nearClip);

        float GetFar() const;
        void SetFar(float farClip);

        float GetFOV() const;
        void SetFOV(float fov);

        float GetAspect() const;
        void SetAspect(float aspect);

        static void* operator new(size_t size);

        static void operator delete (void *p);
    };
}
