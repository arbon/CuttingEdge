#pragma once
#include <memory>
#include <unordered_map>
#include "Texture.h"
#include <glm/gtc/matrix_transform.hpp>
#include "ShaderProgram.h"

namespace ce {
    class Shader;

    class Material
    {
    public:

        static GLuint boundProgram;

        std::unordered_map<std::string, GLint> propertyMap;

        std::vector<std::string> propTest;
        std::vector<std::shared_ptr<void>> propertyData;

        std::shared_ptr<ShaderProgram> shader;

        Material();
        explicit Material(std::shared_ptr<ShaderProgram> shader);

        void Bind();
        void Unbind();

        void SetFloat(std::string name, GLfloat& data);
        void SetVector2(std::string name, glm::vec2& data);
        void SetVector3(std::string name, glm::vec3& data);
        void SetVector4(std::string name, glm::vec4& data);
        void SetMatrix4(std::string name, const glm::mat4& data);
        void SetTexture(std::string name, Texture& data);
        void SetTexture(std::string name, GLuint& data);

        void SetShader(std::shared_ptr<ShaderProgram> shader);

        void UpdateAllUniforms();
    };
}
