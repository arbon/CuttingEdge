#pragma once

#include <string>
#include <memory>

#include <glm/common.hpp>

#include "gl_includes.h"

namespace ce {
    class RHI;

    struct sdl_deleter
    {
        void operator()(SDL_Window *p) const { SDL_DestroyWindow(p); }
        void operator()(SDL_Renderer *p) const { SDL_DestroyRenderer(p); }
        void operator()(SDL_Texture *p) const { SDL_DestroyTexture(p); }
    };

    struct CreateWindowParameters
    {
        std::string name;
        int x = 0;
        int y = 0;
        unsigned int width;
        unsigned int height;
        Uint32 sdlFlags = 0;
        std::weak_ptr<RHI> rhi;
    };

    class Window
    {
    public:
        Window(const CreateWindowParameters& params);
        glm::vec2 GetWindowSize();
        SDL_Window* GetRawWindow();

    private:
        std::unique_ptr<SDL_Window, sdl_deleter> sdlWindow;
    };

}