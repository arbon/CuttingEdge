#pragma once
#include <set>
#include "UIComponent.h"
#include "RenderLayer.h"

namespace ce {
	class DebugRenderLayer : public ce::RenderLayer {

	public:
		DebugRenderLayer();

		virtual void Render(ce::Camera & camera) override;
		virtual void AddDrawable(std::weak_ptr<ce::Drawable> drawable) override;
	};
}