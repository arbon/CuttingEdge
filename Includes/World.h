#pragma once
#include <fstream>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <memory>

#include "TimeUtil.h"

#include "Window.h"
#include "gl_includes.h"
#include "ResourceManager.h"
#include "Scene.h"
#include "FPSCamera.h"
#include "MeshComponent.h"
#include "SpriteComponent.h"
#include "SpriteAnimator.h"
#include "SpriteAnimation.h"
#include "Texture.h"
#include "guicon.h"
#include "PhysicsSystem.h"
#include "Box2DPhysicsSystem.h"
#include "PlatformUtils.h"
#include "CommandLine.h"

namespace ce {
    class RHI;
    class Input;

    struct WorldConfig {
        unsigned int WindowWidth = 1200;
        unsigned int WindowHeight = 800;
        std::string WindowName = "Cutting Edge Game";
        std::vector<std::string> CommandLineArguments;

        #ifdef BAZEL_RUNFILES_ENABLED
            std::string RunfilesRepository;
        #endif
    };

    class World {
    protected:
        static ce::World* instance;

        std::ofstream logfile;

        std::unique_ptr<Window> mainWindow;
        SDL_Event windowEvent;

        std::vector<std::shared_ptr<ce::PhysicsSystem>> physicsSystems;
        double frameCount;
        double totalTime;
        double accumulator;
        double dt;
        double t;

        std::shared_ptr<ce::Scene> scene;
        std::shared_ptr<ce::RHI> rhi;
        std::unique_ptr<ce::PlatformUtils> platformUtils;

        ce::CommandLine commandLine;

    public:
        bool ShowFPS;

        ce::Input Input;

        World();
        virtual void Initialize(WorldConfig worldConfig = WorldConfig());
        int Tick();
        void LoadScene(std::string path);
        void LoadScene(std::shared_ptr<ce::Scene> scene);
        void AddPhysicsSystem(std::shared_ptr<ce::PhysicsSystem> physics);
        void Cleanup();
        std::weak_ptr<Scene> GetScene() const;
        std::weak_ptr<ce::RHI> GetRHI() const;
        const PlatformUtils& GetPlatformUtils() const;
        glm::vec2 GetWindowSize();
        float GetPhysicsTickTime();
        double GetTimeSinceWorldCreation() const;
        const ce::CommandLine& GetCommandLine() const;

        static ce::World* GetWorld();

        template<typename T>
        std::shared_ptr<T> GetRHIAs() {
            return std::dynamic_pointer_cast<T>(this->GetRHI());
        }

        template<typename T>
        T* GetPhysicsSystem() {
            for (auto physics : this->physicsSystems) {
                T* physicsTyped = dynamic_cast<T*>(physics.get());

                if (physicsTyped != nullptr) {
                    return physicsTyped;
                }
            }

            return nullptr;
        }
    };
}
