#pragma once
#include "gl_includes.h"
#include <vector>
#include <string>
#include <memory>
#include "Component.h"
#include "Drawable.h"
#include <glm/gtc/type_ptr.hpp>
#include "Vertex.h"
#include "Material.h"
#include "Mesh.h"
#include "ZIndexed.h"

namespace ce {
    class Sprite : public ce::Drawable {
    public:
        struct SpriteData {
            std::shared_ptr<Texture> texture;
            std::string name;
            std::shared_ptr<Material> material;
            glm::vec4 coords;
        };

        static std::string const POS_ATTRIB_NAME;
        static std::string const NORMAL_ATTRIB_NAME;
        static std::string const COLOR_ATTRIB_NAME;
        static std::string const COORD_UNIFORM_NAME;
        static std::string const MAINTEX_UNIFORM_NAME;

        bool FlippedX;

        std::shared_ptr<Material> material;

        explicit Sprite(std::shared_ptr<SpriteData> spriteData);

        void SetSpriteCoordsFromPixelCoords(const glm::vec4 &pixelCoords);

        static glm::vec4 CalculateSpriteCoords(const glm::vec4 &pixelCoords, glm::vec2 textureDimensions);

        // Gets sprite data
        std::weak_ptr<SpriteData> GetSpriteData() const;

        std::weak_ptr<Material> GetMaterial() override;

        void _SetMeshData(std::shared_ptr<Mesh::MeshData> meshData);

        std::weak_ptr<Component> GetComponent() override;

        void Draw() override;

    private:
        std::shared_ptr<SpriteData> spriteData;
        std::shared_ptr<Mesh::MeshData> meshData;
    };
}
