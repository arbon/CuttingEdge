#pragma once
#include "Drawable.h"
#include "Mesh.h"
#include "ResourceManager.h"
#include "PostProcessTrait.h"

namespace ce {
    class PostProcessEffect : public ce::Drawable {
    protected:
        std::shared_ptr<ce::Material> material;
        std::shared_ptr<ce::Mesh::MeshData> meshData;
    
    public:

        PostProcessEffect();
        virtual void Draw() override;
        virtual std::weak_ptr<ce::Material> GetMaterial() override;
        virtual std::weak_ptr<ce::Component> GetComponent() override;
    };
}
