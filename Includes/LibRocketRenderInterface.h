/*
* This source file is part of libRocket, the HTML/CSS Interface Middleware
*
* For the latest information, see http://www.librocket.com
*
* Copyright (c) 2008-2010 CodePoint Ltd, Shift Technology Ltd
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
*/

#pragma once

#include "UI.h"
#include "ResourceManager.h"
#include "World.h"
#include "Material.h"

namespace ce {
    class LibRocketRenderInterface : public RML_CORE::RenderInterface
    {
    public:
        LibRocketRenderInterface();
        virtual ~LibRocketRenderInterface();

        #if defined(CE_UI_ROCKET)
            /// Called by Rocket when it wants to compile geometry it believes will be static for the forseeable future.
            virtual RML_CORE::CompiledGeometryHandle CompileGeometry(RML_CORE::Vertex* vertices, int num_vertices, int* indices, int num_indices, RML_CORE::TextureHandle texture) override;

            /// Called by Rocket when it wants to render geometry that it does not wish to optimise.
            virtual void RenderGeometry(RML_CORE::Vertex* vertices, int num_vertices, int* indices, int num_indices, RML_CORE::TextureHandle texture, const RML_CORE::Vector2f& translation) override;


            /// Called by Rocket when it wants to render application-compiled geometry.
            virtual void RenderCompiledGeometry(RML_CORE::CompiledGeometryHandle geometry, const RML_CORE::Vector2f& translation) override;
            /// Called by Rocket when it wants to release application-compiled geometry.
            virtual void ReleaseCompiledGeometry(RML_CORE::CompiledGeometryHandle geometry) override;

            /// Called by Rocket when it wants to enable or disable scissoring to clip content.
            virtual void EnableScissorRegion(bool enable) override;
            /// Called by Rocket when it wants to change the scissor region.
            virtual void SetScissorRegion(int x, int y, int width, int height) override;

            /// Called by Rocket when a texture is required by the library.
            virtual bool LoadTexture(RML_CORE::TextureHandle& texture_handle, RML_CORE::Vector2i& texture_dimensions, const RML_CORE::String& source) override;
            /// Called by Rocket when a texture is required to be built from an internally-generated sequence of pixels.
            virtual bool GenerateTexture(RML_CORE::TextureHandle& texture_handle, const RML_CORE::byte* source, const RML_CORE::Vector2i& source_dimensions) override;
            /// Called by Rocket when a loaded texture is no longer required.
            virtual void ReleaseTexture(RML_CORE::TextureHandle texture) override;

            /// Returns the native horizontal texel offset for the renderer.
            float GetHorizontalTexelOffset() override;
            /// Returns the native vertical texel offset for the renderer.
            float GetVerticalTexelOffset() override;
        #elif defined(CE_UI_RMLUI)
	        RML_CORE::CompiledGeometryHandle CompileGeometry(RML_CORE::Span<const RML_CORE::Vertex> vertices_span, RML_CORE::Span<const int> indices_span) override;
	        void RenderGeometry(RML_CORE::CompiledGeometryHandle geometry, RML_CORE::Vector2f translation, RML_CORE::TextureHandle texture) override;
	        void ReleaseGeometry(RML_CORE::CompiledGeometryHandle geometry) override;
            RML_CORE::TextureHandle LoadTexture(RML_CORE::Vector2i& texture_dimensions, const RML_CORE::String& source) override;
            RML_CORE::TextureHandle GenerateTexture(RML_CORE::Span<const RML_CORE::byte> source_span, RML_CORE::Vector2i source_dimensions) override;
            void ReleaseTexture(RML_CORE::TextureHandle texture) override;
            void EnableScissorRegion(bool enable) override;
            void SetScissorRegion(RML_CORE::Rectanglei region) override;
        #endif

    private:
        std::string vertShaderPath = "cutting_edge/Resources/Shaders/ui.vert";
        std::string fragShaderPath = "cutting_edge/Resources/Shaders/ui.frag";
        std::string shaderProgName = "ui";

        std::string mainTexUniform = "mainTex";
        std::string translationUniform = "translation";
        std::string noTexUniform = "noTex";

        std::shared_ptr<ce::ShaderProgram> shaderProgram;
        std::shared_ptr<ce::Material> material;
    };
}