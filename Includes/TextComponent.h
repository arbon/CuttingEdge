#pragma once

#include "Component.h"

namespace ce {
	struct FontCharacter {
		GLuint     TextureID;  // ID handle of the glyph texture
		glm::ivec2 Size;       // Size of glyph
		glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
		GLuint     Advance;    // Offset to advance to next glyph
	};

	struct TextComponentParams
	{
		TextComponentParams(std::string FontPath = "", std::string Text = "", std::shared_ptr<ce::Material> Material = std::shared_ptr<ce::Material>(), glm::vec3 MinBound = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 MaxBound = glm::vec3(0.0f, 0.0f, 0.0f),
		 float FontHeight = 12.0f, glm::vec3 Colour = glm::vec3(1.0f, 0.0f, 0.0f), float Lifetime = -1.0f, bool Is2D = true)
		: FontPath(FontPath), Text(Text), Material(Material), MinBound(MinBound), MaxBound(MaxBound), FontHeight(FontHeight), Colour(Colour), Lifetime(Lifetime), Is2D(Is2D)
		{

		}

		std::string FontPath;
		std::string Text;
		std::shared_ptr<ce::Material> Material;
		glm::vec3 MinBound;
		glm::vec3 MaxBound;
		float FontHeight;
		glm::vec3 Colour;
		float Lifetime;
		bool Is2D;
	};

	class TextComponent : public ce::Component, public ce::Drawable, public ce::Processable {
	protected:
		static std::vector<FontCharacter> Characters;

		static std::string DefaultFontPath;
		static unsigned int DefaultNumChars;
		static unsigned int DefaultPixelHeight;

		GLuint vao;
		GLuint vbo;

		bool timeout;
		float timeToLive;

		glm::mat4 mvp;
	
		static void LoadCharacters(std::string fontPath, unsigned int numCharacters, unsigned int pixelHeight);

	public:
		TextComponentParams Params;

		TextComponent(std::weak_ptr<ce::Transform> transform, TextComponentParams params, std::string name = "Text Component");

        ~TextComponent();

		// Inherited via Component
		virtual std::weak_ptr<Drawable> GetDrawable() override;
		virtual std::weak_ptr<Processable> GetProcessable() override;

		// Inherited via Drawable
		virtual void Draw() override;
		virtual std::weak_ptr<Material> GetMaterial() override;
		virtual std::weak_ptr<Component> GetComponent() override;

		// Inherited via Processable
		virtual void Process() override;
	};
}