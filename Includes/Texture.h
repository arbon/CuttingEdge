#pragma once
#include <memory>
#include <string>
#include <glm/gtc/matrix_transform.hpp>
#include "gl_includes.h"

namespace ce {
    class Texture {
    public:
        struct TextureData {
            std::string path;
            std::string name;
            GLint width;
            GLint height;
            GLint components;
            GLuint index;
            const unsigned char* data;
        };

        std::shared_ptr<TextureData> data;

        Texture(std::shared_ptr<TextureData> data);

        glm::vec2 GetDimensions();
    };
}
