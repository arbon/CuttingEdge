#pragma once
#include "DrawableTrait.h"

namespace ce {
    struct ZIndexed : public DrawableTrait {
    private:
        int zIndex;

    public:
        int GetZIndex() const;
        void SetZIndex(int zIndex);

        // Inherited via DrawableTrait
        virtual bool operator<(const ce::DrawableTrait & rhs) const override;
    };
}
