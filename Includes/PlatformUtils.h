#pragma once

#include <filesystem>

#ifdef BAZEL_RUNFILES_ENABLED
    #include "tools/cpp/runfiles/runfiles.h"
    #include <iostream>

    typedef bazel::tools::cpp::runfiles::Runfiles Runfiles;
#endif

namespace ce
{
    class PlatformUtils
    {
    private:
        #ifdef BAZEL_RUNFILES_ENABLED
            std::unique_ptr<Runfiles> runfiles;
        #endif

        std::filesystem::path GetBaseDirectory() const;
        std::filesystem::path GetResourceDirectory() const;

    public:
        PlatformUtils() {};

        #ifdef BAZEL_RUNFILES_ENABLED
            PlatformUtils(const std::string& argv0, const std::string& bazelRepository = BAZEL_CURRENT_REPOSITORY)
            {
                std::string error;
                this->runfiles = std::unique_ptr<Runfiles>(Runfiles::Create(argv0, bazelRepository, &error));

                if (runfiles == nullptr) {
                    std::cout << "Failed to create Runfiles object for " << bazelRepository << std::endl;
                    //abort();
                }
            }
        #endif

        std::filesystem::path ResolvePath_Internal(std::string path) const;
    };

    std::filesystem::path ResolvePath(std::string path);
};