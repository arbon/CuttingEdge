#pragma once
#include <string>
#include <memory>
#include "Transform.h"
#include "Drawable.h"
#include "Processable.h"

namespace ce {
    class Component : public std::enable_shared_from_this<Component> {
    protected:
        std::weak_ptr<Transform> transform;

        std::string name;

    public:
        std::shared_ptr<Component> getPointer() {
            return shared_from_this();
        }

        explicit Component(std::weak_ptr<Transform> transform, std::string name = "component");

        virtual std::weak_ptr<Drawable> GetDrawable() = 0;

        virtual std::weak_ptr<Processable> GetProcessable() = 0;

        virtual void Initialize() {};

        std::weak_ptr<Transform> GetTransform() const;

		void Destroy(std::weak_ptr<ce::Component> component);
		void Destroy(std::weak_ptr<ce::Transform> transform);
    };
}
