#pragma once

#include "Component.h"

namespace ce {
	struct DebugDrawObject {
		GLenum DrawMode;
		bool Is2D;
		glm::vec3 Color;
		std::vector<float> Vertices;
		float LifeTime;
		float DrawWidth;
        GLuint Vao;
        GLuint Vbo;
        std::shared_ptr<ce::Material> Material;
	};

	class DebugDrawComponent : public ce::Component, public ce::Drawable, public ce::Processable {
	protected:
        const std::string COLOR_UNIFORM_NAME = "color";

		DebugDrawObject drawObject;
		bool timeout;
		float timeToLive;

	public:
		DebugDrawComponent(std::weak_ptr<ce::Transform> transform, DebugDrawObject drawObject, std::string name = "Debug Draw Component");

        ~DebugDrawComponent();

		// Inherited via Component
		virtual std::weak_ptr<Drawable> GetDrawable() override;
		virtual std::weak_ptr<Processable> GetProcessable() override;

		// Inherited via Drawable
		virtual void Draw() override;
		virtual std::weak_ptr<Material> GetMaterial() override;
		virtual std::weak_ptr<Component> GetComponent() override;

		// Inherited via Processable
		virtual void Process() override;
	};
}