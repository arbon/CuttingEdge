#pragma once
#include "Component.h"
#include "DeferredAmbientLight.h"

namespace ce {
    class AmbientLightComponent : public ce::Component {
        std::shared_ptr<DeferredAmbientLight> light;

    public:
        AmbientLightComponent(std::weak_ptr<Transform> transform, float intensity, std::string name = "ambient-light");

        virtual void Initialize() override;
        virtual std::weak_ptr<ce::Drawable> GetDrawable() override;
        virtual std::weak_ptr<ce::Processable> GetProcessable() override;
    };
}