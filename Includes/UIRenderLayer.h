#pragma once
#include <set>
#include "UIComponent.h"
#include "RenderLayer.h"

namespace ce {
    class UIRenderLayer : public ce::RenderLayer {

    public:
        UIRenderLayer();

        virtual void Render(ce::Camera & camera) override;
        virtual void AddDrawable(std::weak_ptr<ce::Drawable> drawable) override;
    };
}