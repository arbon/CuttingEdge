#pragma once
#include <memory>
#include "Component.h"
#include "Sprite.h"
#include "ResourceManager.h"

namespace ce {
    class SpriteComponent : public Component {
    protected:
        std::shared_ptr<Sprite> sprite;

    public:
        SpriteComponent(std::weak_ptr<Transform> transform, std::shared_ptr<Sprite> sprite, std::string name = "SpriteComponent");

        glm::vec2 GetSpriteDimensions();
        glm::vec4 GetSpriteCoords();
        void SetSpriteCoords(const glm::vec4 &spritecoords);
        void SetMaterial(std::shared_ptr<Material> material);
        std::weak_ptr<Material> GetMaterial();

        std::shared_ptr<ce::Sprite>& GetSprite();

        virtual std::weak_ptr<Drawable> GetDrawable() override;
        virtual std::weak_ptr<Processable> GetProcessable() override;
        virtual void Initialize() override;
    };
}
