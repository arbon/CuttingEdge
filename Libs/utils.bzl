def get_path_from_label(label_string):
    label = Label(label_string)
    if label.workspace_root:
        return label.workspace_root + "/" + label.package + "/" + label.name
    else:
        return label.package + "/" + label.name

def get_output_from_target(target, output_name):
    return "$$(TARGET_OUTPUTS=($(execpaths " + target + ")); OUTPUT_REGEX='\\S*" + output_name + "';\
    for path in $${TARGET_OUTPUTS[@]}; do [[ $$path =~ $$OUTPUT_REGEX ]] && echo $${path}; done)"