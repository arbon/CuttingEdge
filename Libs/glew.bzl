load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def glew_library():
    http_archive(
        name = "glew",
        url = "https://github.com/nigels-com/glew/releases/download/glew-2.1.0/glew-2.1.0.tgz",
        sha256 = "04de91e7e6763039bc11940095cd9c7f880baba82196a7765f727ac05a993c95",
        strip_prefix = "glew-2.1.0",
        build_file_content = """
load("@rules_foreign_cc//foreign_cc:defs.bzl", "cmake", "configure_make")

filegroup(
    name = "glew_files",
    srcs = glob(["**"]),
    visibility = ["//visibility:public"],
)

exports_files(
    glob(include = ["**"], exclude_directories = 0)
)
        """,
    )