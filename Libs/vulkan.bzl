def _impl(repository_ctx):
    sdk_path = repository_ctx.os.environ.get("VULKAN_SDK")

    if sdk_path == '' or sdk_path == None:
        print("Unable to find Vulkan SDK, assuming it exists in sysroot")
        file_content = """
cc_library(
    name = "vulkan",
    linkopts = ["-lvulkan"],
    visibility = ["//visibility:public"],
)

cc_library(
    name = "shaderc",
    linkopts = ["-lshaderc_combined"],
    visibility = ["//visibility:public"],
)
"""
    else:
        repository_ctx.symlink(sdk_path, "vulkan_sdk")
        file_content = """
config_setting(
    name = "cfg_dbg",
    values = {"compilation_mode": "dbg"},
)

cc_import(
    name = "vulkan_imported",
    static_library = "vulkan_sdk/Lib/vulkan-1.lib",
    hdrs = glob([
        "vulkan_sdk/Include/vulkan/*.h",
        "vulkan_sdk/Include/vulkan/*.hpp",
        ]),
    includes = ["vulkan_sdk/Include"],
    visibility = ["//visibility:public"],
)

cc_library(
    name = "vulkan",
    includes = ["vulkan_sdk/Include"],
    deps = [":vulkan_imported"],
    visibility = ["//visibility:public"],
)

cc_import(
    name = "shaderc_imported",
    static_library = select({
        "//:cfg_dbg": "vulkan_sdk/Lib/shaderc_combinedd.lib",
        "//conditions:default": "vulkan_sdk/Lib/shaderc_combined.lib",
    }),
    hdrs = glob([
        "vulkan_sdk/Include/shaderc/*.h",
        "vulkan_sdk/Include/shaderc/*.hpp",
        ]),
    visibility = ["//visibility:public"],
)

cc_library(
    name = "shaderc",
    includes = ["vulkan_sdk/Include"],
    deps = [":shaderc_imported"],
    visibility = ["//visibility:public"],
)
"""

    repository_ctx.file("BUILD.bazel", file_content)

vulkan_library = repository_rule(
    implementation = _impl,
    local = True,
    environ = ["VULKAN_SDK"],
)